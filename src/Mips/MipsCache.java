package Mips;

/**
 * Created by yhzmiao on 16/5/9.
 */
public class MipsCache {
    public boolean imm;
    public int val;

    public MipsCache (boolean imm, int val) {
        this.imm = imm;
        this.val = val;
    }

    public String toString () {
        if(imm)
            return Integer.toString(val);
        else
            return Integer.toString(val)+"($sp)";
    }
}
