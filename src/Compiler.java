import AST.Stmt.Expr.Constant.StringConstant;
import IR.FuncPara;
import IR.IRInstruction.ArithInstr.*;
import IR.IRInstruction.BitwiseInstr.*;
import IR.IRInstruction.CondSetInstr.*;
import IR.IRInstruction.FuncCallInstr.BuildinFuncCall;
import IR.IRInstruction.FuncCallInstr.NormalFuncCall;
import IR.IRInstruction.FuncCallInstr.VoidFuncCall;
import IR.IRInstruction.FuncDeclInstr;
import IR.IRInstruction.FuncEndInstr;
import IR.IRInstruction.IRInstr;
import IR.IRInstruction.JumpInstr.Branch;
import IR.IRInstruction.JumpInstr.Jump;
import IR.IRInstruction.JumpInstr.RetInstr;
import IR.IRInstruction.Label;
import IR.IRInstruction.MemAccInstr.Alloc;
import IR.IRInstruction.MemAccInstr.Load;
import IR.IRInstruction.MemAccInstr.Save;
import IR.IRInstruction.RegTransInstr.RegTransInstr;
import IR.IRRegister.IRRegister;
import IR.IRRegister.ImmediateNum;
import IR.IRRegister.VirtualReg.Address;
import IR.IRRegister.VirtualReg.VirtualReg;
import Mips.MipsCache;
import Parser.*;
import Symbol.SymbolTable;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by yhzmiao on 16/4/4.
 */
public class Compiler {

    public static HashMap func;

    public static void init() {
        EvalListenerSecond.symbolTable = new SymbolTable();
        SymbolTable.var = new HashMap();
        SymbolTable.UniDecl = new ArrayList<>();
        IRRegister.cur_reg = 0;
        IRRegister.uniReg = new HashMap();
        IRRegister.regList = new HashMap();
        IRRegister.cur_Tar = 0;
        IRRegister.cur_tmp = 0;
        IRInstr.instrs = new ArrayList<>();
        func = new HashMap();
    }

    public static void PrintIR () {
        for (int i = 0; i < IRInstr.instrs.size(); ++i) {
            System.out.println(IRInstr.instrs.get(i).toString());
        }
    }

    public static void instr (IRInstr toTrans, String cur_func) {
        if (toTrans instanceof FuncDeclInstr) {
            return;
        }
        if (toTrans instanceof FuncEndInstr) {
            System.out.println("lw $ra 0($sp)");
            System.out.println("jr $ra");
        }
        else if (toTrans instanceof Label) {
            //System.out.println(((Label) toTrans).body.id);
            System.out.println (((Label) toTrans).body.name + (((Label) toTrans).body.id == 0 ? "" : Integer.toString(((Label) toTrans).body.id)) + ":");
            if (((Label) toTrans).body.id == 0) {
                System.out.println("sw $ra 0($sp)");
            }
        }
        else {
            FuncPara now = (FuncPara) func.get(cur_func);
            if (toTrans instanceof Add) {
                MipsCache rd = new MipsCache(false , (int)now.para.get(((Add) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((Add) toTrans).src1 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Add) toTrans).src1).value;
                    MipsCache rs = new MipsCache((((Add) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rs.toString());
                }
                else {
                    tmp = (int) now.para.get(((Add) toTrans).src1.getId()) * (4);
                    MipsCache rs = new MipsCache((((Add) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rs.toString());
                }
                if (((Add) toTrans).src2 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Add) toTrans).src2).value;
                    MipsCache rt = new MipsCache((((Add) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("li $t1 " + rt.toString());
                }
                else {
                    tmp = (int) now.para.get(((Add) toTrans).src2.getId()) * (4);
                    MipsCache rt = new MipsCache((((Add) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t1 " + rt.toString());
                }
                System.out.println("add $t0 $t0 $t1");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof Div) {
                MipsCache rd = new MipsCache(false, (int)now.para.get(((Div) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((Div) toTrans).src1 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Div) toTrans).src1).value;
                    MipsCache rt = new MipsCache((((Div) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rt.toString());
                }
                else {
                    tmp = (int)now.para.get(((Div) toTrans).src1.getId()) * (4);
                    MipsCache rt = new MipsCache((((Div) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rt.toString());
                }
                if (((Div) toTrans).src2 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Div) toTrans).src2).value;
                    MipsCache rt = new MipsCache((((Div) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("li $t1 " + rt.toString());
                }
                else {
                    tmp = (int) now.para.get(((Div) toTrans).src2.getId()) * (4);
                    MipsCache rt = new MipsCache((((Div) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t1 " + rt.toString());
                }
                System.out.println("div $t0 $t0 $t1");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof Mul) {
                MipsCache rd = new MipsCache(false ,(int)now.para.get(((Mul) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((Mul) toTrans).src1 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Mul) toTrans).src1).value;
                    MipsCache rs = new MipsCache((((Mul) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rs.toString());
                }
                else {
                    tmp = (int)now.para.get(((Mul) toTrans).src1.getId()) * (4);
                    MipsCache rs = new MipsCache((((Mul) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rs.toString());
                }
                if (((Mul) toTrans).src2 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Mul) toTrans).src2).value;
                    MipsCache rt = new MipsCache((((Mul) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("li $t1 " + rt.toString());
                }
                else {
                    tmp = (int)now.para.get(((Mul) toTrans).src2.getId()) * (4);
                    MipsCache rt = new MipsCache((((Mul) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t1 " + rt.toString());
                }
                System.out.println("mul $t0 $t0 $t1");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof Rem) {
                MipsCache rd = new MipsCache(false ,(int)now.para.get(((Rem) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((Rem) toTrans).src1 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Rem) toTrans).src1).value;
                    MipsCache rs = new MipsCache((((Rem) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rs.toString());
                }
                else {
                    tmp = (int)now.para.get(((Rem) toTrans).src1.getId()) * (4);
                    MipsCache rs = new MipsCache((((Rem) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rs.toString());
                }
                if (((Rem) toTrans).src2 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Rem) toTrans).src2).value;
                    MipsCache rt = new MipsCache((((Rem) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("li $t1 " + rt.toString());
                }
                else {
                    tmp = (int)now.para.get(((Rem) toTrans).src2.getId()) * (4);
                    MipsCache rt = new MipsCache((((Rem) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t1 " + rt.toString());
                }
                System.out.println("rem $t0 $t0 $t1");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof Sub) {
                MipsCache rd = new MipsCache(false ,(int)now.para.get(((Sub) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((Sub) toTrans).src1 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Sub) toTrans).src1).value;
                    MipsCache rs = new MipsCache((((Sub) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rs.toString());
                }
                else {
                    tmp = (int)now.para.get(((Sub) toTrans).src1.getId()) * (4);
                    MipsCache rs = new MipsCache((((Sub) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rs.toString());
                }
                if (((Sub) toTrans).src2 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Sub) toTrans).src2).value;
                    MipsCache rt = new MipsCache((((Sub) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("li $t1 " + rt.toString());
                }
                else {
                    tmp = (int)now.para.get(((Sub) toTrans).src2.getId()) * (4);
                    MipsCache rt = new MipsCache((((Sub) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t1 " + rt.toString());
                }
                System.out.println("sub $t0 $t0 $t1");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof Neg) {
                MipsCache rd = new MipsCache(false ,(int)now.para.get(((Neg) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((Neg) toTrans).src instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Neg) toTrans).src).value;
                    MipsCache rs = new MipsCache((((Neg) toTrans).src instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rs.toString());
                }
                else {
                    tmp = (int)now.para.get(((Neg) toTrans).src.getId()) * (4);
                    MipsCache rs = new MipsCache((((Neg) toTrans).src instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rs.toString());
                }
                System.out.println("neg $t0 $t0");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof And) {
                MipsCache rd = new MipsCache(false ,(int)now.para.get(((And) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((And) toTrans).src1 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((And) toTrans).src1).value;
                    MipsCache rs = new MipsCache((((And) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rs.toString());
                }
                else {
                    tmp = (int)now.para.get(((And) toTrans).src1.getId()) * (4);
                    MipsCache rs = new MipsCache((((And) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rs.toString());
                }
                if (((And) toTrans).src2 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((And) toTrans).src2).value;
                    MipsCache rt = new MipsCache((((And) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("li $t1 " + rt.toString());
                }
                else {
                    tmp = (int)now.para.get(((And) toTrans).src2.getId()) * (4);
                    MipsCache rt = new MipsCache((((And) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t1 " + rt.toString());
                }
                System.out.println("and $t0 $t0 $t1");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof Left) {
                MipsCache rd = new MipsCache(false ,(int)now.para.get(((Left) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((Left) toTrans).src1 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Left) toTrans).src1).value;
                    MipsCache rs = new MipsCache((((Left) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rs.toString());
                }
                else {
                    tmp = (int)now.para.get(((Left) toTrans).src1.getId()) * (4);
                    MipsCache rs = new MipsCache((((Left) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rs.toString());
                }
                if (((Left) toTrans).src2 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Left) toTrans).src2).value;
                    MipsCache rt = new MipsCache((((Left) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("li $t1 " + rt.toString());
                }
                else {
                    tmp = (int)now.para.get(((Left) toTrans).src2.getId()) * (4);
                    MipsCache rt = new MipsCache((((Left) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t1 " + rt.toString());
                }
                System.out.println("sll $t0 $t0 $t1");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof Or) {
                MipsCache rd = new MipsCache(false ,(int)now.para.get(((Or) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((Or) toTrans).src1 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Or) toTrans).src1).value;
                    MipsCache rs = new MipsCache((((Or) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rs.toString());
                }
                else {
                    tmp = (int)now.para.get(((Or) toTrans).src1.getId()) * (4);
                    MipsCache rs = new MipsCache((((Or) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rs.toString());
                }
                if (((Or) toTrans).src2 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Or) toTrans).src2).value;
                    MipsCache rt = new MipsCache((((Or) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("li $t1 " + rt.toString());
                }
                else {
                    tmp = (int)now.para.get(((Or) toTrans).src2.getId()) * (4);
                    MipsCache rt = new MipsCache((((Or) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t1 " + rt.toString());
                }
                System.out.println("or $t0 $t0 $t1");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof Right) {
                MipsCache rd = new MipsCache(false ,(int)now.para.get(((Right) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((Right) toTrans).src1 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Right) toTrans).src1).value;
                    MipsCache rs = new MipsCache((((Right) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rs.toString());
                }
                else {
                    tmp = (int)now.para.get(((Right) toTrans).src1.getId()) * (4);
                    MipsCache rs = new MipsCache((((Right) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rs.toString());
                }
                if (((Right) toTrans).src2 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Right) toTrans).src2).value;
                    MipsCache rt = new MipsCache((((Right) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("li $t1 " + rt.toString());
                }
                else {
                    tmp = (int)now.para.get(((Right) toTrans).src2.getId()) * (4);
                    MipsCache rt = new MipsCache((((Right) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t1 " + rt.toString());
                }
                System.out.println("srl $t0 $t0 $t1");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof Xor) {
                MipsCache rd = new MipsCache(false ,(int)now.para.get(((Xor) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((Xor) toTrans).src1 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Xor) toTrans).src1).value;
                    MipsCache rs = new MipsCache((((Xor) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rs.toString());
                }
                else {
                    tmp = (int)now.para.get(((Xor) toTrans).src1.getId()) * (4);
                    MipsCache rs = new MipsCache((((Xor) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rs.toString());
                }
                if (((Xor) toTrans).src2 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Xor) toTrans).src2).value;
                    MipsCache rt = new MipsCache((((Xor) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("li $t1 " + rt.toString());
                }
                else {
                    tmp = (int)now.para.get(((Xor) toTrans).src2.getId()) * (4);
                    MipsCache rt = new MipsCache((((Xor) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t1 " + rt.toString());
                }
                System.out.println("xor $t0 $t0 $t1");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof Not) {
                MipsCache rd = new MipsCache(false ,(int)now.para.get(((Not) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((Not) toTrans).src instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Not) toTrans).src).value;
                    MipsCache rs = new MipsCache((((Not) toTrans).src instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rs.toString());
                }
                else {
                    tmp = (int)now.para.get(((Not) toTrans).src.getId()) * (4);
                    MipsCache rs = new MipsCache((((Not) toTrans).src instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rs.toString());
                }
                System.out.println("xori $t0 $t0 1");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof EqualTo) {
                MipsCache rd = new MipsCache(false ,(int)now.para.get(((EqualTo) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((EqualTo) toTrans).src1 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((EqualTo) toTrans).src1).value;
                    MipsCache rs = new MipsCache((((EqualTo) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rs.toString());
                }
                else {
                    tmp = (int)now.para.get(((EqualTo) toTrans).src1.getId()) * (4);
                    MipsCache rs = new MipsCache((((EqualTo) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rs.toString());
                }
                if (((EqualTo) toTrans).src2 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((EqualTo) toTrans).src2).value;
                    MipsCache rt = new MipsCache((((EqualTo) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("li $t1 " + rt.toString());
                }
                else {
                    tmp = (int)now.para.get(((EqualTo) toTrans).src2.getId()) * (4);
                    MipsCache rt = new MipsCache((((EqualTo) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t1 " + rt.toString());
                }
                System.out.println("seq $t0 $t0 $t1");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof GreaterThan) {
                MipsCache rd = new MipsCache(false ,(int)now.para.get(((GreaterThan) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((GreaterThan) toTrans).src1 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((GreaterThan) toTrans).src1).value;
                    MipsCache rs = new MipsCache((((GreaterThan) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rs.toString());
                }
                else {
                    tmp = (int)now.para.get(((GreaterThan) toTrans).src1.getId()) * (4);
                    MipsCache rs = new MipsCache((((GreaterThan) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rs.toString());
                }
                if (((GreaterThan) toTrans).src2 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((GreaterThan) toTrans).src2).value;
                    MipsCache rt = new MipsCache((((GreaterThan) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("li $t1 " + rt.toString());
                }
                else {
                    tmp = (int)now.para.get(((GreaterThan) toTrans).src2.getId()) * (4);
                    MipsCache rt = new MipsCache((((GreaterThan) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t1 " + rt.toString());
                }
                System.out.println("sgt $t0 $t0 $t1");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof GreaterEqual) {
                MipsCache rd = new MipsCache(false ,(int)now.para.get(((GreaterEqual) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((GreaterEqual) toTrans).src1 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((GreaterEqual) toTrans).src1).value;
                    MipsCache rs = new MipsCache((((GreaterEqual) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rs.toString());
                }
                else {
                    tmp = (int)now.para.get(((GreaterEqual) toTrans).src1.getId()) * (4);
                    MipsCache rs = new MipsCache((((GreaterEqual) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rs.toString());
                }
                if (((GreaterEqual) toTrans).src2 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((GreaterEqual) toTrans).src2).value;
                    MipsCache rt = new MipsCache((((GreaterEqual) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("li $t1 " + rt.toString());
                }
                else {
                    tmp = (int)now.para.get(((GreaterEqual) toTrans).src2.getId()) * (4);
                    MipsCache rt = new MipsCache((((GreaterEqual) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t1 " + rt.toString());
                }
                System.out.println("sge $t0 $t0 $t1");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof LessThan) {
                MipsCache rd = new MipsCache(false ,(int)now.para.get(((LessThan) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((LessThan) toTrans).src1 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((LessThan) toTrans).src1).value;
                    MipsCache rs = new MipsCache((((LessThan) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rs.toString());
                }
                else {
                    tmp = (int) now.para.get(((LessThan) toTrans).src1.getId()) * (4);
                    MipsCache rs = new MipsCache((((LessThan) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rs.toString());
                }
                if (((LessThan) toTrans).src2 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((LessThan) toTrans).src2).value;
                    MipsCache rt = new MipsCache((((LessThan) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("li $t1 " + rt.toString());
                }
                else {
                    tmp = (int)now.para.get(((LessThan) toTrans).src2.getId()) * (4);
                    MipsCache rt = new MipsCache((((LessThan) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t1 " + rt.toString());
                }
                System.out.println("slt $t0 $t0 $t1");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof LessEqual) {
                MipsCache rd = new MipsCache(false ,(int)now.para.get(((LessEqual) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((LessEqual) toTrans).src1 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((LessEqual) toTrans).src1).value;
                    MipsCache rs = new MipsCache((((LessEqual) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rs.toString());
                }
                else {
                    tmp = (int)now.para.get(((LessEqual) toTrans).src1.getId()) * (4);
                    MipsCache rs = new MipsCache((((LessEqual) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rs.toString());
                }
                if (((LessEqual) toTrans).src2 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((LessEqual) toTrans).src2).value;
                    MipsCache rt = new MipsCache((((LessEqual) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("li $t1 " + rt.toString());
                }
                else {
                    tmp = (int)now.para.get(((LessEqual) toTrans).src2.getId()) * (4);
                    MipsCache rt = new MipsCache((((LessEqual) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t1 " + rt.toString());
                }
                System.out.println("sle $t0 $t0 $t1");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof NotEqualTo) {
                MipsCache rd = new MipsCache(false ,(int)now.para.get(((NotEqualTo) toTrans).dest.getId()) * (4));
                int tmp = 0;
                if (((NotEqualTo) toTrans).src1 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((NotEqualTo) toTrans).src1).value;
                    MipsCache rs = new MipsCache((((NotEqualTo) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rs.toString());
                }
                else {
                    tmp = (int)now.para.get(((NotEqualTo) toTrans).src1.getId()) * (4);
                    MipsCache rs = new MipsCache((((NotEqualTo) toTrans).src1 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rs.toString());
                }
                if (((NotEqualTo) toTrans).src2 instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((NotEqualTo) toTrans).src2).value;
                    MipsCache rt = new MipsCache((((NotEqualTo) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("li $t1 " + rt.toString());
                }
                else {
                    tmp = (int)now.para.get(((NotEqualTo) toTrans).src2.getId()) * (4);
                    MipsCache rt = new MipsCache((((NotEqualTo) toTrans).src2 instanceof ImmediateNum), tmp);
                    System.out.println("lw $t1 " + rt.toString());
                }
                System.out.println("sne $t0 $t0 $t1");
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof Branch) {
                int tmp = (int)now.para.get(((Branch) toTrans).cond.getId()) * (4);
                MipsCache rs = new MipsCache(false, tmp);
                System.out.println("lw $t0 " + rs.toString());
                System.out.println("beqz $t0 " + ((Branch) toTrans).ifFalse.name + Integer.toString(((Branch) toTrans).ifFalse.id));
            }else if (toTrans instanceof Jump) {
                System.out.println("j " + ((Jump)toTrans).target.name + ((Jump) toTrans).target.id);
            }else if (toTrans instanceof RetInstr) {
                int tmp = 0;
                if (((RetInstr) toTrans).ret instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((RetInstr) toTrans).ret).value;
                    MipsCache rt = new MipsCache((((RetInstr) toTrans).ret instanceof ImmediateNum), tmp);
                    System.out.println("li $v0 " + rt);
                }
                else {
                    tmp = (int)now.para.get(((RetInstr) toTrans).ret.getId()) * (4);
                    MipsCache rt = new MipsCache((((RetInstr) toTrans).ret instanceof ImmediateNum), tmp);
                    System.out.println("lw $v0 " + rt);
                }
                System.out.println("lw $ra 0($sp)");
                System.out.println("jr $ra");
            }else if (toTrans instanceof Alloc) {
                System.out.println("li $v0 9");
                int tmp = 0;
                if (((Alloc) toTrans).size instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((Alloc) toTrans).size).value;
                    MipsCache rt = new MipsCache((((Alloc) toTrans).size instanceof ImmediateNum), tmp);
                    System.out.println("li $a0 " + rt);
                }
                else {
                    tmp = (int)now.para.get(((Alloc) toTrans).size.getId()) * (4);
                    MipsCache rt = new MipsCache((((Alloc) toTrans).size instanceof ImmediateNum), tmp);
                    System.out.println("lw $a0 " + rt);
                }
                System.out.println("syscall");
                //System.out.println(((Alloc) toTrans).reg);
                if (((Alloc) toTrans).reg instanceof Address)
                    System.out.println("sw $v0 " + ((Address) ((Alloc) toTrans).reg).body);
                else {
                    MipsCache rd = new MipsCache(false, (int) now.para.get(((Alloc) toTrans).reg.getId()) * (4));
                    System.out.println("sw $v0 " + rd.toString());
                }
            }else if (toTrans instanceof Load) {
                String tmp = "";
                MipsCache rd = new MipsCache(false, (int)now.para.get(((Load) toTrans).dest.getId()) * 4);
                if (((Load) toTrans).addr instanceof Address) {
                    tmp = ((Address) ((Load) toTrans).addr).body;
                    System.out.println("lw $t0 " + tmp);
                    System.out.println("sw $t0 " + rd.toString());
                }
                else {
                    int val = (int) now.para.get(((Load) toTrans).addr.getId()) * (4);
                    MipsCache pointer = new MipsCache(false, val);
                    System.out.println("lw $t0 " + pointer.toString());
                    System.out.println("lw $t1 " + (((Load) toTrans).offset) + "($t0)");
                    System.out.println("sw $t1 " + rd.toString());
                }
            }else if (toTrans instanceof Save) {
                String tmp = "";
                int val = 0;
                if (((Save) toTrans).val instanceof ImmediateNum) {
                    System.out.println("li $t0 " + ((ImmediateNum) ((Save) toTrans).val).value);
                }
                else {
                    val = (int)now.para.get(((Save) toTrans).val.getId()) * 4;
                    MipsCache value = new MipsCache(false, val);
                    System.out.println("lw $t0 " + value.toString());
                }
                if (((Save) toTrans).addr instanceof Address) {
                    tmp = ((Address) ((Save) toTrans).addr).body;
                }
                else {
                    val = (int)now.para.get(((Save) toTrans).addr.getId()) * 4;
                    MipsCache value = new MipsCache(false, val);
                    System.out.println("lw $t1 " + value.toString());
                    tmp = Integer.toString(((Save) toTrans).offset) + "($t1)";
                }
                System.out.println("sw $t0 " + tmp);
            }else if (toTrans instanceof NormalFuncCall) {
                int tmp = ((FuncPara)func.get(((NormalFuncCall) toTrans).funcName + "_entry")).total * 4;
                for (int i = 0; i < ((NormalFuncCall) toTrans).list.size(); ++ i) {
                    int val = 0;
                    if (((NormalFuncCall) toTrans).list.get(i) instanceof ImmediateNum) {
                        val = ((ImmediateNum) ((NormalFuncCall) toTrans).list.get(i)).value;
                        MipsCache rt = new MipsCache(((NormalFuncCall) toTrans).list.get(i) instanceof ImmediateNum, val);
                        System.out.println("li $t0 " + rt);
                    } else {
                        val = (int) now.para.get(((NormalFuncCall) toTrans).list.get(i).getId()) * 4;
                        MipsCache rt = new MipsCache(((NormalFuncCall) toTrans).list.get(i) instanceof ImmediateNum, val);
                        System.out.println("lw $t0 " + rt);
                    }
                    MipsCache target = new MipsCache(false, (-tmp) + i * 4 + 4);
                    System.out.println("sw $t0 " + target);
                }
                System.out.println("subu $sp $sp " + tmp);
                System.out.println("jal " + ((NormalFuncCall) toTrans).funcName + "_entry");
                System.out.println("addu $sp $sp " + tmp);
                tmp = (int)now.para.get(((NormalFuncCall) toTrans).dest.getId()) * 4;
                MipsCache rt = new MipsCache(false, tmp);
                System.out.println("sw $v0 " + rt.toString());
            }else if (toTrans instanceof VoidFuncCall) {
                int tmp = ((FuncPara)func.get(((VoidFuncCall) toTrans).funcName + "_entry")).total * 4;
                for (int i = 0; i < ((VoidFuncCall) toTrans).list.size(); ++ i) {
                    int val = 0;
                    if (((VoidFuncCall) toTrans).list.get(i) instanceof ImmediateNum) {
                        val = ((ImmediateNum) ((VoidFuncCall) toTrans).list.get(i)).value;
                        MipsCache rt = new MipsCache(((VoidFuncCall) toTrans).list.get(i) instanceof ImmediateNum, val);
                        System.out.println("li $t0 " + rt);
                    } else {
                        val = (int) now.para.get(((VoidFuncCall) toTrans).list.get(i).getId()) * 4;
                        MipsCache rt = new MipsCache(((VoidFuncCall) toTrans).list.get(i) instanceof ImmediateNum, val);
                        System.out.println("lw $t0 " + rt);
                    }
                    MipsCache target = new MipsCache(false, (-tmp) + i * 4 + 4);
                    System.out.println("sw $t0 " + target);
                }
                System.out.println("subu $sp $sp " + tmp);
                System.out.println("jal " + ((VoidFuncCall) toTrans).funcName + "_entry");
                System.out.println("addu $sp $sp " + tmp);
            }else if (toTrans instanceof  RegTransInstr) {int tmp = 0;
                if (((RegTransInstr) toTrans).src instanceof ImmediateNum) {
                    tmp = ((ImmediateNum) ((RegTransInstr) toTrans).src).value;
                    MipsCache rt = new MipsCache((((RegTransInstr) toTrans).src instanceof ImmediateNum), tmp);
                    System.out.println("li $t0 " + rt);
                }
                else if (((RegTransInstr) toTrans).src instanceof Address) {
                    System.out.println("la $t0 " + ((Address) ((RegTransInstr) toTrans).src).body);
                    System.out.println("la $t0 " + ((Address) ((RegTransInstr) toTrans).src).body);
                }
                else {
                    tmp = (int)now.para.get(((RegTransInstr) toTrans).src.getId()) * (4);
                    MipsCache rt = new MipsCache((((RegTransInstr) toTrans).src instanceof ImmediateNum), tmp);
                    System.out.println("lw $t0 " + rt);
                }
                MipsCache rd = new MipsCache(false, (int)now.para.get(((RegTransInstr) toTrans).dest.getId()) * 4);
                System.out.println("sw $t0 " + rd.toString());
            }else if (toTrans instanceof BuildinFuncCall) {
                if (((BuildinFuncCall) toTrans).funcName.equals("func__print")) {
                    if (((BuildinFuncCall) toTrans).list.get(0) instanceof Address) {
                        System.out.println("la $a0 " + ((Address) ((BuildinFuncCall) toTrans).list.get(0)).body);
                    }
                    else {
                        int tmp = 0;
                        tmp = (int)now.para.get(((BuildinFuncCall) toTrans).list.get(0).getId()) * 4;
                        MipsCache ret = new MipsCache(false, tmp);
                        System.out.println("lw $a0 " + ret.toString());
                    }
                    System.out.println("jal func__print");
                }
                else if (((BuildinFuncCall) toTrans).funcName.equals("func__println")) {
                    if (((BuildinFuncCall) toTrans).list.get(0) instanceof Address) {
                        System.out.println("la $a0 " + ((Address) ((BuildinFuncCall) toTrans).list.get(0)).body);
                    } else {
                        int tmp = 0;
                        tmp = (int) now.para.get(((BuildinFuncCall) toTrans).list.get(0).getId()) * 4;
                        MipsCache ret = new MipsCache(false, tmp);
                        System.out.println("lw $a0 " + ret.toString());
                    }
                    System.out.println("jal func__println");
                }
                else if (((BuildinFuncCall) toTrans).funcName.equals("func__getInt")) {
                    System.out.println("jal func__getInt");
                    MipsCache rt = new MipsCache(false, (int)now.para.get(((BuildinFuncCall) toTrans).dest.getId()) * 4);
                    System.out.println("sw $v0 " + rt.toString());
                }
                else if (((BuildinFuncCall) toTrans).funcName.equals("func__getString")) {
                    System.out.println("jal func__getString");
                    MipsCache rt = new MipsCache(false, (int)now.para.get(((BuildinFuncCall) toTrans).dest.getId()) * 4);
                    System.out.println("sw $v0 " + rt.toString());
                }
                else if (((BuildinFuncCall) toTrans).funcName.equals("func__toString")) {
                    if (((BuildinFuncCall) toTrans).list.get(0) instanceof ImmediateNum) {
                        System.out.println("li $a0 " + ((ImmediateNum) ((BuildinFuncCall) toTrans).list.get(0)).value);
                    }
                    else {
                        int tmp = 0;
                        tmp = (int) now.para.get(((BuildinFuncCall) toTrans).list.get(0).getId()) * 4;
                        MipsCache ret = new MipsCache(false, tmp);
                        System.out.println("lw $a0 " + ret.toString());
                    }
                    System.out.println("jal func__toString");
                    //System.out.println(((BuildinFuncCall)toTrans).dest);
                    MipsCache rt = new MipsCache(false, (int)now.para.get(((BuildinFuncCall) toTrans).dest.getId()) * 4);
                    System.out.println("sw $v0 " + rt.toString());
                }
                else if (((BuildinFuncCall) toTrans).funcName.equals("func__array.size")) {
                    MipsCache rt = new MipsCache(false, (int)now.para.get( ((BuildinFuncCall) toTrans).list.get(0).getId()) * 4);
                    System.out.println("lw $a0 " + rt.toString());
                    System.out.println("jal func__array.size");
                    MipsCache rd = new MipsCache(false, (int)now.para.get( ((BuildinFuncCall) toTrans).dest.getId()) * 4);
                    System.out.println("sw $v0 " + rd.toString());
                }
                else if (((BuildinFuncCall) toTrans).funcName.equals("func__string.length")) {
                    if (((BuildinFuncCall) toTrans).list.get(0) instanceof Address) {
                        System.out.println("la $a0 " + ((Address) ((BuildinFuncCall) toTrans).list.get(0)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(0).getId()) * 4);
                        System.out.println("lw $a0 " + rt.toString());
                    }
                    System.out.println("jal func__string.length");
                    MipsCache rd = new MipsCache(false, (int)now.para.get( ((BuildinFuncCall) toTrans).dest.getId()) * 4);
                    System.out.println("sw $v0 " + rd.toString());
                }
                else if (((BuildinFuncCall) toTrans).funcName.equals("func__string.parseInt")) {
                    if (((BuildinFuncCall) toTrans).list.get(0) instanceof Address) {
                        System.out.println("la $a0 " + ((Address) ((BuildinFuncCall) toTrans).list.get(0)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(0).getId()) * 4);
                        System.out.println("lw $a0 " + rt.toString());
                    }
                    System.out.println("jal func__string.parseInt");
                    MipsCache rd = new MipsCache(false, (int)now.para.get( ((BuildinFuncCall) toTrans).dest.getId()) * 4);
                    System.out.println("sw $v0 " + rd.toString());
                }
                else if (((BuildinFuncCall) toTrans).funcName.equals("func__string.ord")) {
                    if (((BuildinFuncCall) toTrans).list.get(0) instanceof Address) {
                        System.out.println("la $a0 " + ((Address) ((BuildinFuncCall) toTrans).list.get(0)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(0).getId()) * 4);
                        System.out.println("lw $a0 " + rt.toString());
                    }
                    if (((BuildinFuncCall) toTrans).list.get(1) instanceof ImmediateNum) {
                        System.out.println("li $a1 " + ((ImmediateNum) ((BuildinFuncCall) toTrans).list.get(1)).value);
                    }
                    else {
                        int tmp = 0;
                        tmp = (int) now.para.get(((BuildinFuncCall) toTrans).list.get(1).getId()) * 4;
                        MipsCache ret = new MipsCache(false, tmp);
                        System.out.println("lw $a1 " + ret.toString());
                    }
                    System.out.println("jal func__string.ord");
                    MipsCache rd = new MipsCache(false, (int)now.para.get( ((BuildinFuncCall) toTrans).dest.getId()) * 4);
                    System.out.println("sw $v0 " + rd.toString());
                }
                else if (((BuildinFuncCall) toTrans).funcName.equals("func__string.substring")) {
                    if (((BuildinFuncCall) toTrans).list.get(0) instanceof Address) {
                        System.out.println("la $a0 " + ((Address) ((BuildinFuncCall) toTrans).list.get(0)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(0).getId()) * 4);
                        System.out.println("lw $a0 " + rt.toString());
                    }
                    if (((BuildinFuncCall) toTrans).list.get(1) instanceof ImmediateNum) {
                        System.out.println("li $a1 " + ((ImmediateNum) ((BuildinFuncCall) toTrans).list.get(1)).value);
                    }
                    else {
                        int tmp = 0;
                        tmp = (int) now.para.get(((BuildinFuncCall) toTrans).list.get(1).getId()) * 4;
                        MipsCache ret = new MipsCache(false, tmp);
                        System.out.println("lw $a1 " + ret.toString());
                    }
                    if (((BuildinFuncCall) toTrans).list.get(2) instanceof ImmediateNum) {
                        System.out.println("li $a2 " + ((ImmediateNum) ((BuildinFuncCall) toTrans).list.get(2)).value);
                    }
                    else {
                        int tmp = 0;
                        tmp = (int) now.para.get(((BuildinFuncCall) toTrans).list.get(2).getId()) * 4;
                        MipsCache ret = new MipsCache(false, tmp);
                        System.out.println("lw $a2 " + ret.toString());
                    }
                    System.out.println("jal func__string.substring");
                    MipsCache rd = new MipsCache(false, (int)now.para.get( ((BuildinFuncCall) toTrans).dest.getId()) * 4);
                    System.out.println("sw $v0 " + rd.toString());
                }
                else if (((BuildinFuncCall) toTrans).funcName.equals("func__stringConcatenate")) {
                    if (((BuildinFuncCall) toTrans).list.get(0) instanceof Address) {
                        System.out.println("la $a0 " + ((Address) ((BuildinFuncCall) toTrans).list.get(0)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(0).getId()) * 4);
                        System.out.println("lw $a0 " + rt.toString());
                    }
                    if (((BuildinFuncCall) toTrans).list.get(1) instanceof Address) {
                        System.out.println("la $a1 " + ((Address) ((BuildinFuncCall) toTrans).list.get(1)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(1).getId()) * 4);
                        System.out.println("lw $a1 " + rt.toString());
                    }
                    System.out.println("jal func__stringConcatenate");
                    MipsCache rd = new MipsCache(false, (int)now.para.get( ((BuildinFuncCall) toTrans).dest.getId()) * 4);
                    System.out.println("sw $v0 " + rd.toString());
                }
                else if (((BuildinFuncCall) toTrans).funcName.equals("func__stringIsEqual")) {
                    if (((BuildinFuncCall) toTrans).list.get(0) instanceof Address) {
                        System.out.println("la $a0 " + ((Address) ((BuildinFuncCall) toTrans).list.get(0)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(0).getId()) * 4);
                        System.out.println("lw $a0 " + rt.toString());
                    }
                    if (((BuildinFuncCall) toTrans).list.get(1) instanceof Address) {
                        System.out.println("la $a1 " + ((Address) ((BuildinFuncCall) toTrans).list.get(1)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(1).getId()) * 4);
                        System.out.println("lw $a1 " + rt.toString());
                    }
                    System.out.println("jal func__stringIsEqual");
                    MipsCache rd = new MipsCache(false, (int)now.para.get( ((BuildinFuncCall) toTrans).dest.getId()) * 4);
                    System.out.println("sw $v0 " + rd.toString());
                }
                else if (((BuildinFuncCall) toTrans).funcName.equals("func__stringLess")) {
                    if (((BuildinFuncCall) toTrans).list.get(0) instanceof Address) {
                        System.out.println("la $a0 " + ((Address) ((BuildinFuncCall) toTrans).list.get(0)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(0).getId()) * 4);
                        System.out.println("lw $a0 " + rt.toString());
                    }
                    if (((BuildinFuncCall) toTrans).list.get(1) instanceof Address) {
                        System.out.println("la $a1 " + ((Address) ((BuildinFuncCall) toTrans).list.get(1)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(1).getId()) * 4);
                        System.out.println("lw $a1 " + rt.toString());
                    }
                    System.out.println("func__stringLess");
                    MipsCache rd = new MipsCache(false, (int)now.para.get( ((BuildinFuncCall) toTrans).dest.getId()) * 4);
                    System.out.println("sw $t0 " + rd.toString());
                }
                else if (((BuildinFuncCall) toTrans).funcName.equals("func__stringLeq")) {
                    if (((BuildinFuncCall) toTrans).list.get(0) instanceof Address) {
                        System.out.println("la $a0 " + ((Address) ((BuildinFuncCall) toTrans).list.get(0)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(0).getId()) * 4);
                        System.out.println("lw $a0 " + rt.toString());
                    }
                    if (((BuildinFuncCall) toTrans).list.get(1) instanceof Address) {
                        System.out.println("la $a1 " + ((Address) ((BuildinFuncCall) toTrans).list.get(1)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(1).getId()) * 4);
                        System.out.println("lw $a1 " + rt.toString());
                    }
                    System.out.println("jal func__stringLeq");
                    MipsCache rd = new MipsCache(false, (int)now.para.get( ((BuildinFuncCall) toTrans).dest.getId()) * 4);
                    System.out.println("sw $v0 " + rd.toString());
                }
                else if (((BuildinFuncCall) toTrans).funcName.equals("func__func__stringGeq")) {
                    if (((BuildinFuncCall) toTrans).list.get(0) instanceof Address) {
                        System.out.println("la $a0 " + ((Address) ((BuildinFuncCall) toTrans).list.get(0)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(0).getId()) * 4);
                        System.out.println("lw $a0 " + rt.toString());
                    }
                    if (((BuildinFuncCall) toTrans).list.get(1) instanceof Address) {
                        System.out.println("la $a1 " + ((Address) ((BuildinFuncCall) toTrans).list.get(1)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(1).getId()) * 4);
                        System.out.println("lw $a1 " + rt.toString());
                    }
                    System.out.println("jal func__func__stringGeq");
                    MipsCache rd = new MipsCache(false, (int)now.para.get( ((BuildinFuncCall) toTrans).dest.getId()) * 4);
                    System.out.println("sw $v0 " + rd.toString());
                }
                else if (((BuildinFuncCall) toTrans).funcName.equals("func__stringNeq")) {
                    if (((BuildinFuncCall) toTrans).list.get(0) instanceof Address) {
                        System.out.println("la $a0 " + ((Address) ((BuildinFuncCall) toTrans).list.get(0)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(0).getId()) * 4);
                        System.out.println("lw $a0 " + rt.toString());
                    }
                    if (((BuildinFuncCall) toTrans).list.get(1) instanceof Address) {
                        System.out.println("la $a1 " + ((Address) ((BuildinFuncCall) toTrans).list.get(1)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(1).getId()) * 4);
                        System.out.println("lw $a1 " + rt.toString());
                    }
                    System.out.println("jal func__stringNeq");
                    MipsCache rd = new MipsCache(false, (int)now.para.get( ((BuildinFuncCall) toTrans).dest.getId()) * 4);
                    System.out.println("sw $v0 " + rd.toString());
                }
                else if (((BuildinFuncCall) toTrans).funcName.equals("func__stringLarge")) {
                    if (((BuildinFuncCall) toTrans).list.get(0) instanceof Address) {
                        System.out.println("la $a0 " + ((Address) ((BuildinFuncCall) toTrans).list.get(0)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(0).getId()) * 4);
                        System.out.println("lw $a0 " + rt.toString());
                    }
                    if (((BuildinFuncCall) toTrans).list.get(1) instanceof Address) {
                        System.out.println("la $a1 " + ((Address) ((BuildinFuncCall) toTrans).list.get(1)).body);
                    }
                    else {
                        MipsCache rt = new MipsCache(false, (int) now.para.get(((BuildinFuncCall) toTrans).list.get(1).getId()) * 4);
                        System.out.println("lw $a1 " + rt.toString());
                    }
                    System.out.println("jal func__stringLarge");
                    MipsCache rd = new MipsCache(false, (int)now.para.get( ((BuildinFuncCall) toTrans).dest.getId()) * 4);
                    System.out.println("sw $v0 " + rd.toString());
                }
            }
        }
    }

    public static void toMips () throws IOException{

        func = new HashMap();
        FuncPara fun = new FuncPara();
        String name = "";
        boolean spy = false;

        Scanner scanner = new Scanner(new File("src/Buildin/buildin.s"));

        while (scanner.hasNext()) {
            System.out.println(scanner.nextLine());
        }

        fun = new FuncPara();
        name = "main";
        IRInstr.uniins.add(new FuncEndInstr());
        for (int i = 0; i < IRInstr.uniins.size(); ++ i) {
            IRInstr now = IRInstr.uniins.get(i);
            if (now instanceof RegTransInstr) {
                fun.add(((RegTransInstr) now).dest.getId());
                if (((RegTransInstr) now).src instanceof VirtualReg) {
                    fun.add(((RegTransInstr) now).src.getId());
                }
            }
            else if (now instanceof Save) {
                if (((Save) now).addr instanceof VirtualReg) {
                    fun.add(((Save) now).addr.getId());
                }
                if(((Save) now).val instanceof VirtualReg) {
                    fun.add(((Save) now).val.getId());
                }
            }
            else if (now instanceof Load) {
                if (((Load) now).addr instanceof VirtualReg) {
                    fun.add(((Load) now).addr.getId());
                }
                fun.add(((Load) now).dest.getId());
            }
            else if (now instanceof Alloc) {
                fun.add(((Alloc)now).reg.getId());
                if (((Alloc) now).size instanceof VirtualReg) {
                    fun.add(((Alloc) now).size.getId());
                }
            }
            else if (now instanceof Branch) {
                fun.add(((Branch) now).cond.getId());
            }
            else if (now instanceof RetInstr) {
                fun.add(((RetInstr) now).ret.getId());
            }
            else if (now instanceof NormalFuncCall) {
                fun.add(((NormalFuncCall) now).dest.getId());
            }
            else if (now instanceof EqualTo) {
                fun.add(((EqualTo) now).dest.getId());
            }
            else if (now instanceof GreaterThan) {
                fun.add(((GreaterThan) now).dest.getId());
            }
            else if (now instanceof GreaterEqual) {
                fun.add(((GreaterEqual) now).dest.getId());
            }
            else if (now instanceof LessThan) {
                fun.add(((LessThan) now).dest.getId());
            }
            else if (now instanceof LessEqual) {
                fun.add(((LessEqual) now).dest.getId());
            }
            else if (now instanceof NotEqualTo) {
                fun.add(((NotEqualTo) now).dest.getId());
            }
            else if (now instanceof And) {
                fun.add(((And) now).dest.getId());
            }
            else if (now instanceof Left) {
                fun.add(((Left) now).dest.getId());
            }
            else if (now instanceof Not) {
                fun.add(((Not) now).dest.getId());
            }
            else if (now instanceof Or) {
                fun.add(((Or) now).dest.getId());
            }
            else if (now instanceof Right) {
                fun.add(((Right) now).dest.getId());
            }
            else if (now instanceof Xor) {
                fun.add(((Xor) now).dest.getId());
            }
            else if (now instanceof Add) {
                fun.add(((Add) now).dest.getId());
            }
            else if (now instanceof Div) {
                fun.add(((Div) now).dest.getId());
            }
            else if (now instanceof Mul) {
                fun.add(((Mul) now).dest.getId());
            }
            else if (now instanceof Neg) {
                fun.add(((Neg) now).dest.getId());
            }
            else if (now instanceof Rem) {
                fun.add(((Rem) now).dest.getId());
            }
            else if (now instanceof Sub) {
                fun.add(((Sub) now).dest.getId());
            }
            else if (now instanceof BuildinFuncCall) {
                if(!((BuildinFuncCall) now).isVoid)
                    fun.add(((BuildinFuncCall) now).dest.getId());
            }
            else if (now instanceof FuncEndInstr) {
                spy = false;
                //System.out.println(name + " " + fun.total);
                func.put(name, fun);
            }

        }

        for (int i = 0; i < IRInstr.instrs.size(); ++ i) {
            if (IRInstr.instrs.get(i) instanceof FuncDeclInstr) {
                fun = new FuncPara();
                name = ((Label)IRInstr.instrs.get(i + 1)).body.name;
                spy = true;
                for (int j = 0; j < ((FuncDeclInstr)IRInstr.instrs.get(i)).paraList.size(); ++ j) {
                    fun.add(((FuncDeclInstr)IRInstr.instrs.get(i)).paraList.get(j).getId());
                }
            }
            if (spy) {
                IRInstr now = IRInstr.instrs.get(i);
                if (now instanceof RegTransInstr) {
                    fun.add(((RegTransInstr) now).dest.getId());
                    if (((RegTransInstr) now).src instanceof VirtualReg) {
                        fun.add(((RegTransInstr) now).src.getId());
                    }
                }
                else if (now instanceof Save) {
                    if (((Save) now).addr instanceof VirtualReg) {
                        fun.add(((Save) now).addr.getId());
                    }
                    if(((Save) now).val instanceof VirtualReg) {
                        fun.add(((Save) now).val.getId());
                    }
                }
                else if (now instanceof Load) {
                    if (((Load) now).addr instanceof VirtualReg) {
                        fun.add(((Load) now).addr.getId());
                    }
                    fun.add(((Load) now).dest.getId());
                }
                else if (now instanceof Alloc) {
                    fun.add(((Alloc)now).reg.getId());
                    if (((Alloc) now).size instanceof VirtualReg) {
                        fun.add(((Alloc) now).size.getId());
                    }
                }
                else if (now instanceof Branch) {
                    fun.add(((Branch) now).cond.getId());
                }
                else if (now instanceof RetInstr) {
                    fun.add(((RetInstr) now).ret.getId());
                }
                else if (now instanceof NormalFuncCall) {
                    fun.add(((NormalFuncCall) now).dest.getId());
                }
                else if (now instanceof EqualTo) {
                    fun.add(((EqualTo) now).dest.getId());
                }
                else if (now instanceof GreaterThan) {
                    fun.add(((GreaterThan) now).dest.getId());
                }
                else if (now instanceof GreaterEqual) {
                    fun.add(((GreaterEqual) now).dest.getId());
                }
                else if (now instanceof LessThan) {
                    fun.add(((LessThan) now).dest.getId());
                }
                else if (now instanceof LessEqual) {
                    fun.add(((LessEqual) now).dest.getId());
                }
                else if (now instanceof NotEqualTo) {
                    fun.add(((NotEqualTo) now).dest.getId());
                }
                else if (now instanceof And) {
                    fun.add(((And) now).dest.getId());
                }
                else if (now instanceof Left) {
                    fun.add(((Left) now).dest.getId());
                }
                else if (now instanceof Not) {
                    fun.add(((Not) now).dest.getId());
                }
                else if (now instanceof Or) {
                    fun.add(((Or) now).dest.getId());
                }
                else if (now instanceof Right) {
                    fun.add(((Right) now).dest.getId());
                }
                else if (now instanceof Xor) {
                    fun.add(((Xor) now).dest.getId());
                }
                else if (now instanceof Add) {
                    fun.add(((Add) now).dest.getId());
                }
                else if (now instanceof Div) {
                    fun.add(((Div) now).dest.getId());
                }
                else if (now instanceof Mul) {
                    fun.add(((Mul) now).dest.getId());
                }
                else if (now instanceof Neg) {
                    fun.add(((Neg) now).dest.getId());
                }
                else if (now instanceof Rem) {
                    fun.add(((Rem) now).dest.getId());
                }
                else if (now instanceof Sub) {
                    fun.add(((Sub) now).dest.getId());
                }
                else if (now instanceof BuildinFuncCall) {
                    if(!((BuildinFuncCall) now).isVoid)
                        fun.add(((BuildinFuncCall) now).dest.getId());
                }
                else if (now instanceof FuncEndInstr) {
                    spy = false;
                    //System.out.println(name + " " + fun.total);
                    func.put(name, fun);
                }
            }
        }

        System.out.print(".data\n");
        for (int i = 0; i < EvalListenerSecond.symbolTable.UniDecl.size(); ++ i) {
            System.out.print("Universe__" + EvalListenerSecond.symbolTable.UniDecl.get(i).name + ": .word 0\n");
        }

        for (int i = 0; i < StringConstant.str.size(); ++ i) {
            System.out.println(".word " + ((int)StringConstant.len.get(i) - 2));
            System.out.println("Universe__str" + i + ": .asciiz " + StringConstant.str.get(i));
            System.out.println(".align 2");
        }

        System.out.println(".text\n");
        System.out.print("main:\n");
        System.out.println("subu $sp $sp " + Integer.toString(((FuncPara)func.get("main")).total * 4));
        System.out.println("sw $ra 0($sp)");
        for (int i = 0; i < IRInstr.uniins.size(); ++ i) {
            if (IRInstr.uniins.get(i) instanceof FuncEndInstr)
                continue;
            instr(IRInstr.uniins.get(i), "main");
        }
        System.out.println("subu $sp $sp " + ((FuncPara)func.get("main_entry")).total * 4);
        System.out.println("j main_entry");
        System.out.println("addu $sp $sp " + ((FuncPara)func.get("main_entry")).total * 4);
        System.out.println("lw $ra 0($sp)");
        System.out.println("jr $ra");
        String cur_func = "";
        for (int i = 0; i < IRInstr.instrs.size(); ++ i) {
            if (IRInstr.instrs.get(i) instanceof FuncDeclInstr) {
                cur_func = ((Label) IRInstr.instrs.get(i + 1)).body.name;
            };
            instr(IRInstr.instrs.get(i), cur_func);
        }
    }

    public static void main(String[] args) throws IOException {
        InputStream is = System.in;
        //InputStream is = new FileInputStream("TestCases/passed/basicopt1-5100309127-hetianxing.mx");
        ANTLRInputStream input = new ANTLRInputStream(is);
        MParserLexer lexer = new MParserLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MParserParser parser = new MParserParser(tokens);
        ParseTree tree = parser.program();
        ParseTreeWalker walker = new ParseTreeWalker();
        //try {
        /**
         * First Time
         */
        EvalListenerFirst first = new EvalListenerFirst();
        walker.walk(first, tree);
        /**
         * Second Time
         */
        EvalListenerSecond second = new EvalListenerSecond();
        walker.walk(second, tree);
        /**
         * Third Time
         */
        EvalListenerThird third = new EvalListenerThird();
        walker.walk(third, tree);
        //}
        //catch (Exception | Error error) {
        //System.exit(1);
        //}
        //to IR to Mips
        EvalListenerThird.root.translate(".");
        //PrintIR();
        toMips();

        System.exit(0);
        //System.out.print("+1s!");
    }
}
