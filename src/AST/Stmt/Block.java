package AST.Stmt;

import AST.Decl.VarDecl;
import Exception.CompilationError;
import IR.IRRegister.IRRegister;
import Type.Type;
import Type.VoidType;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class Block extends Stmt {
    public boolean isDecl;
    public VarDecl decl;
    public Stmt statement;
    public Type returnType;
    public boolean BorC;
    public Block tail;

    public Block () {
        this.isDecl = false;
        this.decl = null;
        this.statement = null;
        this.BorC = false;
        this.tail = null;
    }

    public boolean check() {
        if (statement instanceof NullStmt && !(returnType instanceof VoidType)) {
            throw new CompilationError("No Return!");
        }
        if (statement instanceof JumpStmt && ((JumpStmt) statement).type == 3 && !(((JumpStmt) statement).body.getType().equals(returnType))) {
            throw new CompilationError("Return a Wrong Type!");
        }
        return true;
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    public IRRegister translate (String frame) {
        if (isDecl) {
            decl.translate(frame);
        }
        else {
            statement.translate(frame);
        }
        if (tail != null) {
            tail.translate(frame);
        }
        return null;
    }
}
