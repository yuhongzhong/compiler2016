package AST.Stmt;

import AST.Stmt.Expr.Expr;
import IR.IRInstruction.IRInstr;
import IR.IRInstruction.JumpInstr.Branch;
import IR.IRInstruction.JumpInstr.Jump;
import IR.IRInstruction.Label;
import IR.IRRegister.IRRegister;
import IR.IRRegister.ImmediateNum;
import IR.IRRegister.Target;
import IR.IRRegister.VirtualReg.VirtualReg;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class SelectionStmt extends Stmt {
    public Expr condition;
    public boolean isElse = false;
    public Block First, Second;

    public SelectionStmt () {
        this.isElse = false;
        this.First = new Block();
        this.Second = new Block();
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    public IRRegister translate (String frame) {
        IRRegister cond = condition.translate(frame);
        if (cond instanceof ImmediateNum) {
            if (((ImmediateNum) cond).value == 0) {
                if (isElse) {
                    if (Second != null)
                        Second.translate(frame + "if.");
                }
            }
            else {
                if (First != null)
                    First.translate(frame + "if.");
            }
        }
        else {
            Target ift = new Target("if_true");
            Target iff = new Target("if_false");
            Target afi = new Target("after_if");
            Label ifTrue = new Label(ift);
            Label ifFalse = new Label(iff);
            Label afterIf = new Label(afi);
            Branch body = new Branch((VirtualReg) cond, ift, iff);
            IRInstr.instrs.add(body);
            IRInstr.instrs.add(ifTrue);
            if (First != null)
                First.translate(frame + "if.");
            Jump endIf = new Jump(afterIf.body);
            IRInstr.instrs.add(endIf);
            IRInstr.instrs.add(ifFalse);
            if (isElse && Second != null)
                Second.translate(frame + "else.");
            IRInstr.instrs.add(endIf);
            IRInstr.instrs.add(afterIf);
        }
        return null;
    }
}
