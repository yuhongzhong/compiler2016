package AST.Stmt;

import AST.Stmt.Expr.Expr;
import IR.IRInstruction.IRInstr;
import IR.IRInstruction.JumpInstr.Branch;
import IR.IRInstruction.JumpInstr.Jump;
import IR.IRInstruction.Label;
import IR.IRRegister.IRRegister;
import IR.IRRegister.ImmediateNum;
import IR.IRRegister.Target;
import IR.IRRegister.VirtualReg.VirtualReg;

import java.util.ArrayList;

/**
 * Created by yhzmiao on 16/4/6.
 * true for While
 * false for for
 */
public class IterationStmt extends Stmt {
    public boolean type = false;
    public Expr first, second, third;
    public Block body;

    public static ArrayList iterType = new ArrayList();
    public static ArrayList iterID = new ArrayList();

    public IterationStmt() {
        this.type = false;
        this.first = null;
        this.second = null;
        this.third = null;
        this.body = null;
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    @Override
    public IRRegister translate(String frame) {
        if (type) {
            Target while_cond = new Target("while_cond");
            Target while_loop = new Target("while_loop");
            Target after_while = new Target("after_while");

            iterType.add(0);
            iterID.add(while_cond.id);

            IRInstr.instrs.add(new Jump(while_cond));
            IRInstr.instrs.add(new Label(while_cond));

            IRRegister cond = second.translate(frame + "while.");
            if (cond instanceof ImmediateNum) {
                if (((ImmediateNum) cond).value == 0) {
                    Jump tar = new Jump(after_while);
                    IRInstr.instrs.add(tar);
                }
                else {
                    Jump tar = new Jump(while_loop);
                    IRInstr.instrs.add(tar);
                }
            }
            else if (cond instanceof VirtualReg) {
                Branch br = new Branch((VirtualReg) cond, while_loop, after_while);
                IRInstr.instrs.add(br);
            }

            IRInstr.instrs.add(new Label(while_loop));
            body.translate(frame + "while.");
            IRInstr.instrs.add(new Jump(while_cond));

            IRInstr.instrs.add(new Label(after_while));

            iterType.remove(iterType.size() - 1);
            iterID.remove(iterID.size() - 1);
        }
        else {
            Target for_init = new Target("for_init");
            Target for_cond = new Target("for_cond");
            Target for_loop = new Target("for_loop");
            Target for_step = new Target("for_step");
            Target after_for = new Target("after_for");

            iterType.add(1);
            iterID.add(for_cond.id);

            IRInstr.instrs.add(new Jump(for_init));
            IRInstr.instrs.add(new Label(for_init));
            if (first != null)
                first.translate(frame);

            IRInstr.instrs.add(new Jump(for_cond));
            IRInstr.instrs.add(new Label(for_cond));

            if (second != null) {
                IRRegister cond = second.translate(frame);
                if (cond instanceof ImmediateNum) {
                    if (((ImmediateNum) cond).value == 0) {
                        Jump tar = new Jump(after_for);
                        IRInstr.instrs.add(tar);
                    } else {
                        Jump tar = new Jump(for_loop);
                        IRInstr.instrs.add(tar);
                    }
                } else if (cond instanceof VirtualReg) {
                    Branch br = new Branch((VirtualReg) cond, for_loop, after_for);
                    IRInstr.instrs.add(br);
                }
            }

            IRInstr.instrs.add(new Label(for_loop));
            body.translate(frame + "for.");
            IRInstr.instrs.add(new Jump(for_step));
            IRInstr.instrs.add(new Label(for_step));
            if (third != null)
                third.translate(frame);
            IRInstr.instrs.add(new Jump(for_cond));

            IRInstr.instrs.add(new Label(after_for));

            iterType.remove(iterType.size() - 1);
            iterID.remove(iterID.size() - 1);
        }
        return null;
    }
}
