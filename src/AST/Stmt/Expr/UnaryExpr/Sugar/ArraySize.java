package AST.Stmt.Expr.UnaryExpr.Sugar;

import AST.Stmt.Expr.Expr;
import IR.IRInstruction.FuncCallInstr.BuildinFuncCall;
import IR.IRInstruction.IRInstr;
import IR.IRRegister.IRRegister;
import Type.IntType;
import Type.Type;

/**
 * Created by yhzmiao on 16/5/10.
 */
public class ArraySize extends Sugar {

    public Expr body;

    public ArraySize (Expr body) {
        this.body = body;
    }

    public Type getType () {
        return IntType.instance;
    }

    public String toString (int indent) {
        return null;
    }

    public boolean check() {
        return true;
    }

    public boolean isLValue () {
        return false;
    }

    public IRRegister translate(String frame) {
        BuildinFuncCall now = new BuildinFuncCall("func__array.size", false);
        now.list.add(body.translate(frame));
        IRInstr.AddIns(now, frame);
        return now.dest;
    }
}
