package AST.Stmt.Expr.UnaryExpr.Sugar;

import AST.Stmt.Expr.Expr;
import IR.IRInstruction.FuncCallInstr.BuildinFuncCall;
import IR.IRInstruction.IRInstr;
import IR.IRRegister.IRRegister;
import Type.StringType;
import Type.Type;

/**
 * Created by yhzmiao on 16/5/10.
 */
public class StringSub extends Sugar {

    public Expr body, left, right;

    public StringSub (Expr body, Expr left, Expr right) {
        this.body = body;
        this.left = left;
        this.right = right;
    }

    public Type getType () {
        return StringType.instance;
    }

    public String toString (int indent) {
        return null;
    }

    public boolean check() {
        return true;
    }

    public boolean isLValue () {
        return false;
    }

    public IRRegister translate(String frame) {
        BuildinFuncCall now = new BuildinFuncCall("func__string.substring", false);
        now.list.add(body.translate(frame));
        now.list.add(left.translate(frame));
        now.list.add(right.translate(frame));
        IRInstr.AddIns(now, frame);
        return now.dest;
    }
}
