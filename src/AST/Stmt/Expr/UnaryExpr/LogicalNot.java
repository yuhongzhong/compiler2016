package AST.Stmt.Expr.UnaryExpr;

import AST.Stmt.Expr.Expr;
import Exception.CompilationError;
import IR.IRInstruction.IRInstr;
import IR.IRRegister.IRRegister;
import IR.IRRegister.ImmediateNum;
import IR.IRRegister.VirtualReg.VRegforVar;
import Type.BoolType;
import Type.Type;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class LogicalNot extends UnaryExpr {
    public Expr body;



    @Override
    public boolean isLValue() {
        return false;
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    @Override
    public Type getType() {
        return BoolType.instance;
    }

    @Override
    public boolean check() {
        if (body.getType() instanceof BoolType) {
            return true;
        }
        throw new CompilationError("Type Error!");
    }

    public IRRegister translate (String frame) {
        IRRegister bod = body.translate(frame);

        if(bod instanceof ImmediateNum) {
            ImmediateNum ret = new ImmediateNum(((ImmediateNum) bod).value == 0 ? 1 : 0);
            return ret;
        }

        VRegforVar dest = new VRegforVar();
        IR.IRInstruction.BitwiseInstr.Not ret = new IR.IRInstruction.BitwiseInstr.Not(dest, bod);

        IRInstr.AddIns(ret, frame);
        return dest;
    }
}
