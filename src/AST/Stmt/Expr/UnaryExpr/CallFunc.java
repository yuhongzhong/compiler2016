package AST.Stmt.Expr.UnaryExpr;

import AST.Stmt.Expr.VarExpr.ArgumentListExpr;
import Exception.CompilationError;
import IR.IRInstruction.FuncCallInstr.BuildinFuncCall;
import IR.IRInstruction.FuncCallInstr.NormalFuncCall;
import IR.IRInstruction.FuncCallInstr.VoidFuncCall;
import IR.IRInstruction.IRInstr;
import IR.IRRegister.IRRegister;
import Type.FunctionType;
import Type.Type;
import Type.VoidType;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class CallFunc extends UnaryExpr {
    public FunctionType func;
    public ArgumentListExpr arguementlist;

    @Override
    public boolean check() {
        if (func.typelist.size() != arguementlist.argumentlist.size())
            throw new CompilationError("Number of Arguments Unmatched!");
        for (int i = 0; i < arguementlist.argumentlist.size(); ++ i) {
            if (!arguementlist.argumentlist.get(i).getType().equals(func.typelist.get(i))) {
                throw new CompilationError("Type Error!");
            }
        }
        return true;
    }

    @Override
    public boolean isLValue() {
        return false;
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    @Override
    public Type getType() {
        return func.type;
    }

    public IRRegister translate (String frame) {
        if (func.name.equals("print")) {
            BuildinFuncCall body = new BuildinFuncCall("func__print", true);
            IRRegister toPrint = arguementlist.argumentlist.get(0).translate(frame);
            body.list.add(toPrint);
            body.isVoid = true;
            IRInstr.AddIns(body, frame);
            return null;
        }
        else if (func.name.equals("println")) {
            BuildinFuncCall body = new BuildinFuncCall("func__println", true);
            IRRegister toPrint = arguementlist.argumentlist.get(0).translate(frame);
            body.list.add(toPrint);
            body.isVoid = true;
            IRInstr.AddIns(body, frame);
            return null;
        }
        else if (func.name.equals("getString")) {
            BuildinFuncCall body = new BuildinFuncCall("func__getString", false);
            IRInstr.AddIns(body, frame);
            return body.dest;
        }
        else if (func.name.equals("getInt")) {
            BuildinFuncCall body = new BuildinFuncCall("func__getInt", false);
            IRInstr.AddIns(body, frame);
            return body.dest;
        }
        else if (func.name.equals("toString")) {
            BuildinFuncCall body = new BuildinFuncCall("func__toString", false);
            body.list.add(arguementlist.argumentlist.get(0).translate(frame));
            IRInstr.AddIns(body, frame);
            return body.dest;
        }
        if (func.type instanceof VoidType) {
            VoidFuncCall body = new VoidFuncCall(func.name);
            if (arguementlist != null)
                for (int i = 0; i < arguementlist.argumentlist.size(); ++ i) {
                    body.list.add(arguementlist.argumentlist.get(arguementlist.argumentlist.size() - i - 1).translate(frame));
                }
            IRInstr.AddIns(body, frame);
            return null;
        }
        else {
            NormalFuncCall body = new NormalFuncCall(func.name);
            if (arguementlist != null)
                for (int i = 0; i < arguementlist.argumentlist.size(); ++ i) {
                    body.list.add(arguementlist.argumentlist.get(arguementlist.argumentlist.size() - i - 1).translate(frame));
                }
            IRInstr.AddIns(body, frame);
            return body.dest;
        }
    }
}
