package AST.Stmt.Expr.UnaryExpr;

import AST.Stmt.Expr.Expr;
import Exception.CompilationError;
import IR.IRInstruction.ArithInstr.Add;
import IR.IRInstruction.IRInstr;
import IR.IRInstruction.MemAccInstr.Save;
import IR.IRInstruction.RegTransInstr.RegTransInstr;
import IR.IRRegister.IRRegister;
import IR.IRRegister.ImmediateNum;
import IR.IRRegister.VirtualReg.VRegforAddr;
import IR.IRRegister.VirtualReg.VRegforVar;
import Type.IntType;
import Type.Type;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class PostInc extends UnaryExpr {
    public Expr body;



    @Override
    public boolean isLValue() {
        return false;
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    @Override
    public Type getType() {
        return IntType.instance;
    }

    @Override
    public boolean check() {
        if (body.getType() instanceof IntType) {
            return true;
        }
        throw new CompilationError("Type Error!");
    }

    @Override
    public IRRegister translate(String frame) {
        IRRegister bod = body.translate(frame);

        if (bod instanceof ImmediateNum) {
            return bod;
        }

        VRegforVar tmp = new VRegforVar();
        RegTransInstr move = new RegTransInstr(tmp, bod);
        IRInstr.AddIns(move, frame);

        if (bod instanceof VRegforVar) {
            Add inc = new Add((VRegforVar)bod, bod, new ImmediateNum(1));
            IRInstr.AddIns(inc, frame);
        }
        else if (bod instanceof VRegforAddr){
            Add inc = new Add((VRegforAddr)bod, bod, new ImmediateNum(1));
            IRInstr.AddIns(inc, frame);
            IRInstr.AddIns(new Save(((VRegforAddr) bod).addr, bod, ((VRegforAddr) bod).offset), frame);
        }

        return tmp;
    }
}
