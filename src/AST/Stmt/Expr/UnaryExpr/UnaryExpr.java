package AST.Stmt.Expr.UnaryExpr;

import AST.Stmt.Expr.Expr;

/**
 * Created by yhzmiao on 16/4/6.
 */
public abstract class UnaryExpr extends Expr {
    public abstract boolean check();
}
