package AST.Stmt.Expr.UnaryExpr;

import AST.Stmt.Expr.Expr;
import Exception.CompilationError;
import IR.IRRegister.IRRegister;
import Type.IntType;
import Type.Type;

/**
 * Created by yhzmiao on 16/5/6.
 */
public class Positive extends UnaryExpr {
    public Expr body;

    @Override
    public Type getType() {
        return body.getType();
    }

    public boolean check() {
        if (body.getType() instanceof IntType) {
            return true;
        }
        throw new CompilationError("Type Error!");
    }

    public String toString (int indent) {
        return null;
    }

    public boolean isLValue () {
        return false;
    }

    public IRRegister translate (String frame) {
        IRRegister bod = body.translate(frame);
        return bod;
    }
}
