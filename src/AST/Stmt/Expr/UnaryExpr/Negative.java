package AST.Stmt.Expr.UnaryExpr;

import AST.Stmt.Expr.Expr;
import Exception.CompilationError;
import IR.IRInstruction.IRInstr;
import IR.IRRegister.IRRegister;
import IR.IRRegister.ImmediateNum;
import IR.IRRegister.VirtualReg.VRegforVar;
import Type.IntType;
import Type.Type;

/**
 * Created by yhzmiao on 16/5/6.
 */
public class Negative extends UnaryExpr {
    public Expr body;

    @Override
    public Type getType() {
        return body.getType();
    }

    public boolean check() {
        if (body.getType() instanceof IntType) {
            return true;
        }
        throw new CompilationError("Type Error!");
    }

    public String toString (int indent) {
        return null;
    }

    public boolean isLValue () {
        return false;
    }

    public IRRegister translate (String frame) {
        IRRegister bod = body.translate(frame);

        if(bod instanceof ImmediateNum) {
            ImmediateNum ret = new ImmediateNum(-((ImmediateNum) bod).value);
            return ret;
        }

        VRegforVar dest = new VRegforVar();
        IR.IRInstruction.ArithInstr.Neg ret = new IR.IRInstruction.ArithInstr.Neg(dest, bod);

        IRInstr.AddIns(ret, frame);
        return dest;
    }
}
