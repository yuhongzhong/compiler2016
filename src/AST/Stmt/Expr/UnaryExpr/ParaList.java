package AST.Stmt.Expr.UnaryExpr;

import IR.IRRegister.IRRegister;
import Type.Type;

import java.util.ArrayList;

/**
 * Created by yhzmiao on 16/4/9.
 */
public class ParaList extends UnaryExpr {
    public ArrayList<Type> list;

    public ParaList () {
        list = new ArrayList<>();
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    @Override
    public Type getType() {
        return null;
    }

    @Override
    public boolean check() {
        return false;
    }

    @Override
    public boolean isLValue() {
        return false;
    }

    @Override
    public IRRegister translate(String frame) {
        return null;
    }
}
