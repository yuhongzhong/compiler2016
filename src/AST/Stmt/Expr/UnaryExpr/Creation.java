package AST.Stmt.Expr.UnaryExpr;

import AST.Stmt.Expr.Expr;
import Exception.CompilationError;
import IR.IRInstruction.ArithInstr.Add;
import IR.IRInstruction.ArithInstr.Mul;
import IR.IRInstruction.IRInstr;
import IR.IRInstruction.MemAccInstr.Alloc;
import IR.IRInstruction.MemAccInstr.Save;
import IR.IRInstruction.RegTransInstr.RegTransInstr;
import IR.IRRegister.IRRegister;
import IR.IRRegister.ImmediateNum;
import IR.IRRegister.VirtualReg.VRegforVar;
import IR.IRRegister.VirtualReg.VirtualReg;
import Type.ArrayType;
import Type.ClassType;
import Type.Type;

/**
 * Created by yhzmiao on 16/4/7.
 */
public class Creation extends UnaryExpr {
    public Type type;
    public Expr dim;

    public Creation (Type type, Expr dim) {
        this.type = type;
        this.dim = dim;
    }

    @Override
    public boolean check() {
        if (type.isVal()) {
            return true;
        }
        throw new CompilationError("Unable to Create!");
    }

    @Override
    public boolean isLValue() {
        return false;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public String toString(int depth) {
        return null;
    }


    public IRRegister translate (String frame) {
        //System.out.println("spy");
        //System.out.println(type);
        if (type instanceof ClassType) {
            int size = ((ClassType) type).typelist.size() * 4 + 4;
            VRegforVar reg = IRRegister.newVarReg("tmp" + Integer.toString(++IRRegister.cur_tmp));
            Alloc ret = new Alloc(reg, new ImmediateNum(size));

            IRInstr.AddIns(ret, frame);

            IRInstr.AddIns(new Save(reg, new ImmediateNum(size), 0), frame);

            return reg;
        }
        else if (type instanceof ArrayType) {
            IRRegister size;
            IRRegister tmp = dim.translate(frame);
            IRRegister length;
            if (tmp instanceof ImmediateNum) {
                size = new ImmediateNum(((ImmediateNum) tmp).value * 4 + 4);
                length = new ImmediateNum(((ImmediateNum) tmp).value);
            } else {
                size = IRRegister.newVarReg("tmp" + Integer.toString(++IRRegister.cur_reg));
                IRInstr.AddIns(new RegTransInstr((VirtualReg) size, tmp), frame);
                length = IRRegister.newVarReg("tmp" + Integer.toString(++IRRegister.cur_reg));
                IRInstr.AddIns(new RegTransInstr((VRegforVar)length, size), frame);
                Mul mul = new Mul((VirtualReg) size, size, new ImmediateNum(4));
                Add add = new Add((VirtualReg) size, size, new ImmediateNum(4));
                IRInstr.AddIns(mul, frame);
                IRInstr.AddIns(add, frame);
            }
            VRegforVar reg = IRRegister.newVarReg("tmp" + Integer.toString(++IRRegister.cur_tmp));
            Alloc ret = new Alloc(reg, size);

            IRInstr.AddIns(ret, frame);

            IRInstr.AddIns(new Save(reg, length, 0), frame);
            IRInstr.AddIns(new Add(reg, reg, new ImmediateNum(4)), frame);

            return reg;
        }
        return null;
    }
}
