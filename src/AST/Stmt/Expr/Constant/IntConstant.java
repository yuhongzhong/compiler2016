package AST.Stmt.Expr.Constant;

import IR.IRRegister.IRRegister;
import IR.IRRegister.ImmediateNum;
import Type.IntType;
import Type.Type;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class IntConstant extends Constant {
    public int value;

    public IntConstant () {}

    public IntConstant (int value) {
        this.value = value;
    }

    @Override
    public boolean isLValue() {
        return super.isLValue();
    }

    @Override
    public Type getType() {
        return IntType.instance;
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    @Override
    public IRRegister translate(String frame) {
        return new ImmediateNum(value);
    }
}
