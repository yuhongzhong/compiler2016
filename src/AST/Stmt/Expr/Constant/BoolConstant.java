package AST.Stmt.Expr.Constant;

import IR.IRRegister.IRRegister;
import IR.IRRegister.ImmediateNum;
import Type.BoolType;
import Type.Type;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class BoolConstant extends Constant {
    public boolean value;

    public BoolConstant () {}

    public BoolConstant (boolean value) {
        this.value = value;
    }

    @Override
    public boolean isLValue() {
        return super.isLValue();
    }

    @Override
    public Type getType() {
        return BoolType.instance;
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    public IRRegister translate (String frame) {
        return new ImmediateNum(value ? 1 : 0);
    }
}
