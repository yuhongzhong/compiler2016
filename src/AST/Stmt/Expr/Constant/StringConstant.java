package AST.Stmt.Expr.Constant;

import IR.IRRegister.IRRegister;
import IR.IRRegister.VirtualReg.Address;
import Type.StringType;
import Type.Type;

import java.util.ArrayList;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class StringConstant extends Constant{

    public static ArrayList<String> str = new ArrayList<>();
    public static ArrayList len = new ArrayList();
    public String value;

    public StringConstant () {}

    public StringConstant (String value) {
        String fix = "";
        for (int i = 0; i < value.length(); ++ i) {
            if (value.charAt(i) == '\\' && (i + 1 < value.length() && value.charAt(i + 1) == '"'))
                continue;
            fix = fix + value.charAt(i);
        }
        this.value = value;
        str.add(value);
        len.add(fix.length());
    }

    @Override
    public boolean isLValue() {
        return super.isLValue();
    }

    @Override
    public Type getType() {
        return StringType.instance;
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    public IRRegister translate (String frame) {
        int spy = 0;
        for (int i = 0; i < str.size(); ++ i)
            if (str.get(i).equals(value)) {
                spy = i;
                break;
            }
        Address now = new Address("str" + Integer.toString(spy));
        return now;
    }
}
