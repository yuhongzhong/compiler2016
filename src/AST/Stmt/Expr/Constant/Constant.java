package AST.Stmt.Expr.Constant;

import AST.Stmt.Expr.Expr;

/**
 * Created by yhzmiao on 16/4/6.
 */
public abstract class Constant extends Expr {
    @Override
    public boolean isLValue() {
        return false;
    }
}
