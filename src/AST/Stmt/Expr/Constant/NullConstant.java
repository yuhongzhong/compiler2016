package AST.Stmt.Expr.Constant;

import IR.IRRegister.IRRegister;
import IR.IRRegister.ImmediateNum;
import Type.NullType;
import Type.Type;

/**
 * Created by yhzmiao on 16/4/9.
 */
public class NullConstant extends Constant {
    @Override
    public String toString(int depth) {
        return null;
    }

    @Override
    public boolean isLValue() {
        return super.isLValue();
    }

    @Override
    public Type getType() {
        return NullType.instance;
    }

    public IRRegister translate (String frame) {
        return new ImmediateNum(0);
    }
}
