package AST.Stmt.Expr.BinaryExpr;

import AST.Stmt.Expr.Expr;
import Exception.CompilationError;
import IR.IRInstruction.IRInstr;
import IR.IRInstruction.MemAccInstr.Save;
import IR.IRInstruction.RegTransInstr.RegTransInstr;
import IR.IRRegister.IRRegister;
import IR.IRRegister.VirtualReg.VRegforAddr;
import IR.IRRegister.VirtualReg.VirtualReg;
import Type.Type;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class AssignExpr extends BinaryExpr {
    public Expr left, right;

    @Override
    public boolean check() {
        if (!left.isLValue()) {
            throw new CompilationError("Cannot Assign!");
        }
        //System.out.print(left.getType());
        //System.out.print(right.getType());
        if (!right.getType().equals(left.getType())) {
            throw new CompilationError("Type Error!");
        }
        return true;
    }

    @Override
    public Type getType() {
        return left.getType();
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    public IRRegister translate (String frame) {
        VirtualReg lef = (VirtualReg) left.translate(frame);
        IRRegister rig = right.translate(frame);

        RegTransInstr body = new RegTransInstr(lef, rig);

        IRInstr.AddIns(body, frame);

        if (lef instanceof VRegforAddr) {
            IRInstr.AddIns(new Save(((VRegforAddr) lef).addr, lef, ((VRegforAddr) lef).offset), frame);
        }
        return lef;
    }
}
