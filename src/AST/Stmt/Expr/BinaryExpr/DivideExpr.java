package AST.Stmt.Expr.BinaryExpr;

import AST.Stmt.Expr.Expr;
import Exception.CompilationError;
import IR.IRInstruction.ArithInstr.Div;
import IR.IRInstruction.IRInstr;
import IR.IRRegister.IRRegister;
import IR.IRRegister.ImmediateNum;
import IR.IRRegister.VirtualReg.VRegforVar;
import Type.IntType;
import Type.Type;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class DivideExpr extends BinaryExpr {
    public Expr left, right;

    @Override
    public boolean check() {
        if (left.getType().equals(right.getType()) && left.getType() instanceof IntType) {
            /**
             *  if (right.value == 0)
             *      throw new CompilationError("Undefined Operation!");
             */
            return true;
        }
        throw new CompilationError("Cannot Divide!");
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    @Override
    public boolean isLValue() {
        return super.isLValue();
    }

    @Override
    public Type getType() {
        return IntType.instance;
    }

    public IRRegister translate (String frame) {
        IRRegister lef = left.translate(frame);
        IRRegister rig = right.translate(frame);

        if(lef instanceof ImmediateNum && rig instanceof ImmediateNum) {
            ImmediateNum ret = new ImmediateNum(((ImmediateNum) lef).value / ((ImmediateNum) rig).value);
            return ret;
        }

        VRegforVar dest = new VRegforVar();
        Div ret = new Div(dest, lef, rig);

        IRInstr.AddIns(ret, frame);
        return dest;
    }
}
