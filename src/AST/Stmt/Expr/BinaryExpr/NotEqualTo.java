package AST.Stmt.Expr.BinaryExpr;

import AST.Stmt.Expr.Expr;
import Exception.CompilationError;
import IR.IRInstruction.FuncCallInstr.BuildinFuncCall;
import IR.IRInstruction.IRInstr;
import IR.IRRegister.IRRegister;
import IR.IRRegister.ImmediateNum;
import IR.IRRegister.VirtualReg.VRegforVar;
import Type.BoolType;
import Type.StringType;
import Type.Type;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class NotEqualTo extends BinaryExpr {
    public Expr left, right;

    @Override
    public boolean check() {
        if (right.getType().equals(left.getType()))
            return true;
        throw new CompilationError("Cannot Compare!");
    }

    @Override
    public Type getType() {
        return BoolType.instance;
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    public IRRegister translate (String frame) {
        IRRegister lef = left.translate(frame);
        IRRegister rig = right.translate(frame);
        if (left.getType() instanceof StringType && right.getType() instanceof StringType) {
            BuildinFuncCall ret = new BuildinFuncCall("func__stringNeq", false);
            ret.list.add(lef);
            ret.list.add(rig);
            IRInstr.AddIns(ret, frame);
            return ret.dest;
        }

        if(lef instanceof ImmediateNum && rig instanceof ImmediateNum) {
            ImmediateNum ret = new ImmediateNum(((ImmediateNum) lef).value != ((ImmediateNum) rig).value ? 1 : 0);
            return ret;
        }

        VRegforVar dest = new VRegforVar();
        IR.IRInstruction.CondSetInstr.NotEqualTo ret = new IR.IRInstruction.CondSetInstr.NotEqualTo(dest, lef, rig);

        IRInstr.AddIns(ret, frame);
        return dest;
    }
}
