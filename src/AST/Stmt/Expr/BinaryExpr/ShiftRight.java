package AST.Stmt.Expr.BinaryExpr;

import AST.Stmt.Expr.Expr;
import Exception.CompilationError;
import IR.IRInstruction.BitwiseInstr.Right;
import IR.IRInstruction.IRInstr;
import IR.IRRegister.IRRegister;
import IR.IRRegister.ImmediateNum;
import IR.IRRegister.VirtualReg.VRegforVar;
import Type.IntType;
import Type.Type;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class ShiftRight extends BinaryExpr {
    public Expr left, right;

    @Override
    public boolean check() {
        if (left.getType().equals(right.getType()) && (left.getType() instanceof IntType))
            return true;
        throw new CompilationError("Cannot Shift!");
    }

    @Override
    public Type getType() {
        return left.getType();
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    public IRRegister translate (String frame) {
        IRRegister lef = left.translate(frame);
        IRRegister rig = right.translate(frame);

        if(lef instanceof ImmediateNum && rig instanceof ImmediateNum) {
            ImmediateNum ret = new ImmediateNum(((ImmediateNum) lef).value >> ((ImmediateNum) rig).value);
            return ret;
        }

        VRegforVar dest = new VRegforVar();
        Right ret = new Right(dest, lef, rig);

        IRInstr.AddIns(ret, frame);
        return dest;
    }
}
