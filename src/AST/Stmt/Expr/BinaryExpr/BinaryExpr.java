package AST.Stmt.Expr.BinaryExpr;


import AST.Stmt.Expr.Expr;

/**
 * Created by yhzmiao on 16/4/6.
 */
public abstract class BinaryExpr extends Expr{
    public abstract boolean check();
    @Override
    public boolean isLValue() {
        return false;
    }
}
