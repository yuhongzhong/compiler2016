package AST.Stmt.Expr.BinaryExpr;

import AST.Stmt.Expr.Expr;
import Exception.CompilationError;
import IR.IRInstruction.BitwiseInstr.And;
import IR.IRInstruction.IRInstr;
import IR.IRInstruction.JumpInstr.Branch;
import IR.IRInstruction.JumpInstr.Jump;
import IR.IRInstruction.Label;
import IR.IRInstruction.RegTransInstr.RegTransInstr;
import IR.IRRegister.IRRegister;
import IR.IRRegister.ImmediateNum;
import IR.IRRegister.Target;
import IR.IRRegister.VirtualReg.VRegforVar;
import IR.IRRegister.VirtualReg.VirtualReg;
import Type.BoolType;
import Type.Type;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class LogicalAnd extends BinaryExpr {
    public Expr left, right;

    @Override
    public Type getType() {
        return BoolType.instance;
    }

    @Override
    public boolean isLValue() {
        return super.isLValue();
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    @Override
    public boolean check() {
        if (left.getType().equals(right.getType()) && (left.getType() instanceof BoolType)) {
            return true;
        }
        throw new CompilationError("Cannot LogicalAnd!");
    }

    public IRRegister translate (String frame) {
        IRRegister lef = left.translate(frame);
        if (lef instanceof ImmediateNum) {
            if (((ImmediateNum) lef).value == 0)
                return new ImmediateNum(0);
            else
                return right.translate(frame);
        }

        Target ift = new Target("if_true");
        Target iff = new Target("if_false");
        Target afi = new Target("after_if");
        Label ifTrue = new Label(ift);
        Label ifFalse = new Label(iff);
        Label afterIf = new Label(afi);

        VRegforVar ret = IRRegister.newVarReg("tmp" + Integer.toString(++ IRRegister.cur_tmp));
        Branch body = new Branch((VirtualReg) lef, ift, iff);
        IRInstr.AddIns(body, frame);
        IRInstr.AddIns(ifTrue, frame);
        IRInstr.AddIns(new RegTransInstr(ret, right.translate(frame)), frame);
        Jump endIf = new Jump(afterIf.body);
        IRInstr.AddIns(endIf, frame);
        IRInstr.AddIns(ifFalse, frame);
        IRInstr.AddIns(new RegTransInstr(ret, new ImmediateNum(0)), frame);
        IRInstr.instrs.add(endIf);
        IRInstr.instrs.add(afterIf);

        return ret;
    }

    /*
    public IRRegister translate (String frame) {
        IRRegister lef = left.translate(frame);
        IRRegister rig = right.translate(frame);

        if(lef instanceof ImmediateNum && rig instanceof ImmediateNum) {
            ImmediateNum ret = new ImmediateNum(((ImmediateNum) lef).value & ((ImmediateNum) rig).value);
            return ret;
        }

        VRegforVar dest = new VRegforVar();
        And ret = new And(dest, lef, rig);

        IRInstr.AddIns(ret, frame);
        return dest;
    }
    */
}
