package AST.Stmt.Expr;

import AST.Stmt.Stmt;
import Type.Type;

/**
 * Created by yhzmiao on 16/4/6.
 */

public abstract class Expr extends Stmt {
    public abstract boolean isLValue();
    public abstract Type getType();
}
