package AST.Stmt.Expr.VarExpr;

import AST.Stmt.Expr.Expr;
import IR.IRRegister.IRRegister;
import Type.Type;

import java.util.ArrayList;

/**
 * Created by yhzmiao on 16/5/9.
 */
public class ArgumentListExpr extends VarExpr {

    public ArrayList <Expr> argumentlist;

    public ArgumentListExpr () {
        argumentlist = new ArrayList<>();
    }

    public Type getType () {
        return null;
    }

    public String toString (int indent) {
        return null;
    }

    public boolean check() {
        return true;
    }

    public boolean isLValue () {
        return false;
    }

    public IRRegister translate (String frame) {
        return null;
    }
}
