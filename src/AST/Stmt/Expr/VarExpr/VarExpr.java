package AST.Stmt.Expr.VarExpr;

import AST.Stmt.Expr.Expr;

/**
 * Created by yhzmiao on 16/4/6.
 */
public abstract class VarExpr extends Expr {

    public String name;

    public abstract boolean check();
    @Override
    public boolean isLValue() {
        return true;
    }
}
