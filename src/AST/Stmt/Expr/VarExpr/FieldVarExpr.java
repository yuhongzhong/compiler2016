package AST.Stmt.Expr.VarExpr;

import AST.Stmt.Expr.Expr;
import Exception.CompilationError;
import IR.IRInstruction.IRInstr;
import IR.IRInstruction.MemAccInstr.Load;
import IR.IRRegister.IRRegister;
import IR.IRRegister.VirtualReg.VRegforAddr;
import IR.IRRegister.VirtualReg.VirtualReg;
import Type.ClassType;
import Type.Type;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class FieldVarExpr extends VarExpr {
    public ClassType type;
    public Expr pointer;
    public String name;

    public FieldVarExpr (ClassType type, String name) {
        this.type = type;
        this.name = name;
    }

    @Override
    public boolean check() {
        for (int i = 0; i < type.namelist.size(); ++ i) {
            if (type.namelist.get(i).equals(name)) {
                return true;
            }
        }
        throw new CompilationError("Member does not Exist!");
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    @Override
    public boolean isLValue() {
        return super.isLValue();
    }

    @Override
    public Type getType() {
        int ret;
        for (ret = 0; ret < type.namelist.size(); ++ ret) {
            //System.out.println(name);
            if (type.namelist.get(ret).equals(name))
                return type.typelist.get(ret);
        }
        throw new CompilationError("Member does not Exist!");
    }

    public IRRegister translate (String frame) {
        IRRegister poi = pointer.translate(frame);
        int loc = 0;
        for (int i = 0; i < type.namelist.size(); ++ i) {
            if(type.namelist.get(i).equals(name)) {
                loc = 4 * i + 4;
            }
        }
        name = ((VarExpr)pointer).name + name;
        VRegforAddr ret = IRRegister.newAddrReg(name, (VirtualReg) poi, loc);
        Load getval = new Load(poi, loc, ret);

        IRInstr.AddIns(getval, frame);
        return ret;

    }
}
