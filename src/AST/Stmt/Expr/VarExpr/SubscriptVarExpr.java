package AST.Stmt.Expr.VarExpr;

import AST.Stmt.Expr.Expr;
import Exception.CompilationError;
import IR.IRInstruction.ArithInstr.Add;
import IR.IRInstruction.ArithInstr.Mul;
import IR.IRInstruction.IRInstr;
import IR.IRInstruction.MemAccInstr.Load;
import IR.IRInstruction.RegTransInstr.RegTransInstr;
import IR.IRRegister.IRRegister;
import IR.IRRegister.ImmediateNum;
import IR.IRRegister.VirtualReg.VRegforAddr;
import IR.IRRegister.VirtualReg.VRegforVar;
import IR.IRRegister.VirtualReg.VirtualReg;
import Type.ArrayType;
import Type.Type;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class SubscriptVarExpr extends VarExpr {
    public ArrayType type;
    public int dim;
    public String name;
    public Expr pointer, id;

    @Override
    public boolean check() {
        if (dim == type.dim) {
            return true;
        }
        throw new CompilationError("Dim Unmatched!");
    }

    @Override
    public boolean isLValue() {
        return super.isLValue();
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    @Override
    public Type getType() {
        if (type.dim == dim) {
            return type.pointer;
        }
        return new ArrayType(type.pointer, type.dim - dim);
    }

    public IRRegister translate (String frame) {
        //System.out.println(dim);
        IRRegister poi = this.pointer.translate(frame);
        int loc = 0;
        IRRegister expr = id.translate(frame);
        if (expr instanceof ImmediateNum) {
            loc =  ((ImmediateNum) expr).value * 4;


            name = ((VarExpr)pointer).name + '[' + ']';
            //VRegforAddr ret = IRRegister.newAddrReg(name, (VirtualReg) poi, loc);
            VRegforAddr ret = IRRegister.newAddrReg("tmp" + Integer.toString(++ IRRegister.cur_tmp), (VirtualReg) poi, loc);
            Load getval = new Load(poi, loc, ret);

            IRInstr.AddIns(getval, frame);
            return ret;
        }
        else {
            VRegforVar tmp = IRRegister.newVarReg("tmp" + Integer.toString(++ IRRegister.cur_tmp));
            IRInstr.AddIns(new RegTransInstr(tmp, expr), frame);
            Mul tmp0 = new Mul((VirtualReg)tmp, tmp, new ImmediateNum(4));
            IRInstr.AddIns(tmp0, frame);
            VRegforVar point = IRRegister.newVarReg("tmp" + Integer.toString(++ IRRegister.cur_tmp));
            IRInstr.AddIns(new RegTransInstr(point, poi), frame);
            IRInstr.AddIns(new Add(point, point, tmp), frame);

            name = ((VarExpr)pointer).name + '[' + ']';
            //VRegforAddr ret = IRRegister.newAddrReg(name, (VirtualReg) point, loc);
            VRegforAddr ret = IRRegister.newAddrReg("tmp" + Integer.toString(++ IRRegister.cur_tmp), (VirtualReg)point, loc);
            Load getval = new Load(point, loc, ret);

            IRInstr.AddIns(getval, frame);
            return ret;
        }
    }
}
