package AST.Stmt.Expr.VarExpr;

import IR.IRInstruction.IRInstr;
import IR.IRInstruction.MemAccInstr.Load;
import IR.IRRegister.IRRegister;
import IR.IRRegister.VirtualReg.Address;
import IR.IRRegister.VirtualReg.VRegforAddr;
import IR.IRRegister.VirtualReg.VRegforVar;
import Type.Type;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class SimpleVarExpr extends VarExpr {
    public Type type;
    public String name;

    public SimpleVarExpr (Type type, String name) {
        this.type = type;
        this.name = name;
    }

    public boolean check() {
        return true;
    }

    @Override
    public boolean isLValue() {
        return super.isLValue();
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    @Override
    public IRRegister translate(String frame) {
        String cur_f = frame;
        //if (IRRegister.regList.containsKey(".main.for.j")) System.out.println("spy");
        boolean spy = false;
        if (!(cur_f.equals("."))) {
            if (IRRegister.regList.containsKey(frame + name)) {
                return new VRegforVar((int)IRRegister.regList.get(frame + name));
            }
            for (int i = cur_f.length() - 2; i >= 0; -- i) {
                if (cur_f.charAt(i) == '.') {
                    cur_f = cur_f.substring(0, i + 1);
                    if (IRRegister.regList.containsKey(cur_f + name))
                        return (new VRegforVar((int) IRRegister.regList.get(cur_f + name)));
                }
            }
        }
        Address address = new Address(name);
        VRegforAddr tmp = IRRegister.newAddrReg("tmp" + Integer.toString(++ IRRegister.cur_tmp), address, 0);
        Load load = new Load(address, 0, tmp);

        IRInstr.AddIns(load, frame);
        return tmp;
    }
}
