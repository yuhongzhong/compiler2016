package AST.Stmt;

import IR.IRRegister.VirtualReg.VirtualReg;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class NullStmt extends Stmt{
    @Override
    public String toString(int depth) {
        return null;
    }

    public VirtualReg translate (String frame) {
        return null;
    }
}
