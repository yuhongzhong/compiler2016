package AST.Stmt;

import AST.Stmt.Expr.Expr;
import IR.IRRegister.IRRegister;

/**
 * Created by yhzmiao on 16/5/7.
 */
public class ExprStmt extends Stmt {

    public Expr body;

    public ExprStmt (Expr body) {
        this.body = body;
    }

    public String toString (int indent) {
        return null;
    }

    public IRRegister translate (String frame) {
        body.translate(frame);
        return null;
    }
}
