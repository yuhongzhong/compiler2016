package AST.Stmt;

import AST.Stmt.Expr.Expr;
import IR.IRInstruction.IRInstr;
import IR.IRInstruction.JumpInstr.Jump;
import IR.IRInstruction.JumpInstr.RetInstr;
import IR.IRRegister.IRRegister;
import IR.IRRegister.Target;
import IR.IRRegister.VirtualReg.VirtualReg;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class JumpStmt extends Stmt {
    public int type;
    public Expr body;

    @Override
    public String toString(int depth) {
        return null;
    }

    /**
     * break 1
     * continue 2
     * return 3
     * @return
     */

    public VirtualReg translate (String frame) {
        if (type == 1) {
            //Target target = new Target("for_step");
            //Jump body = new Jump(target);
            //IRInstr.instrs.add(body);
            if((int)IterationStmt.iterType.get(IterationStmt.iterType.size() - 1) == 1) {
                Target target = new Target("after_for", (int)IterationStmt.iterID.get(IterationStmt.iterID.size() - 1) + 3);
                IRInstr.instrs.add(new Jump(target));
            }
            else if((int)IterationStmt.iterType.get(IterationStmt.iterType.size() - 1) == 0) {
                Target target = new Target("after_while", (int)IterationStmt.iterID.get(IterationStmt.iterID.size() - 1) + 2);
                IRInstr.instrs.add(new Jump(target));
            }
            return null;
        }
        else if (type == 2) {
            //Target target = new Target("for_after");
            //Jump body = new Jump(target);
            //IRInstr.instrs.add(body);
            if((int)IterationStmt.iterType.get(IterationStmt.iterType.size() - 1) == 1) {
                Target target = new Target("for_step", (int)IterationStmt.iterID.get(IterationStmt.iterID.size() - 1) + 2);
                IRInstr.instrs.add(new Jump(target));
            }
            else if((int)IterationStmt.iterType.get(IterationStmt.iterType.size() - 1) == 0) {
                Target target = new Target("while_loop", (int)IterationStmt.iterID.get(IterationStmt.iterID.size() - 1) + 1);
                IRInstr.instrs.add(new Jump(target));
            }
            return null;
        }
        else if (type == 3) {
            IRRegister ret;
            if (body != null) ret = body.translate(frame);
            else ret = null;
            RetInstr body = new RetInstr(ret);
            IRInstr.instrs.add(body);
        }
        return null;
    }
}
