package AST.Decl;

import AST.Stmt.Block;
import IR.IRInstruction.FuncDeclInstr;
import IR.IRInstruction.FuncEndInstr;
import IR.IRInstruction.IRInstr;
import IR.IRInstruction.Label;
import IR.IRRegister.IRRegister;
import IR.IRRegister.Target;
import Type.FunctionType;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class FuncDecl extends Decl {
    public FunctionType type;
    public String name;
    public Block body;

    public FuncDecl (FunctionType type, String name, Block body) {
        this.type = type;
        this.name = name;
        this.body = body;
    }

    public IRRegister translate (String frame) {
        FuncDeclInstr begin = new FuncDeclInstr();
        FuncEndInstr end = new FuncEndInstr();

        frame += this.name + ".";

        begin.name = name;

        for (int i = 0; i < this.type.namelist.size(); ++ i) {
            String tmp = this.type.namelist.get(i);
            begin.paraList.add(IRRegister.newVarReg(frame + tmp));
        }

        IRInstr.instrs.add(begin);
        Target enter = new Target(name + "_entry", 0);
        IRInstr.instrs.add(new Label(enter));

        body.translate(frame);
        IRInstr.instrs.add(end);
        return null;
    }

    @Override
    public String toString(int depth) {
        String ret = "";
        ret += indent(depth) + type.toString(depth) + name + "(";
        for (int i = 0; i < type.namelist.size(); ++ i) {
            ret += type.typelist.get(i) + " " + type.namelist.get(i);
            if (i < type.namelist.size() - 1)
                ret += ", ";
        }
        ret += ") {\n";
        return ret + body.toString(depth) + indent(depth) + "}\n";
    }

    @Override
    public boolean check() {
        return true;
    }
}
