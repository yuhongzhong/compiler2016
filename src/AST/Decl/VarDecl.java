package AST.Decl;

import AST.Stmt.Expr.Expr;
import Exception.CompilationError;
import IR.IRInstruction.IRInstr;
import IR.IRInstruction.MemAccInstr.Save;
import IR.IRInstruction.RegTransInstr.RegTransInstr;
import IR.IRRegister.IRRegister;
import IR.IRRegister.VirtualReg.Address;
import IR.IRRegister.VirtualReg.VirtualReg;
import Type.Type;

/**
 * Created by yhzmiao on 16/4/5.
 */
public class VarDecl extends Decl {

    public Type type;
    public String name;
    public boolean init = false;
    public Expr value;

    @Override
    public boolean check() {
        if (!init) {
            return true;
        }
        if (type.equals(value.getType())) {
            return true;
        }
        if (!(type.isVal())) {
            throw new CompilationError("Unable to Declare!");
        }
        throw new CompilationError("Type Unmatched!");
    }

    @Override
    public String toString(int depth) {
        return indent(depth) + type.toString() + " " + name + (init ? "= " + value.toString() : "") + ";\n";
    }

    public IRRegister translate (String frame) {
        if (frame.equals(".")) {
            Address dest = new Address(name);
            if (init) {
                IRRegister val = value.translate(frame);
                IRInstr.uniins.add(new Save(dest, val, 0));
            }
        }
        else {
            VirtualReg dest = IRRegister.newVarReg(frame + name);
            if (init) {
                IRRegister val = value.translate(frame);
                RegTransInstr move = new RegTransInstr(dest, val);
                IRInstr.instrs.add(move);
            }
        }
        return null;
    }
}
