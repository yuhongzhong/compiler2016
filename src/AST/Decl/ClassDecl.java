package AST.Decl;

import IR.IRRegister.IRRegister;
import Type.ClassType;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class ClassDecl extends Decl {
    public ClassType type;
    public String name;

    @Override
    public boolean check() {
        return true;
    }

    @Override
    public String toString(int depth) {
        String ret = indent(depth) + type.toString(depth) + " " + name + "{\n";
        for (int i = 0; i < type.namelist.size(); ++ i) {
            ret += indent(depth + 1) + type.typelist.get(i) + " " + type.namelist.get(i) + ";\n";
        }
        return ret + indent(depth) + "}\n";
    }

    public IRRegister translate (String frame) {
        return null;
    }
}
