package AST.Decl;

import AST.ASTNode;

/**
 * Created by yhzmiao on 16/4/5.
 */
public abstract class Decl extends ASTNode {
    public abstract boolean check();
}
