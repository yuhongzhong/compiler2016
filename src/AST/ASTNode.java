package AST;

import IR.IRRegister.IRRegister;

/**
 * Created by yhzmiao on 16/4/5.
 */
public abstract class ASTNode {

    public String indent(int depth) {
        String ret = "";
        for (int i = 0; i < depth; ++ i) {
            ret += " ";
        }
        return ret;
    }
    public abstract String toString (int depth);

    public abstract IRRegister translate (String frame);
}
