package AST.Prog;

import AST.ASTNode;
import AST.Decl.Decl;
import IR.IRRegister.IRRegister;

/**
 * Created by yhzmiao on 16/4/5.
 */
public class Prog extends ASTNode {
    Decl head;
    Prog tail;

    public Prog (Decl head, Prog tail) {
        this.head = head;
        this.tail = tail;
    }

    @Override
    public String toString(int depth) {
        return head.toString() + tail.toString();
    }

    public IRRegister translate (String frame) {
        //System.out.print("spy");
        head.translate(frame);
        if (tail != null)
            tail.translate(frame);
        return null;
    }
}
