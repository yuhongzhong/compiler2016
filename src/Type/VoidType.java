package Type;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class VoidType extends Type {
    public static VoidType instance = new VoidType();

    @Override
    public boolean isVal() {
        return false;
    }

    @Override
    public boolean equals(Type right) {
        return right instanceof VoidType;
    }

    @Override
    public String toString(int depth) {
        return indent(depth) + "void";
    }
}
