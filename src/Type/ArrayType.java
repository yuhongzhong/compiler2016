package Type;

/**
 * Created by yhzmiao on 16/4/3.
 */
public class ArrayType extends Type{
    public Type pointer;
    public int dim;
    String name;

    public ArrayType(Type pointer, int dim) {
        this.pointer = pointer;
        this.dim = dim;
    }

    @Override
    public boolean isVal() {
        return true;
    }

    @Override
    public boolean equals(Type right) {
        return (right instanceof ArrayType && pointer.equals(((ArrayType) right).pointer) && dim == ((ArrayType) right).dim);
    }

    @Override
    public String toString(int depth) {
        String tmp = " ";
        for (int i = 0; i < dim; ++ i) {
            tmp += "[]";
        }
        return indent(depth) + pointer.toString() + tmp;
    }
}
