package Type;

/**
 * Created by yhzmiao on 16/4/9.
 */
public class NullType extends Type {

    public static NullType instance = new NullType();

    @Override
    public boolean isVal() {
        return false;
    }

    @Override
    public String toString(int depth) {
        return null;
    }

    @Override
    public boolean equals(Type right) {
        return (!(right instanceof StringType));
    }
}
