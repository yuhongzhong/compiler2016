package Type;

import java.util.ArrayList;

/**
 * Created by yhzmiao on 16/4/4.
 */
public class FunctionType extends Type {
    public String name;
    public Type type;
    public ArrayList<Type> typelist;
    public ArrayList<String> namelist;

    public FunctionType(String name, Type type) {
        this.name = name;
        this.type = type;
        typelist = new ArrayList<>();
        namelist = new ArrayList<>();
    }

    @Override
    public boolean isVal() {
        return false;
    }

    @Override
    public boolean equals(Type right) {
        return (right instanceof FunctionType && name.equals(((FunctionType)right).name));
    }

    @Override
    public String toString(int depth) {
        return name;
    }
}
