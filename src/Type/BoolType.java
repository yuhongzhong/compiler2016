package Type;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class BoolType extends Type {
    public static BoolType instance = new BoolType();
    String name;

    @Override
    public String toString(int depth) {
        return "bool";
    }

    @Override
    public boolean equals(Type right) {
        return right instanceof BoolType;
    }

    @Override
    public boolean isVal() {
        return true;
    }
}
