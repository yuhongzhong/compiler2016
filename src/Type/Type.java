package Type;

/**
 * Created by yhzmiao on 16/4/3.
 */
public abstract class Type {
    public String indent (int depth) {
        String ret = "";
        for (int i = 0; i < depth; ++ i) {
            ret += " ";
        }
        return ret;
    }

    public abstract boolean isVal();
    public abstract boolean equals(Type right);
    public abstract String toString(int depth);

    public static Type toType(String type) {
        if (type.equals("int")) {
            return IntType.instance;
        }
        if (type.equals("string")) {
            return StringType.instance;
        }
        if (type.equals("bool")) {
            return BoolType.instance;
        }
        if (type.equals("void")) {
            return VoidType.instance;
        }
        if (type.equals("null")) {
            return NullType.instance;
        }
        int dim = 0;
        for (int i = 0; i < type.length(); ++ i) {
            if (type.charAt(i) == '[') {
                ++ dim;
            }
        }
        if (dim > 0) {
            String pointer = "";
            for (int i = 0; i < type.length(); ++ i) {
                if (type.charAt(i) != ' ' && type.charAt(i) != '[') {
                    pointer += type.charAt(i);
                }
                else {
                    break;
                }
            }
            return new ArrayType(toType(pointer), dim);
        }
        return new ClassType(type);
    }
}
