package Type;

import java.util.ArrayList;

/**
 * Created by yhzmiao on 16/4/3.
 */
public class ClassType extends Type{
    public String name;
    public ArrayList<Type> typelist;
    public ArrayList<String> namelist;

    public ClassType (String name) {
        this.name = name;
        typelist = new ArrayList<>();
        namelist = new ArrayList<>();
    }

    @Override
    public boolean isVal() {
        return true;
    }

    @Override
    public boolean equals(Type right) {
        return (right instanceof ClassType && ((ClassType) right).name.equals(name));
    }

    @Override
    public String toString(int depth) {
        return name;
    }
}
