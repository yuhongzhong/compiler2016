package Type;

/**
 * Created by yhzmiao on 16/4/6.
 */
public class StringType extends Type {
    public static StringType instance = new StringType();
    String name;

    @Override
    public boolean isVal() {
        return true;
    }

    @Override
    public boolean equals(Type right) {
        return right instanceof StringType;
    }

    @Override
    public String toString(int depth) {
        return null;
    }
}
