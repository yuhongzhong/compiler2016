package Type;

/**
 * Created by yhzmiao on 16/4/3.
 */
public class IntType extends Type {
    public static IntType instance = new IntType();
    String name;

    private IntType() {}

    @Override
    public String toString (int depth) {
        return indent(depth) + "int";
    }

    @Override
    public boolean isVal() {
        return true;
    }

    @Override
    public boolean equals(Type right) {
        return right instanceof IntType;
    }
}
