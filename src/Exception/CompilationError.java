package Exception;

/**
 * Created by yhzmiao on 16/4/4.
 */
public class CompilationError extends Error {
    public CompilationError(String err) {
        super(err);
    }
}
