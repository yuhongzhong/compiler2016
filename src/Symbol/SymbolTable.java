package Symbol;

import AST.Decl.VarDecl;
import Exception.CompilationError;
import Type.Type;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by yhzmiao on 16/4/3.
 */
public class SymbolTable {
    public int currentscope = 0;
    public ArrayList<Symbol> symboltable = new ArrayList<>();
    public ArrayList<Type> typetable = new ArrayList<>();
    public static HashMap var = new HashMap();
    public static ArrayList<VarDecl> UniDecl = new ArrayList<>();

    public void enterscope() {
        ++currentscope;
    }

    public void addType(Type type) {
        typetable.add(type);
    }

    public void insert(Type type, String name) {
        for (int i = symboltable.size() - 1; i >= 0; -- i) {
            if (symboltable.get(i).type.equals(type) && symboltable.get(i).name.equals(name) && symboltable.get(i).scope == currentscope) {
                throw new CompilationError("Double Declaration!");
            }
        }
        symboltable.add(new Symbol(type,name,currentscope));
    }

    public void exitscope() {
        for (int i = symboltable.size() - 1; i >= 0 && symboltable.get(i).scope == currentscope; --i) {
            symboltable.remove(i);
        }
        --currentscope;
    }

    public boolean lookup(Type type, String name) {
        for (int i = symboltable.size() - 1; i >= 0; -- i) {
            if (symboltable.get(i).type.equals(type) && symboltable.get(i).name == name) {
                return true;
            }
        }
        return false;
    }
}
