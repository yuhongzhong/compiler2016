package Symbol;

import Type.Type;

/**
 * Created by yhzmiao on 16/4/3.
 */
public class Symbol {
    public Type type;
    public String name;
    int scope;

    public Symbol(Type type, String name, int scope) {
        this.type = type;
        this.name = name;
        this.scope = scope;
    }
}
