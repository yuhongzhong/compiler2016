// Generated from /Users/yhzmiao/IdeaProjects/Compiler/src/Parser/MParser.g4 by ANTLR 4.5.1
package Parser;

import AST.ASTNode;
import AST.Decl.ClassDecl;
import AST.Decl.FuncDecl;
import AST.Decl.VarDecl;
import AST.Prog.Prog;
import AST.Stmt.*;
import AST.Stmt.Expr.BinaryExpr.*;
import AST.Stmt.Expr.Constant.BoolConstant;
import AST.Stmt.Expr.Constant.IntConstant;
import AST.Stmt.Expr.Constant.NullConstant;
import AST.Stmt.Expr.Constant.StringConstant;
import AST.Stmt.Expr.Expr;
import AST.Stmt.Expr.UnaryExpr.*;
import AST.Stmt.Expr.UnaryExpr.Sugar.*;
import AST.Stmt.Expr.VarExpr.ArgumentListExpr;
import AST.Stmt.Expr.VarExpr.FieldVarExpr;
import AST.Stmt.Expr.VarExpr.SimpleVarExpr;
import AST.Stmt.Expr.VarExpr.SubscriptVarExpr;
import Exception.CompilationError;
import Symbol.Symbol;
import Type.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;


/**
 * This class provides an empty implementation of {@link MParserListener},
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
public class EvalListenerThird implements MParserListener {
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGra(MParserParser.GraContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGra(MParserParser.GraContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    boolean checkedMain = false;
    public ParseTreeProperty<ASTNode> list = new ParseTreeProperty<>();
    public static Prog root;

    @Override public void enterGetClassDP(MParserParser.GetClassDPContext ctx) {
        if (!checkedMain) {
            for (int i = 0; i < EvalListenerSecond.symbolTable.typetable.size(); ++ i) {
                Type tmp = EvalListenerSecond.symbolTable.typetable.get(i);
                //if (tmp instanceof FunctionType) System.out.println(((FunctionType) tmp).type);
                if (tmp instanceof FunctionType && ((FunctionType) tmp).name.equals("main") && ((FunctionType) tmp).type instanceof IntType) {
                    //System.out.println("spy");
                    checkedMain = true;
                    return;
                }
            }
            throw new CompilationError("No 'int main'!");
        }
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetClassDP(MParserParser.GetClassDPContext ctx) {
        /*
        MParserParser.Class_declContext head = ctx.class_decl();
        MParserParser.ProgramContext tail = ctx.program();
        Prog now = new Prog((ClassDecl) list.get(head), (Prog)list.get(tail));
        list.put(ctx, now);
        */
        root = new Prog((ClassDecl)list.get(ctx.class_decl()), (Prog) list.get(ctx.program()));
        list.put(ctx, root);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetFuncDP(MParserParser.GetFuncDPContext ctx) {
        if (!checkedMain) {
            for (int i = 0; i < EvalListenerSecond.symbolTable.typetable.size(); ++ i) {
                Type tmp = EvalListenerSecond.symbolTable.typetable.get(i);
                //if (tmp instanceof FunctionType) System.out.println(((FunctionType) tmp).type);
                if (tmp instanceof FunctionType && ((FunctionType) tmp).name.equals("main") && ((FunctionType) tmp).type instanceof IntType) {
                    //System.out.println("spy");
                    checkedMain = true;
                    return;
                }
            }
            throw new CompilationError("No 'int main'!");
        }
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetFuncDP(MParserParser.GetFuncDPContext ctx) {
        /*
        MParserParser.Func_declContext head = ctx.func_decl();
        MParserParser.ProgramContext tail = ctx.program();
        Prog now = new Prog((FuncDecl) list.get(head), (Prog)list.get(tail));
        list.put(ctx, now);
        */
        root = new Prog((FuncDecl)list.get(ctx.func_decl()), (Prog) list.get(ctx.program()));
        list.put(ctx, root);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetVarDP(MParserParser.GetVarDPContext ctx) {
        if (!checkedMain) {
            for (int i = 0; i < EvalListenerSecond.symbolTable.typetable.size(); ++ i) {
                Type tmp = EvalListenerSecond.symbolTable.typetable.get(i);
                //if (tmp instanceof FunctionType) System.out.println(((FunctionType) tmp).type);
                if (tmp instanceof FunctionType && ((FunctionType) tmp).name.equals("main") && ((FunctionType) tmp).type instanceof IntType) {
                    //System.out.println("spy");
                    checkedMain = true;
                    return;
                }
            }
            throw new CompilationError("No 'int main'!");
        }
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetVarDP(MParserParser.GetVarDPContext ctx) {
        /*
        MParserParser.Var_declContext head = ctx.var_decl();
        MParserParser.ProgramContext tail = ctx.program();
        Prog now = new Prog((VarDecl) list.get(head), (Prog)list.get(tail));
        list.put(ctx, now);
        */
        root = new Prog((VarDecl)list.get(ctx.var_decl()), (Prog) list.get(ctx.program()));
        list.put(ctx, root);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetNullDP(MParserParser.GetNullDPContext ctx) {
        if (!checkedMain) {
            for (int i = 0; i < EvalListenerSecond.symbolTable.typetable.size(); ++ i) {
                Type tmp = EvalListenerSecond.symbolTable.typetable.get(i);
                //if (tmp instanceof FunctionType) System.out.println(((FunctionType) tmp).type);
                if (tmp instanceof FunctionType && ((FunctionType) tmp).name.equals("main") && ((FunctionType) tmp).type instanceof IntType) {
                    //System.out.println("spy");
                    checkedMain = true;
                    return;
                }
            }
            throw new CompilationError("No 'int main'!");
        }
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetNullDP(MParserParser.GetNullDPContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterClassDecl(MParserParser.ClassDeclContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitClassDecl(MParserParser.ClassDeclContext ctx) {
        list.put(ctx, new ClassDecl());
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterEndofClass(MParserParser.EndofClassContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitEndofClass(MParserParser.EndofClassContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterStmtofClass(MParserParser.StmtofClassContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitStmtofClass(MParserParser.StmtofClassContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetFuncDecl(MParserParser.GetFuncDeclContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */


    @Override public void exitGetFuncDecl(MParserParser.GetFuncDeclContext ctx) {
        String type = ctx.type().getText();
        Type cla = Type.toType(type);

        boolean spy = false;
        for (int i = 0; i < EvalListenerSecond.symbolTable.typetable.size(); ++ i) {
            Type tmp = EvalListenerSecond.symbolTable.typetable.get(i);
            if (tmp.equals(cla) || (tmp instanceof ArrayType && ((ArrayType) tmp).pointer.equals(cla))) {
                spy = true;
                break;
            }
        }
        if (!spy)
            throw new CompilationError("Undefined Type!");

        Block block = (Block) list.get(ctx.block());
        if (block.BorC) {
            throw new CompilationError("Not in Iteration!");
        }

        EvalListenerSecond.symbolTable.exitscope();

        FunctionType tmpFunction = null;

        for (int i = 0; i < EvalListenerSecond.symbolTable.typetable.size(); ++ i)
            if (EvalListenerSecond.symbolTable.typetable.get(i) instanceof FunctionType) {
                String tmp = ((FunctionType) EvalListenerSecond.symbolTable.typetable.get(i)).name;
                if (tmp.equals(ctx.func_name().getText()))
                    tmpFunction = (FunctionType) EvalListenerSecond.symbolTable.typetable.get(i);
            }

        FuncDecl now = new FuncDecl(tmpFunction, ctx.func_name().getText(), block);

        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterPositive(MParserParser.PositiveContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitPositive(MParserParser.PositiveContext ctx) {
        if (!(((Expr)list.get(ctx.multiplicative_expr())).getType() instanceof IntType)) {
            throw new CompilationError("Type Error!");
        }

        Positive ret = new Positive();
        ret.body = (Expr) list.get(ctx.multiplicative_expr());

        list.put(ctx, ret);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterNagative(MParserParser.NagativeContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitNagative(MParserParser.NagativeContext ctx) {

            if (!(((Expr)list.get(ctx.multiplicative_expr())).getType() instanceof IntType)) {
                throw new CompilationError("Type Error!");
            }

            Negative ret = new Negative();
            ret.body = (Expr) list.get(ctx.multiplicative_expr());

            list.put(ctx, ret);
    }

    @Override public void enterDemension(MParserParser.DemensionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitDemension(MParserParser.DemensionContext ctx) {
    }
    @Override public void enterFunc_name(MParserParser.Func_nameContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitFunc_name(MParserParser.Func_nameContext ctx) {

        String name = ctx.ID().getText();
        FunctionType now = null;

        for (int i = 0; i < EvalListenerSecond.symbolTable.typetable.size(); ++ i) {
            Type tmp = EvalListenerSecond.symbolTable.typetable.get(i);
            if (tmp instanceof FunctionType && ((FunctionType) tmp).name.equals(name)) {
                now = (FunctionType) tmp;
                break;
            }
        }

        if (now == null) {
            throw new CompilationError("Function Not Found!");
        }

        EvalListenerSecond.symbolTable.enterscope();
        for (int i = 0; i < now.typelist.size(); ++ i) {
            EvalListenerSecond.symbolTable.insert(now.typelist.get(i), now.namelist.get(i));
            //System.out.println(now.typelist.get(i));
            //System.out.println(now.namelist.get(i));
        }
        SimpleVarExpr ret = new SimpleVarExpr(now, name);
        list.put(ctx, ret);

    }
    @Override public void enterGetVFuncDecl(MParserParser.GetVFuncDeclContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetVFuncDecl(MParserParser.GetVFuncDeclContext ctx)  {

        Block block = (Block) list.get(ctx.block());
        if (block.BorC) {
            throw new CompilationError("Not in Iteration!");
        }

        EvalListenerSecond.symbolTable.exitscope();

        FunctionType tmpFunction = null;

        for (int i = 0; i < EvalListenerSecond.symbolTable.typetable.size(); ++ i)
            if (EvalListenerSecond.symbolTable.typetable.get(i) instanceof FunctionType) {
                String tmp = ((FunctionType) EvalListenerSecond.symbolTable.typetable.get(i)).name;
                if (tmp.equals(ctx.func_name().getText()))
                    tmpFunction = (FunctionType) EvalListenerSecond.symbolTable.typetable.get(i);
            }

        FuncDecl now = new FuncDecl(tmpFunction, ctx.func_name().getText(), block);

        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterParaList(MParserParser.ParaListContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitParaList(MParserParser.ParaListContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterNullParaList(MParserParser.NullParaListContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitNullParaList(MParserParser.NullParaListContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterType(MParserParser.TypeContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitType(MParserParser.TypeContext ctx) { }



    @Override public void enterNullBlock(MParserParser.NullBlockContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitNullBlock(MParserParser.NullBlockContext ctx) {
        list.put(ctx, new Block());
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterMultiBlock(MParserParser.MultiBlockContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitMultiBlock(MParserParser.MultiBlockContext ctx) {
        list.put(ctx, list.get(ctx.block_stmt_list()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterSingleBS(MParserParser.SingleBSContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitSingleBS(MParserParser.SingleBSContext ctx) {
        list.put(ctx, list.get(ctx.block_stmt()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterMultiBS(MParserParser.MultiBSContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitMultiBS(MParserParser.MultiBSContext ctx) {
        //System.out.println((ctx.block_stmt()));
        Block ret;
        Block now = ((Block)list.get(ctx.block_stmt()));
        Block sec = ((Block)list.get(ctx.block_stmt_list()));
        ret = now;
        ret.tail = sec;
        ret.BorC = now.BorC | sec.BorC;
        list.put(ctx, ret);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterBSisS(MParserParser.BSisSContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitBSisS(MParserParser.BSisSContext ctx) {
        Block now = new Block();
        if (list.get(ctx.stmt()) instanceof JumpStmt &&( ((JumpStmt) list.get(ctx.stmt())).type == 1  ||((JumpStmt) list.get(ctx.stmt())).type == 2)) {
            now.BorC = true;
        }
        now.isDecl = false;
        now.statement = (Stmt) list.get(ctx.stmt());
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterBSisVarDecl(MParserParser.BSisVarDeclContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitBSisVarDecl(MParserParser.BSisVarDeclContext ctx) {
        Block now = new Block();
        now.BorC = false;
        now.isDecl = true;
        now.decl = (VarDecl) list.get(ctx.var_decl());
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterCompStmt(MParserParser.CompStmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitCompStmt(MParserParser.CompStmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterExprStmt(MParserParser.ExprStmtContext ctx) {
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitExprStmt(MParserParser.ExprStmtContext ctx) {
        list.put(ctx, new ExprStmt((Expr)list.get(ctx.expr())));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterSelectStmt(MParserParser.SelectStmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitSelectStmt(MParserParser.SelectStmtContext ctx) {
        list.put(ctx, list.get(ctx.selection_stmt()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterIterStmt(MParserParser.IterStmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitIterStmt(MParserParser.IterStmtContext ctx) {
        list.put(ctx, list.get(ctx.iteration_stmt()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterJumpStmt(MParserParser.JumpStmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitJumpStmt(MParserParser.JumpStmtContext ctx) {
        list.put(ctx, list.get(ctx.jump_stmt()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterVarDecl(MParserParser.VarDeclContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitVarDecl(MParserParser.VarDeclContext ctx) {
        Type nowType = Type.toType(ctx.type().getText());
        String name = ctx.ID().getText();

        boolean spy = false;
        for (int i = 0; i < EvalListenerSecond.symbolTable.typetable.size(); ++ i) {
            Type tmp = EvalListenerSecond.symbolTable.typetable.get(i);
            //if (tmp instanceof ClassType) System.out.println(((ClassType) tmp).name);
            if (nowType.equals(tmp) || (nowType instanceof ArrayType && ((ArrayType) nowType).pointer.equals(tmp))) {
                //System.out.print("spy");
                spy = true;
                break;
            }
        }
        if (!spy)
            throw new CompilationError("Undefined Type!");


        VarDecl now = new VarDecl();
        now.type = nowType;
        now.name = name;
        now.init = false;
        EvalListenerSecond.symbolTable.insert(nowType, name);

        if (EvalListenerSecond.symbolTable.currentscope == 0) {
            EvalListenerSecond.symbolTable.UniDecl.add(now);
        }

        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterVarDeclValue(MParserParser.VarDeclValueContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitVarDeclValue(MParserParser.VarDeclValueContext ctx) {
        Type nowType = Type.toType(ctx.type().getText());
        //System.out.println(nowType);
        String name = ctx.ID().getText();

        boolean spy = false;
        for (int i = 0; i < EvalListenerSecond.symbolTable.typetable.size(); ++ i) {
            Type tmp = EvalListenerSecond.symbolTable.typetable.get(i);
            //if (tmp instanceof ClassType) System.out.println(((ClassType) tmp).name);
            if (nowType.equals(tmp) || (nowType instanceof ArrayType && ((ArrayType) nowType).pointer.equals(tmp))) {
                //System.out.print("spy");
                spy = true;
                break;
            }
        }
        if (!spy)
            throw new CompilationError("Undefined Type!");



        VarDecl now = new VarDecl();
        now.type = nowType;
        now.name = name;
        now.init = true;
        now.value = ((Expr) list.get(ctx.expr()));

        if (!(now.value.getType().equals(nowType)))
            throw new CompilationError("Cannot Declare!");

        //System.out.println(name);
        EvalListenerSecond.symbolTable.insert(nowType, name);

        if (EvalListenerSecond.symbolTable.currentscope == 0) {
            EvalListenerSecond.symbolTable.UniDecl.add(now);
        }

        //System.out.print(((ArrayType)now.type).dim);
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */

    @Override public void enterReturnExpr(MParserParser.ReturnExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitReturnExpr(MParserParser.ReturnExprContext ctx) {
        JumpStmt now = new JumpStmt();
        now.type = 3;
        now.body = (Expr) list.get(ctx.expr());
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterBreakExpr(MParserParser.BreakExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitBreakExpr(MParserParser.BreakExprContext ctx) {
        JumpStmt now = new JumpStmt();
        now.type = 1;
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterContinueExpr(MParserParser.ContinueExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitContinueExpr(MParserParser.ContinueExprContext ctx) {
        JumpStmt now = new JumpStmt();
        now.type = 2;
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterSelection_stmt(MParserParser.Selection_stmtContext ctx) {
        EvalListenerSecond.symbolTable.enterscope();
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitSelection_stmt(MParserParser.Selection_stmtContext ctx) {
        Type exprType = ((Expr)list.get(ctx.expr())).getType();
        if (!(exprType instanceof BoolType)) {
            throw new CompilationError("Type Error!");
        }
        EvalListenerSecond.symbolTable.exitscope();

        SelectionStmt now = new SelectionStmt();
        if (ctx.ELSE() == null) {
            now.isElse = false;
            now.condition = (Expr) list.get(ctx.expr());
            if (ctx.firBlo != null) {
                now.First = (Block) list.get(ctx.firBlo);
            }
            else if (ctx.firStmt != null) {
                now.First.statement = (Stmt) list.get(ctx.firStmt);
            }
        }
        else  {
            now.isElse = true;
            now.condition = (Expr) list.get(ctx.expr());
            if (ctx.firBlo != null) {
                now.First = (Block) list.get(ctx.firBlo);
            }
            else if (ctx.firStmt != null) {
                now.First.statement = (Stmt) list.get(ctx.firStmt);
            }
            if(ctx.secBlo != null) {
                now.Second = (Block) list.get(ctx.secBlo);
            }
            else if (ctx.secStmt != null) {
                now.Second.statement = (Stmt) list.get(ctx.secStmt);
            }
        }
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterIteration_stmt(MParserParser.Iteration_stmtContext ctx) {
        EvalListenerSecond.symbolTable.enterscope();
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitIteration_stmt(MParserParser.Iteration_stmtContext ctx) {
        if (ctx.cond != null) {
            Type exprType = ((Expr) list.get(ctx.cond)).getType();
            if (!(exprType instanceof BoolType)) {
                throw new CompilationError("Type Error!");
            }
        }
        EvalListenerSecond.symbolTable.exitscope();

        IterationStmt now = new IterationStmt();
        if (ctx.FOR() == null) {
            now.type = true;
            now.second = (Expr) list.get(ctx.cond);
            now.body = new Block();
            if (ctx.stmt() == null) {
                now.body = (Block) list.get(ctx.block());
            }
            else {
                now.body.statement = (Stmt) list.get(ctx.stmt());
            }
        }
        else {
            now.type = false;
            if (ctx.init != null) {
                now.first = (Expr) list.get(ctx.init);
            }
            if (ctx.cond != null) {
                now.second = (Expr) list.get(ctx.cond);
            }
            if (ctx.step != null) {
                now.third = (Expr)list.get(ctx.step);
            }
            if (ctx.stmt() == null) {
                now.body = (Block) list.get(ctx.block());
            }
            else {
                now.body = new Block();
                //System.out.println(ctx.stmt().getText());
                now.body.statement = (Stmt) list.get(ctx.stmt());
            }
        }
        //System.out.println(ctx.step);
        list.put(ctx, now);

    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterCompound_stmt(MParserParser.Compound_stmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitCompound_stmt(MParserParser.Compound_stmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterStmt_list(MParserParser.Stmt_listContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitStmt_list(MParserParser.Stmt_listContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGeneral_expr(MParserParser.General_exprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGeneral_expr(MParserParser.General_exprContext ctx) {
        //System.out.print(ctx.getText());

        if (ctx.ID() != null) {
            String now = ctx.ID().toString();
            //System.out.print(now);
            boolean spy = false;
            Symbol ret = null;

            for (int i = 0; i < EvalListenerSecond.symbolTable.symboltable.size(); ++ i) {
                Symbol tmp = EvalListenerSecond.symbolTable.symboltable.get(i);
                if (tmp.name.equals(now)) {
                    spy = true;
                    ret = tmp;
                    break;
                }
            }
            if (spy) {
                //System.out.print(ret.type);
                SimpleVarExpr haha = new SimpleVarExpr(ret.type, ret.name);
                //System.out.println(haha.getType());
                //System.out.println(haha.name);
                list.put(ctx, haha);
                return;
            }

            spy = false;
            ClassType get = null;
            for (int i = 0; i < EvalListenerSecond.symbolTable.typetable.size(); ++ i) {
                Type tmp = EvalListenerSecond.symbolTable.typetable.get(i);
                if (tmp instanceof ClassType && ((ClassType) tmp).name.equals(now)) {
                    spy = true;
                    get = (ClassType) tmp;
                    break;
                }
            }
            if (spy) {
                list.put(ctx, new SimpleVarExpr(get, now));
                return;
            }

            spy = false;
            FunctionType get_2 = null;
            for (int i = 0; i < EvalListenerSecond.symbolTable.typetable.size(); ++ i) {
                Type tmp = EvalListenerSecond.symbolTable.typetable.get(i);
                if (tmp instanceof FunctionType && ((FunctionType) tmp).name.equals(now)) {
                    spy = true;
                    get_2 = (FunctionType) tmp;
                }
            }
            if (!spy) {
                throw new CompilationError("Not Found!");
            }
            list.put(ctx, new SimpleVarExpr(get_2, now));
            return;
        }
        if (ctx.expr() != null) {
            list.put(ctx, list.get(ctx.expr()));
            return;
        }

        if (ctx.CSTRING() != null) {
            list.put(ctx, new StringConstant(ctx.CSTRING().getText()));
            return;
        }

        if (ctx.constant() != null) {
            list.put(ctx, list.get(ctx.constant()));
            return;
        }
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetSub(MParserParser.GetSubContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetSub(MParserParser.GetSubContext ctx) {
        //System.out.print("spy");
        if (!(((Expr)list.get(ctx.postfix_expr())).getType() instanceof ArrayType)) {
            throw new CompilationError("Not An Array!");
        }

        SubscriptVarExpr now = new SubscriptVarExpr();
        //System.out.println(ctx.postfix_expr().getText());

        if (list.get(ctx.postfix_expr()) instanceof SubscriptVarExpr) {
            now.type = ((SubscriptVarExpr) list.get(ctx.postfix_expr())).type;
            now.pointer = (Expr)list.get(ctx.postfix_expr());
            now.id = (Expr)list.get(ctx.expr());
            now.dim = ((SubscriptVarExpr) list.get(ctx.postfix_expr())).dim + 1;
            //System.out.println(((SubscriptVarExpr)now.pointer).pointer);
        }
        else {
            now.type = (ArrayType) ((Expr)list.get(ctx.postfix_expr())).getType();
            now.pointer = (Expr)list.get(ctx.postfix_expr());
            now.id = (Expr)list.get(ctx.expr());
            now.dim = 1;
        }
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetPostInc(MParserParser.GetPostIncContext ctx) {
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetPostInc(MParserParser.GetPostIncContext ctx) {
        PostInc now = new PostInc();
        now.body = (Expr) list.get(ctx.postfix_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetFunc(MParserParser.GetFuncContext ctx) {
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetFunc(MParserParser.GetFuncContext ctx) {
        //System.out.print(ctx.getText());
        ArgumentListExpr paraList;
        paraList = (ArgumentListExpr) list.get(ctx.argument_list());
        if (!(((Expr)list.get(ctx.postfix_expr())).getType() instanceof FunctionType)) {
            throw new CompilationError("Not a Function!");
        }
        FunctionType now = (FunctionType)(((Expr) list.get(ctx.postfix_expr())).getType());
        if (now.name.equals("main"))
            throw new CompilationError("Cannot Using 'main'!");
        if (now.typelist == null)
            now.typelist = new ArrayList<>();
        if (paraList == null)
            paraList = new ArgumentListExpr();
        if (now.typelist.size() != paraList.argumentlist.size()) {
            throw new CompilationError("Type Unmatched!");
        }
        for (int i = 0; i < now.typelist.size(); ++ i) {
            if (!(now.typelist.get(now.typelist.size() - 1 - i).equals(paraList.argumentlist.get(i).getType()))) {
                //System.out.println(i);
                //System.out.println(now.typelist.get(i));
                //System.out.println(paraList.argumentlist.get(i).getType());
                throw new CompilationError("Type Unmatched!");
            }
        }
        CallFunc ret = new CallFunc();
        ret.func = now;
        ret.arguementlist = (ArgumentListExpr) list.get(ctx.argument_list());
        list.put(ctx, ret);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetPostDec(MParserParser.GetPostDecContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetPostDec(MParserParser.GetPostDecContext ctx) {
        PostDec now = new PostDec();
        now.body = (Expr) list.get(ctx.postfix_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetConstant(MParserParser.GetConstantContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetConstant(MParserParser.GetConstantContext ctx) {
        list.put(ctx, list.get(ctx.general_expr()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetField(MParserParser.GetFieldContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetField(MParserParser.GetFieldContext ctx) {
        //System.out.println(ctx.getText());
        String name = ctx.ID().getText();
        Type nowType = ((Expr)list.get(ctx.postfix_expr())).getType();
        ///System.out.println(((Expr)list.get(ctx.postfix_expr())).getType());
        if (!(nowType instanceof ClassType)) {
            throw new CompilationError("Not A Class!");
        }
        for (int i = 0; i < EvalListenerSecond.symbolTable.typetable.size(); ++ i)
            if (((ClassType) nowType).equals(EvalListenerSecond.symbolTable.typetable.get(i))) {
                ClassType tmp = (ClassType) EvalListenerSecond.symbolTable.typetable.get(i);
                //System.out.println(tmp.name);
                for (int j = 0; j < tmp.namelist.size(); ++j) {
                    if (tmp.namelist.get(j).equals(name)) {
                        FieldVarExpr now = new FieldVarExpr(tmp, name);
                        now.pointer = (Expr)list.get(ctx.postfix_expr());
                        list.put(ctx, now);
                        return;
                    }
                }
                throw new CompilationError("No this Member!");
            }
        throw new CompilationError("No this Class!");
    }




    @Override public void enterSUGAR(MParserParser.SUGARContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitSUGAR(MParserParser.SUGARContext ctx) {
        Type nowType = ((Expr)list.get(ctx.postfix_expr())).getType();
        ArgumentListExpr paraList = (ArgumentListExpr) list.get(ctx.argument_list());
        String sugar = ctx.SUGAR().getText();
        if (nowType instanceof ArrayType) {
            if (sugar.equals("size")) {
                if (ctx.argument_list() == null) {
                    list.put(ctx, new ArraySize((Expr)list.get(ctx.postfix_expr())));
                    return;
                }
            }
        }
        if (nowType instanceof StringType) {
            if (sugar.equals("length")) {
                if (ctx.argument_list() == null) {
                    list.put(ctx, new StringLength((Expr)list.get(ctx.postfix_expr())));
                    return;
                }
            }
            if (sugar.equals("parseInt")) {
                if (ctx.argument_list() == null) {
                    list.put(ctx, new StringParseInt((Expr)list.get(ctx.postfix_expr())));
                    return;
                }
            }
            if (sugar.equals("ord")) {
                if (paraList.argumentlist.get(0).getType() instanceof IntType) {
                    list.put(ctx, new StringOrd((Expr)list.get(ctx.postfix_expr()), ((ArgumentListExpr)list.get(ctx.argument_list())).argumentlist.get(0)));
                    return;
                }
            }
            if (sugar.equals("substring")) {
                if (paraList.argumentlist.get(0).getType() instanceof IntType && paraList.argumentlist.get(1).getType() instanceof IntType) {
                    list.put(ctx, new StringSub((Expr)list.get(ctx.postfix_expr()), ((ArgumentListExpr)list.get(ctx.argument_list())).argumentlist.get(0), ((ArgumentListExpr)list.get(ctx.argument_list())).argumentlist.get(1)));
                    return;
                }
            }
        }
        throw new CompilationError("Undefined Method!");
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetPostExpr(MParserParser.GetPostExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetPostExpr(MParserParser.GetPostExprContext ctx) {
        list.put(ctx, list.get(ctx.postfix_expr()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetPreInc(MParserParser.GetPreIncContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetPreInc(MParserParser.GetPreIncContext ctx) {
        PreInc now = new PreInc();
        now.body = (Expr) list.get(ctx.unary_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetPreDec(MParserParser.GetPreDecContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetPreDec(MParserParser.GetPreDecContext ctx) {
        PreDec now = new PreDec();
        now.body = (Expr) list.get(ctx.unary_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetNotExpr(MParserParser.GetNotExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetNotExpr(MParserParser.GetNotExprContext ctx) {
        LogicalNot now = new LogicalNot();
        now.body = (Expr) list.get(ctx.multiplicative_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterUnary_operator(MParserParser.Unary_operatorContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitUnary_operator(MParserParser.Unary_operatorContext ctx) {
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetUnaryExpr(MParserParser.GetUnaryExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetUnaryExpr(MParserParser.GetUnaryExprContext ctx) {
        list.put(ctx, list.get(ctx.unary_expr()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetNew(MParserParser.GetNewContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetNew(MParserParser.GetNewContext ctx) {
        //System.out.println(ctx.type().getText());
        Type nowType = Type.toType(ctx.type().getText());
        //System.out.println(nowType);
        boolean spy = false;
        for (int i = 0; i < EvalListenerSecond.symbolTable.typetable.size(); ++ i) {
            Type tmp = EvalListenerSecond.symbolTable.typetable.get(i);
            //System.out.println(tmp);
            if (tmp instanceof NullType)
                continue;
            if (tmp.equals(nowType) || (tmp instanceof ArrayType && ((ArrayType) tmp).pointer.equals(nowType))) {
                spy = true;
                if (nowType instanceof ClassType)
                    nowType = tmp;
                break;
            }
        }
        if (!spy)
            throw new CompilationError("Undefined Type!");
        Type retType;
        Expr retExpr;
        if (ctx.demension().size() == 0) {
            retType = nowType;
            retExpr = null;
            //System.out.println("spy");
        }
        else {
            spy = false;
            for (int i = 0; i < ctx.demension().size(); ++ i) {
                if (ctx.demension().get(i).expr() == null) {
                    spy = true;
                }
                else if (ctx.demension().get(i).expr() == null && spy) {
                    throw new CompilationError("Wrong Declaration!");
                }
            }
            retExpr = (Expr) list.get(ctx.demension().get(0).expr());
            retType = new ArrayType(nowType, ctx.demension().size());
        }
        //System.out.println(retType);
        list.put(ctx, new Creation(retType, retExpr));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetDivide(MParserParser.GetDivideContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetDivide(MParserParser.GetDivideContext ctx) {
        DivideExpr now = new DivideExpr();
        now.left = (Expr)list.get(ctx.m1);
        now.right = (Expr)list.get(ctx.m2);
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetMultiply(MParserParser.GetMultiplyContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetMultiply(MParserParser.GetMultiplyContext ctx) {
        MultiplyExpr now = new MultiplyExpr();
        now.left = (Expr)list.get(ctx.m1);
        now.right = (Expr)list.get(ctx.m2);
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetCreationExpr(MParserParser.GetCreationExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetCreationExpr(MParserParser.GetCreationExprContext ctx) {
        list.put(ctx, list.get(ctx.creation_expr()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetModulo(MParserParser.GetModuloContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetModulo(MParserParser.GetModuloContext ctx) {
        ModuloExpr now = new ModuloExpr();
        now.left = (Expr)list.get(ctx.m1);
        now.right = (Expr)list.get(ctx.m2);
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetAdd(MParserParser.GetAddContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetAdd(MParserParser.GetAddContext ctx) {
        AddExpr now = new AddExpr();
        now.left = (Expr)list.get(ctx.additive_expr());
        now.right = (Expr)list.get(ctx.multiplicative_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetMinus(MParserParser.GetMinusContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetMinus(MParserParser.GetMinusContext ctx) {
        MinusExpr now = new MinusExpr();
        now.left = (Expr)list.get(ctx.additive_expr());
        now.right = (Expr)list.get(ctx.multiplicative_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetMultiplyExpr(MParserParser.GetMultiplyExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetMultiplyExpr(MParserParser.GetMultiplyExprContext ctx) {
        list.put(ctx, list.get(ctx.multiplicative_expr()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetAddExpr(MParserParser.GetAddExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetAddExpr(MParserParser.GetAddExprContext ctx) {
        list.put(ctx, list.get(ctx.additive_expr()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetRShift(MParserParser.GetRShiftContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetRShift(MParserParser.GetRShiftContext ctx) {
            ShiftRight now = new ShiftRight();
            now.left = (Expr)list.get(ctx.shift_expr());
            now.right = (Expr)list.get(ctx.additive_expr());
            now.check();
            list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetLShift(MParserParser.GetLShiftContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetLShift(MParserParser.GetLShiftContext ctx) {
        ShiftLeft now = new ShiftLeft();
        now.left = (Expr)list.get(ctx.shift_expr());
        now.right = (Expr)list.get(ctx.additive_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetGreater(MParserParser.GetGreaterContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetGreater(MParserParser.GetGreaterContext ctx) {
        Greater now = new Greater();
        now.left = (Expr)list.get(ctx.relational_expr());
        now.right = (Expr)list.get(ctx.shift_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetShiftExpr(MParserParser.GetShiftExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetShiftExpr(MParserParser.GetShiftExprContext ctx) {
        list.put(ctx, list.get(ctx.shift_expr()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetLE(MParserParser.GetLEContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetLE(MParserParser.GetLEContext ctx) {
        LessEqual now = new LessEqual();
        now.left = (Expr)list.get(ctx.relational_expr());
        //System.out.println(((FieldVarExpr) now.left).name);
        now.right = (Expr)list.get(ctx.shift_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetLess(MParserParser.GetLessContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetLess(MParserParser.GetLessContext ctx) {
        Less now = new Less();
        now.left = (Expr)list.get(ctx.relational_expr());
        now.right = (Expr)list.get(ctx.shift_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetGE(MParserParser.GetGEContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetGE(MParserParser.GetGEContext ctx) {
        GreaterEqual now = new GreaterEqual();
        now.left = (Expr)list.get(ctx.relational_expr());
        now.right = (Expr)list.get(ctx.shift_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetNotEqualTo(MParserParser.GetNotEqualToContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetNotEqualTo(MParserParser.GetNotEqualToContext ctx) {
        NotEqualTo now = new NotEqualTo();
        now.left = (Expr)list.get(ctx.equality_expr());
        now.right = (Expr)list.get(ctx.relational_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetRelationalExpr(MParserParser.GetRelationalExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetRelationalExpr(MParserParser.GetRelationalExprContext ctx) {
        list.put(ctx, list.get(ctx.relational_expr()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetEqualTo(MParserParser.GetEqualToContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetEqualTo(MParserParser.GetEqualToContext ctx) {
        EqualTo now = new EqualTo();
        now.left = (Expr)list.get(ctx.equality_expr());
        now.right = (Expr)list.get(ctx.relational_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetEqualExpr(MParserParser.GetEqualExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetEqualExpr(MParserParser.GetEqualExprContext ctx) {
        list.put(ctx, list.get(ctx.equality_expr()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetAnd(MParserParser.GetAndContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetAnd(MParserParser.GetAndContext ctx) {
        BitAnd now = new BitAnd();
        now.left = (Expr)list.get(ctx.and_expr());
        now.right = (Expr)list.get(ctx.equality_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetAndExpr(MParserParser.GetAndExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetAndExpr(MParserParser.GetAndExprContext ctx) {
        list.put(ctx, list.get(ctx.and_expr()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetXor(MParserParser.GetXorContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetXor(MParserParser.GetXorContext ctx) {
        BitXor now = new BitXor();
        now.left = (Expr)list.get(ctx.exclusive_or_expr());
        now.right = (Expr)list.get(ctx.and_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetXorExpr(MParserParser.GetXorExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetXorExpr(MParserParser.GetXorExprContext ctx) {
        list.put(ctx, list.get(ctx.exclusive_or_expr()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetOr(MParserParser.GetOrContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetOr(MParserParser.GetOrContext ctx) {
        BitOr now = new BitOr();
        now.left = (Expr)list.get(ctx.inclusive_or_expr());
        now.right = (Expr)list.get(ctx.exclusive_or_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetOrExpr(MParserParser.GetOrExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetOrExpr(MParserParser.GetOrExprContext ctx) {
        list.put(ctx, list.get(ctx.inclusive_or_expr()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetLogicAnd(MParserParser.GetLogicAndContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetLogicAnd(MParserParser.GetLogicAndContext ctx) {
        LogicalAnd now = new LogicalAnd();
        now.left = (Expr)list.get(ctx.logical_and_expr());
        now.right = (Expr)list.get(ctx.inclusive_or_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetLogicAndExpr(MParserParser.GetLogicAndExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetLogicAndExpr(MParserParser.GetLogicAndExprContext ctx) {
        list.put(ctx, list.get(ctx.logical_and_expr()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetLogicOr(MParserParser.GetLogicOrContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetLogicOr(MParserParser.GetLogicOrContext ctx) {
        LogicalOr now = new LogicalOr();
        now.left = (Expr)list.get(ctx.logical_or_expr());
        now.right = (Expr)list.get(ctx.logical_and_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetLogicOrExpr(MParserParser.GetLogicOrExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetLogicOrExpr(MParserParser.GetLogicOrExprContext ctx) {
        list.put(ctx, list.get(ctx.logical_or_expr()));
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetAssignment(MParserParser.GetAssignmentContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetAssignment(MParserParser.GetAssignmentContext ctx) {

        //System.out.print(ctx.getText());
        AssignExpr now = new AssignExpr();
        now.left = (Expr)list.get(ctx.unary_expr());
        now.right = (Expr)list.get(ctx.assignment_expr());
        now.check();
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterAssignment_operator(MParserParser.Assignment_operatorContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitAssignment_operator(MParserParser.Assignment_operatorContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetAssignmentExpr(MParserParser.GetAssignmentExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetAssignmentExpr(MParserParser.GetAssignmentExprContext ctx) {
        ArgumentListExpr now = new ArgumentListExpr();
        now.argumentlist.add(((Expr)list.get(ctx.assignment_expr())));
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetArguList(MParserParser.GetArguListContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetArguList(MParserParser.GetArguListContext ctx) {
        //ParaList now = (ParaList)list.get(ctx.argument_list());
        //now.list.add(((Expr)list.get(ctx.assignment_expr())).getType());
        ArgumentListExpr now = (ArgumentListExpr)list.get(ctx.argument_list());
        now.argumentlist.add((Expr)list.get(ctx.assignment_expr()));
        list.put(ctx, now);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterExpr(MParserParser.ExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitExpr(MParserParser.ExprContext ctx) {
        list.put(ctx, list.get(ctx.assignment_expr()));}
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterConstant(MParserParser.ConstantContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitConstant(MParserParser.ConstantContext ctx) {
        if (ctx.TRUE() != null) {
            list.put(ctx, new BoolConstant(true));
        }
        else if (ctx.FALSE() != null) {
            list.put(ctx, new BoolConstant(false));
        }
        else if (ctx.NULL() != null) {
            list.put(ctx, new NullConstant());
        }
        else if (ctx.NUMBER() != null) {
            list.put(ctx, new IntConstant(Integer.parseInt(ctx.NUMBER().getText())));
        }
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterEveryRule(ParserRuleContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitEveryRule(ParserRuleContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void visitTerminal(TerminalNode node) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void visitErrorNode(ErrorNode node) { }
}