// Generated from MParser.g4 by ANTLR 4.5
package Parser;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MParserParser}.
 */
public interface MParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MParserParser#gra}.
	 * @param ctx the parse tree
	 */
	void enterGra(MParserParser.GraContext ctx);
	/**
	 * Exit a parse tree produced by {@link MParserParser#gra}.
	 * @param ctx the parse tree
	 */
	void exitGra(MParserParser.GraContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getClassDP}
	 * labeled alternative in {@link MParserParser#program}.
	 * @param ctx the parse tree
	 */
	void enterGetClassDP(MParserParser.GetClassDPContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getClassDP}
	 * labeled alternative in {@link MParserParser#program}.
	 * @param ctx the parse tree
	 */
	void exitGetClassDP(MParserParser.GetClassDPContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getFuncDP}
	 * labeled alternative in {@link MParserParser#program}.
	 * @param ctx the parse tree
	 */
	void enterGetFuncDP(MParserParser.GetFuncDPContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getFuncDP}
	 * labeled alternative in {@link MParserParser#program}.
	 * @param ctx the parse tree
	 */
	void exitGetFuncDP(MParserParser.GetFuncDPContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getVarDP}
	 * labeled alternative in {@link MParserParser#program}.
	 * @param ctx the parse tree
	 */
	void enterGetVarDP(MParserParser.GetVarDPContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getVarDP}
	 * labeled alternative in {@link MParserParser#program}.
	 * @param ctx the parse tree
	 */
	void exitGetVarDP(MParserParser.GetVarDPContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getNullDP}
	 * labeled alternative in {@link MParserParser#program}.
	 * @param ctx the parse tree
	 */
	void enterGetNullDP(MParserParser.GetNullDPContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getNullDP}
	 * labeled alternative in {@link MParserParser#program}.
	 * @param ctx the parse tree
	 */
	void exitGetNullDP(MParserParser.GetNullDPContext ctx);
	/**
	 * Enter a parse tree produced by the {@code classDecl}
	 * labeled alternative in {@link MParserParser#class_decl}.
	 * @param ctx the parse tree
	 */
	void enterClassDecl(MParserParser.ClassDeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code classDecl}
	 * labeled alternative in {@link MParserParser#class_decl}.
	 * @param ctx the parse tree
	 */
	void exitClassDecl(MParserParser.ClassDeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code endofClass}
	 * labeled alternative in {@link MParserParser#class_body}.
	 * @param ctx the parse tree
	 */
	void enterEndofClass(MParserParser.EndofClassContext ctx);
	/**
	 * Exit a parse tree produced by the {@code endofClass}
	 * labeled alternative in {@link MParserParser#class_body}.
	 * @param ctx the parse tree
	 */
	void exitEndofClass(MParserParser.EndofClassContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stmtofClass}
	 * labeled alternative in {@link MParserParser#class_body}.
	 * @param ctx the parse tree
	 */
	void enterStmtofClass(MParserParser.StmtofClassContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stmtofClass}
	 * labeled alternative in {@link MParserParser#class_body}.
	 * @param ctx the parse tree
	 */
	void exitStmtofClass(MParserParser.StmtofClassContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getFuncDecl}
	 * labeled alternative in {@link MParserParser#func_decl}.
	 * @param ctx the parse tree
	 */
	void enterGetFuncDecl(MParserParser.GetFuncDeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getFuncDecl}
	 * labeled alternative in {@link MParserParser#func_decl}.
	 * @param ctx the parse tree
	 */
	void exitGetFuncDecl(MParserParser.GetFuncDeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getVFuncDecl}
	 * labeled alternative in {@link MParserParser#func_decl}.
	 * @param ctx the parse tree
	 */
	void enterGetVFuncDecl(MParserParser.GetVFuncDeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getVFuncDecl}
	 * labeled alternative in {@link MParserParser#func_decl}.
	 * @param ctx the parse tree
	 */
	void exitGetVFuncDecl(MParserParser.GetVFuncDeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link MParserParser#func_name}.
	 * @param ctx the parse tree
	 */
	void enterFunc_name(MParserParser.Func_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link MParserParser#func_name}.
	 * @param ctx the parse tree
	 */
	void exitFunc_name(MParserParser.Func_nameContext ctx);
	/**
	 * Enter a parse tree produced by the {@code paraList}
	 * labeled alternative in {@link MParserParser#parameter_list}.
	 * @param ctx the parse tree
	 */
	void enterParaList(MParserParser.ParaListContext ctx);
	/**
	 * Exit a parse tree produced by the {@code paraList}
	 * labeled alternative in {@link MParserParser#parameter_list}.
	 * @param ctx the parse tree
	 */
	void exitParaList(MParserParser.ParaListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nullParaList}
	 * labeled alternative in {@link MParserParser#parameter_list}.
	 * @param ctx the parse tree
	 */
	void enterNullParaList(MParserParser.NullParaListContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nullParaList}
	 * labeled alternative in {@link MParserParser#parameter_list}.
	 * @param ctx the parse tree
	 */
	void exitNullParaList(MParserParser.NullParaListContext ctx);
	/**
	 * Enter a parse tree produced by {@link MParserParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(MParserParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link MParserParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(MParserParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code NullBlock}
	 * labeled alternative in {@link MParserParser#block}.
	 * @param ctx the parse tree
	 */
	void enterNullBlock(MParserParser.NullBlockContext ctx);
	/**
	 * Exit a parse tree produced by the {@code NullBlock}
	 * labeled alternative in {@link MParserParser#block}.
	 * @param ctx the parse tree
	 */
	void exitNullBlock(MParserParser.NullBlockContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MultiBlock}
	 * labeled alternative in {@link MParserParser#block}.
	 * @param ctx the parse tree
	 */
	void enterMultiBlock(MParserParser.MultiBlockContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MultiBlock}
	 * labeled alternative in {@link MParserParser#block}.
	 * @param ctx the parse tree
	 */
	void exitMultiBlock(MParserParser.MultiBlockContext ctx);
	/**
	 * Enter a parse tree produced by the {@code SingleBS}
	 * labeled alternative in {@link MParserParser#block_stmt_list}.
	 * @param ctx the parse tree
	 */
	void enterSingleBS(MParserParser.SingleBSContext ctx);
	/**
	 * Exit a parse tree produced by the {@code SingleBS}
	 * labeled alternative in {@link MParserParser#block_stmt_list}.
	 * @param ctx the parse tree
	 */
	void exitSingleBS(MParserParser.SingleBSContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MultiBS}
	 * labeled alternative in {@link MParserParser#block_stmt_list}.
	 * @param ctx the parse tree
	 */
	void enterMultiBS(MParserParser.MultiBSContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MultiBS}
	 * labeled alternative in {@link MParserParser#block_stmt_list}.
	 * @param ctx the parse tree
	 */
	void exitMultiBS(MParserParser.MultiBSContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BSisS}
	 * labeled alternative in {@link MParserParser#block_stmt}.
	 * @param ctx the parse tree
	 */
	void enterBSisS(MParserParser.BSisSContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BSisS}
	 * labeled alternative in {@link MParserParser#block_stmt}.
	 * @param ctx the parse tree
	 */
	void exitBSisS(MParserParser.BSisSContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BSisVarDecl}
	 * labeled alternative in {@link MParserParser#block_stmt}.
	 * @param ctx the parse tree
	 */
	void enterBSisVarDecl(MParserParser.BSisVarDeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BSisVarDecl}
	 * labeled alternative in {@link MParserParser#block_stmt}.
	 * @param ctx the parse tree
	 */
	void exitBSisVarDecl(MParserParser.BSisVarDeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link MParserParser#iteration_stmt}.
	 * @param ctx the parse tree
	 */
	void enterIteration_stmt(MParserParser.Iteration_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MParserParser#iteration_stmt}.
	 * @param ctx the parse tree
	 */
	void exitIteration_stmt(MParserParser.Iteration_stmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code CompStmt}
	 * labeled alternative in {@link MParserParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterCompStmt(MParserParser.CompStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code CompStmt}
	 * labeled alternative in {@link MParserParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitCompStmt(MParserParser.CompStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExprStmt}
	 * labeled alternative in {@link MParserParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterExprStmt(MParserParser.ExprStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExprStmt}
	 * labeled alternative in {@link MParserParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitExprStmt(MParserParser.ExprStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code SelectStmt}
	 * labeled alternative in {@link MParserParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterSelectStmt(MParserParser.SelectStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code SelectStmt}
	 * labeled alternative in {@link MParserParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitSelectStmt(MParserParser.SelectStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IterStmt}
	 * labeled alternative in {@link MParserParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterIterStmt(MParserParser.IterStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IterStmt}
	 * labeled alternative in {@link MParserParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitIterStmt(MParserParser.IterStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code JumpStmt}
	 * labeled alternative in {@link MParserParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterJumpStmt(MParserParser.JumpStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code JumpStmt}
	 * labeled alternative in {@link MParserParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitJumpStmt(MParserParser.JumpStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code varDecl}
	 * labeled alternative in {@link MParserParser#var_decl}.
	 * @param ctx the parse tree
	 */
	void enterVarDecl(MParserParser.VarDeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code varDecl}
	 * labeled alternative in {@link MParserParser#var_decl}.
	 * @param ctx the parse tree
	 */
	void exitVarDecl(MParserParser.VarDeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code varDeclValue}
	 * labeled alternative in {@link MParserParser#var_decl}.
	 * @param ctx the parse tree
	 */
	void enterVarDeclValue(MParserParser.VarDeclValueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code varDeclValue}
	 * labeled alternative in {@link MParserParser#var_decl}.
	 * @param ctx the parse tree
	 */
	void exitVarDeclValue(MParserParser.VarDeclValueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ReturnExpr}
	 * labeled alternative in {@link MParserParser#jump_stmt}.
	 * @param ctx the parse tree
	 */
	void enterReturnExpr(MParserParser.ReturnExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ReturnExpr}
	 * labeled alternative in {@link MParserParser#jump_stmt}.
	 * @param ctx the parse tree
	 */
	void exitReturnExpr(MParserParser.ReturnExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BreakExpr}
	 * labeled alternative in {@link MParserParser#jump_stmt}.
	 * @param ctx the parse tree
	 */
	void enterBreakExpr(MParserParser.BreakExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BreakExpr}
	 * labeled alternative in {@link MParserParser#jump_stmt}.
	 * @param ctx the parse tree
	 */
	void exitBreakExpr(MParserParser.BreakExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ContinueExpr}
	 * labeled alternative in {@link MParserParser#jump_stmt}.
	 * @param ctx the parse tree
	 */
	void enterContinueExpr(MParserParser.ContinueExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ContinueExpr}
	 * labeled alternative in {@link MParserParser#jump_stmt}.
	 * @param ctx the parse tree
	 */
	void exitContinueExpr(MParserParser.ContinueExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MParserParser#selection_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSelection_stmt(MParserParser.Selection_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MParserParser#selection_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSelection_stmt(MParserParser.Selection_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MParserParser#compound_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCompound_stmt(MParserParser.Compound_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MParserParser#compound_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCompound_stmt(MParserParser.Compound_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MParserParser#stmt_list}.
	 * @param ctx the parse tree
	 */
	void enterStmt_list(MParserParser.Stmt_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link MParserParser#stmt_list}.
	 * @param ctx the parse tree
	 */
	void exitStmt_list(MParserParser.Stmt_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link MParserParser#general_expr}.
	 * @param ctx the parse tree
	 */
	void enterGeneral_expr(MParserParser.General_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MParserParser#general_expr}.
	 * @param ctx the parse tree
	 */
	void exitGeneral_expr(MParserParser.General_exprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getSub}
	 * labeled alternative in {@link MParserParser#postfix_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetSub(MParserParser.GetSubContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getSub}
	 * labeled alternative in {@link MParserParser#postfix_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetSub(MParserParser.GetSubContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getPostInc}
	 * labeled alternative in {@link MParserParser#postfix_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetPostInc(MParserParser.GetPostIncContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getPostInc}
	 * labeled alternative in {@link MParserParser#postfix_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetPostInc(MParserParser.GetPostIncContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getFunc}
	 * labeled alternative in {@link MParserParser#postfix_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetFunc(MParserParser.GetFuncContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getFunc}
	 * labeled alternative in {@link MParserParser#postfix_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetFunc(MParserParser.GetFuncContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getPostDec}
	 * labeled alternative in {@link MParserParser#postfix_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetPostDec(MParserParser.GetPostDecContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getPostDec}
	 * labeled alternative in {@link MParserParser#postfix_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetPostDec(MParserParser.GetPostDecContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getConstant}
	 * labeled alternative in {@link MParserParser#postfix_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetConstant(MParserParser.GetConstantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getConstant}
	 * labeled alternative in {@link MParserParser#postfix_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetConstant(MParserParser.GetConstantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getField}
	 * labeled alternative in {@link MParserParser#postfix_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetField(MParserParser.GetFieldContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getField}
	 * labeled alternative in {@link MParserParser#postfix_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetField(MParserParser.GetFieldContext ctx);
	/**
	 * Enter a parse tree produced by the {@code SUGAR}
	 * labeled alternative in {@link MParserParser#postfix_expr}.
	 * @param ctx the parse tree
	 */
	void enterSUGAR(MParserParser.SUGARContext ctx);
	/**
	 * Exit a parse tree produced by the {@code SUGAR}
	 * labeled alternative in {@link MParserParser#postfix_expr}.
	 * @param ctx the parse tree
	 */
	void exitSUGAR(MParserParser.SUGARContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getPostExpr}
	 * labeled alternative in {@link MParserParser#unary_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetPostExpr(MParserParser.GetPostExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getPostExpr}
	 * labeled alternative in {@link MParserParser#unary_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetPostExpr(MParserParser.GetPostExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getPreInc}
	 * labeled alternative in {@link MParserParser#unary_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetPreInc(MParserParser.GetPreIncContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getPreInc}
	 * labeled alternative in {@link MParserParser#unary_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetPreInc(MParserParser.GetPreIncContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getPreDec}
	 * labeled alternative in {@link MParserParser#unary_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetPreDec(MParserParser.GetPreDecContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getPreDec}
	 * labeled alternative in {@link MParserParser#unary_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetPreDec(MParserParser.GetPreDecContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getNotExpr}
	 * labeled alternative in {@link MParserParser#unary_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetNotExpr(MParserParser.GetNotExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getNotExpr}
	 * labeled alternative in {@link MParserParser#unary_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetNotExpr(MParserParser.GetNotExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code positive}
	 * labeled alternative in {@link MParserParser#unary_expr}.
	 * @param ctx the parse tree
	 */
	void enterPositive(MParserParser.PositiveContext ctx);
	/**
	 * Exit a parse tree produced by the {@code positive}
	 * labeled alternative in {@link MParserParser#unary_expr}.
	 * @param ctx the parse tree
	 */
	void exitPositive(MParserParser.PositiveContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nagative}
	 * labeled alternative in {@link MParserParser#unary_expr}.
	 * @param ctx the parse tree
	 */
	void enterNagative(MParserParser.NagativeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nagative}
	 * labeled alternative in {@link MParserParser#unary_expr}.
	 * @param ctx the parse tree
	 */
	void exitNagative(MParserParser.NagativeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MParserParser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void enterUnary_operator(MParserParser.Unary_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MParserParser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void exitUnary_operator(MParserParser.Unary_operatorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getUnaryExpr}
	 * labeled alternative in {@link MParserParser#creation_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetUnaryExpr(MParserParser.GetUnaryExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getUnaryExpr}
	 * labeled alternative in {@link MParserParser#creation_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetUnaryExpr(MParserParser.GetUnaryExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getNew}
	 * labeled alternative in {@link MParserParser#creation_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetNew(MParserParser.GetNewContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getNew}
	 * labeled alternative in {@link MParserParser#creation_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetNew(MParserParser.GetNewContext ctx);
	/**
	 * Enter a parse tree produced by {@link MParserParser#demension}.
	 * @param ctx the parse tree
	 */
	void enterDemension(MParserParser.DemensionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MParserParser#demension}.
	 * @param ctx the parse tree
	 */
	void exitDemension(MParserParser.DemensionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getDivide}
	 * labeled alternative in {@link MParserParser#multiplicative_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetDivide(MParserParser.GetDivideContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getDivide}
	 * labeled alternative in {@link MParserParser#multiplicative_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetDivide(MParserParser.GetDivideContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getMultiply}
	 * labeled alternative in {@link MParserParser#multiplicative_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetMultiply(MParserParser.GetMultiplyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getMultiply}
	 * labeled alternative in {@link MParserParser#multiplicative_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetMultiply(MParserParser.GetMultiplyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getCreationExpr}
	 * labeled alternative in {@link MParserParser#multiplicative_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetCreationExpr(MParserParser.GetCreationExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getCreationExpr}
	 * labeled alternative in {@link MParserParser#multiplicative_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetCreationExpr(MParserParser.GetCreationExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getModulo}
	 * labeled alternative in {@link MParserParser#multiplicative_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetModulo(MParserParser.GetModuloContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getModulo}
	 * labeled alternative in {@link MParserParser#multiplicative_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetModulo(MParserParser.GetModuloContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getAdd}
	 * labeled alternative in {@link MParserParser#additive_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetAdd(MParserParser.GetAddContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getAdd}
	 * labeled alternative in {@link MParserParser#additive_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetAdd(MParserParser.GetAddContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getMinus}
	 * labeled alternative in {@link MParserParser#additive_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetMinus(MParserParser.GetMinusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getMinus}
	 * labeled alternative in {@link MParserParser#additive_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetMinus(MParserParser.GetMinusContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getMultiplyExpr}
	 * labeled alternative in {@link MParserParser#additive_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetMultiplyExpr(MParserParser.GetMultiplyExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getMultiplyExpr}
	 * labeled alternative in {@link MParserParser#additive_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetMultiplyExpr(MParserParser.GetMultiplyExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getAddExpr}
	 * labeled alternative in {@link MParserParser#shift_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetAddExpr(MParserParser.GetAddExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getAddExpr}
	 * labeled alternative in {@link MParserParser#shift_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetAddExpr(MParserParser.GetAddExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getRShift}
	 * labeled alternative in {@link MParserParser#shift_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetRShift(MParserParser.GetRShiftContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getRShift}
	 * labeled alternative in {@link MParserParser#shift_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetRShift(MParserParser.GetRShiftContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getLShift}
	 * labeled alternative in {@link MParserParser#shift_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetLShift(MParserParser.GetLShiftContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getLShift}
	 * labeled alternative in {@link MParserParser#shift_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetLShift(MParserParser.GetLShiftContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getGreater}
	 * labeled alternative in {@link MParserParser#relational_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetGreater(MParserParser.GetGreaterContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getGreater}
	 * labeled alternative in {@link MParserParser#relational_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetGreater(MParserParser.GetGreaterContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getShiftExpr}
	 * labeled alternative in {@link MParserParser#relational_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetShiftExpr(MParserParser.GetShiftExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getShiftExpr}
	 * labeled alternative in {@link MParserParser#relational_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetShiftExpr(MParserParser.GetShiftExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getLE}
	 * labeled alternative in {@link MParserParser#relational_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetLE(MParserParser.GetLEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getLE}
	 * labeled alternative in {@link MParserParser#relational_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetLE(MParserParser.GetLEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getLess}
	 * labeled alternative in {@link MParserParser#relational_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetLess(MParserParser.GetLessContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getLess}
	 * labeled alternative in {@link MParserParser#relational_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetLess(MParserParser.GetLessContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getGE}
	 * labeled alternative in {@link MParserParser#relational_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetGE(MParserParser.GetGEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getGE}
	 * labeled alternative in {@link MParserParser#relational_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetGE(MParserParser.GetGEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getNotEqualTo}
	 * labeled alternative in {@link MParserParser#equality_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetNotEqualTo(MParserParser.GetNotEqualToContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getNotEqualTo}
	 * labeled alternative in {@link MParserParser#equality_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetNotEqualTo(MParserParser.GetNotEqualToContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getRelationalExpr}
	 * labeled alternative in {@link MParserParser#equality_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetRelationalExpr(MParserParser.GetRelationalExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getRelationalExpr}
	 * labeled alternative in {@link MParserParser#equality_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetRelationalExpr(MParserParser.GetRelationalExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getEqualTo}
	 * labeled alternative in {@link MParserParser#equality_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetEqualTo(MParserParser.GetEqualToContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getEqualTo}
	 * labeled alternative in {@link MParserParser#equality_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetEqualTo(MParserParser.GetEqualToContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getEqualExpr}
	 * labeled alternative in {@link MParserParser#and_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetEqualExpr(MParserParser.GetEqualExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getEqualExpr}
	 * labeled alternative in {@link MParserParser#and_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetEqualExpr(MParserParser.GetEqualExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getAnd}
	 * labeled alternative in {@link MParserParser#and_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetAnd(MParserParser.GetAndContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getAnd}
	 * labeled alternative in {@link MParserParser#and_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetAnd(MParserParser.GetAndContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getAndExpr}
	 * labeled alternative in {@link MParserParser#exclusive_or_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetAndExpr(MParserParser.GetAndExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getAndExpr}
	 * labeled alternative in {@link MParserParser#exclusive_or_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetAndExpr(MParserParser.GetAndExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getXor}
	 * labeled alternative in {@link MParserParser#exclusive_or_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetXor(MParserParser.GetXorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getXor}
	 * labeled alternative in {@link MParserParser#exclusive_or_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetXor(MParserParser.GetXorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getXorExpr}
	 * labeled alternative in {@link MParserParser#inclusive_or_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetXorExpr(MParserParser.GetXorExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getXorExpr}
	 * labeled alternative in {@link MParserParser#inclusive_or_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetXorExpr(MParserParser.GetXorExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getOr}
	 * labeled alternative in {@link MParserParser#inclusive_or_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetOr(MParserParser.GetOrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getOr}
	 * labeled alternative in {@link MParserParser#inclusive_or_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetOr(MParserParser.GetOrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getOrExpr}
	 * labeled alternative in {@link MParserParser#logical_and_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetOrExpr(MParserParser.GetOrExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getOrExpr}
	 * labeled alternative in {@link MParserParser#logical_and_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetOrExpr(MParserParser.GetOrExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getLogicAnd}
	 * labeled alternative in {@link MParserParser#logical_and_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetLogicAnd(MParserParser.GetLogicAndContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getLogicAnd}
	 * labeled alternative in {@link MParserParser#logical_and_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetLogicAnd(MParserParser.GetLogicAndContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getLogicAndExpr}
	 * labeled alternative in {@link MParserParser#logical_or_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetLogicAndExpr(MParserParser.GetLogicAndExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getLogicAndExpr}
	 * labeled alternative in {@link MParserParser#logical_or_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetLogicAndExpr(MParserParser.GetLogicAndExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getLogicOr}
	 * labeled alternative in {@link MParserParser#logical_or_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetLogicOr(MParserParser.GetLogicOrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getLogicOr}
	 * labeled alternative in {@link MParserParser#logical_or_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetLogicOr(MParserParser.GetLogicOrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getLogicOrExpr}
	 * labeled alternative in {@link MParserParser#assignment_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetLogicOrExpr(MParserParser.GetLogicOrExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getLogicOrExpr}
	 * labeled alternative in {@link MParserParser#assignment_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetLogicOrExpr(MParserParser.GetLogicOrExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getAssignment}
	 * labeled alternative in {@link MParserParser#assignment_expr}.
	 * @param ctx the parse tree
	 */
	void enterGetAssignment(MParserParser.GetAssignmentContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getAssignment}
	 * labeled alternative in {@link MParserParser#assignment_expr}.
	 * @param ctx the parse tree
	 */
	void exitGetAssignment(MParserParser.GetAssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link MParserParser#assignment_operator}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_operator(MParserParser.Assignment_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MParserParser#assignment_operator}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_operator(MParserParser.Assignment_operatorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getAssignmentExpr}
	 * labeled alternative in {@link MParserParser#argument_list}.
	 * @param ctx the parse tree
	 */
	void enterGetAssignmentExpr(MParserParser.GetAssignmentExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getAssignmentExpr}
	 * labeled alternative in {@link MParserParser#argument_list}.
	 * @param ctx the parse tree
	 */
	void exitGetAssignmentExpr(MParserParser.GetAssignmentExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getArguList}
	 * labeled alternative in {@link MParserParser#argument_list}.
	 * @param ctx the parse tree
	 */
	void enterGetArguList(MParserParser.GetArguListContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getArguList}
	 * labeled alternative in {@link MParserParser#argument_list}.
	 * @param ctx the parse tree
	 */
	void exitGetArguList(MParserParser.GetArguListContext ctx);
	/**
	 * Enter a parse tree produced by {@link MParserParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(MParserParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MParserParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(MParserParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MParserParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConstant(MParserParser.ConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link MParserParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConstant(MParserParser.ConstantContext ctx);
}