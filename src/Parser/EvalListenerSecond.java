// Generated from /Users/yhzmiao/IdeaProjects/Compiler/src/Parser/MParser.g4 by ANTLR 4.5.1
package Parser;

import Exception.CompilationError;
import Symbol.SymbolTable;
import Type.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;

/**
 * This class provides an empty implementation of {@link MParserListener},
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
public class EvalListenerSecond implements MParserListener {
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGra(MParserParser.GraContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGra(MParserParser.GraContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    public static SymbolTable symbolTable = new SymbolTable();

    public String name;
    public ArrayList<Type> typelist = new ArrayList<>();
    public ArrayList<String> namelist = new ArrayList<>();
    @Override public void enterGetClassDP(MParserParser.GetClassDPContext ctx) {
        if (symbolTable.typetable.isEmpty()) {
            symbolTable.typetable.add(IntType.instance);
            symbolTable.typetable.add(BoolType.instance);
            symbolTable.typetable.add(StringType.instance);
            symbolTable.typetable.add(VoidType.instance);
            symbolTable.typetable.add(NullType.instance);

            FunctionType print = new FunctionType("print", VoidType.instance);
            print.typelist.add(StringType.instance);
            print.namelist.add("str");

            FunctionType println = new FunctionType("println", VoidType.instance);
            println.typelist.add(StringType.instance);
            println.namelist.add("str");

            FunctionType getString = new FunctionType("getString", StringType.instance);

            FunctionType getInt = new FunctionType("getInt", IntType.instance);

            FunctionType toString = new FunctionType("toString", StringType.instance);
            toString.typelist.add(IntType.instance);
            toString.namelist.add("i");

            symbolTable.typetable.add(print);
            symbolTable.typetable.add(println);
            symbolTable.typetable.add(getInt);
            symbolTable.typetable.add(getString);
            symbolTable.typetable.add(toString);
        }
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetClassDP(MParserParser.GetClassDPContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetFuncDP(MParserParser.GetFuncDPContext ctx) {
        if (symbolTable.typetable.isEmpty()) {
            symbolTable.typetable.add(IntType.instance);
            symbolTable.typetable.add(BoolType.instance);
            symbolTable.typetable.add(StringType.instance);
            symbolTable.typetable.add(VoidType.instance);
            symbolTable.typetable.add(NullType.instance);

            FunctionType print = new FunctionType("print", VoidType.instance);
            print.typelist.add(StringType.instance);
            print.namelist.add("str");

            FunctionType println = new FunctionType("println", VoidType.instance);
            println.typelist.add(StringType.instance);
            println.namelist.add("str");

            FunctionType getString = new FunctionType("getString", StringType.instance);

            FunctionType getInt = new FunctionType("getInt", IntType.instance);

            FunctionType toString = new FunctionType("toString", StringType.instance);
            toString.typelist.add(IntType.instance);
            toString.namelist.add("i");

            symbolTable.typetable.add(print);
            symbolTable.typetable.add(println);
            symbolTable.typetable.add(getInt);
            symbolTable.typetable.add(getString);
            symbolTable.typetable.add(toString);
        }
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetFuncDP(MParserParser.GetFuncDPContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetVarDP(MParserParser.GetVarDPContext ctx) {
        if (symbolTable.typetable.isEmpty()) {
            symbolTable.typetable.add(IntType.instance);
            symbolTable.typetable.add(BoolType.instance);
            symbolTable.typetable.add(StringType.instance);
            symbolTable.typetable.add(VoidType.instance);
            symbolTable.typetable.add(NullType.instance);

            FunctionType print = new FunctionType("print", VoidType.instance);
            print.typelist.add(StringType.instance);
            print.namelist.add("str");

            FunctionType println = new FunctionType("println", VoidType.instance);
            println.typelist.add(StringType.instance);
            println.namelist.add("str");

            FunctionType getString = new FunctionType("getString", StringType.instance);

            FunctionType getInt = new FunctionType("getInt", IntType.instance);

            FunctionType toString = new FunctionType("toString", StringType.instance);
            toString.typelist.add(IntType.instance);
            toString.namelist.add("i");

            symbolTable.typetable.add(print);
            symbolTable.typetable.add(println);
            symbolTable.typetable.add(getInt);
            symbolTable.typetable.add(getString);
            symbolTable.typetable.add(toString);
        }
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetVarDP(MParserParser.GetVarDPContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetNullDP(MParserParser.GetNullDPContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetNullDP(MParserParser.GetNullDPContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterClassDecl(MParserParser.ClassDeclContext ctx) {
        if (!typelist.isEmpty()) {
            typelist.clear();
            namelist.clear();
        }
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitClassDecl(MParserParser.ClassDeclContext ctx) {
        name = ctx.ID().getText();
        for (int i = 0; i < symbolTable.typetable.size(); ++ i) {
            Type tmp = symbolTable.typetable.get(i);
            if (tmp instanceof ClassType && ((ClassType) tmp).name.equals(name)) {
                throw new CompilationError("Double Declaration!");
            }
        }
        ClassType tmpClassType = new ClassType(name);
        for (int i = 0; i < typelist.size(); ++ i) {
            boolean got = false;
            for (int j = 0; j < EvalListenerFirst.nameList.size(); ++ j) {
                if (EvalListenerFirst.nameList.get(j).equals(typelist.get(i))) {
                    got = true;
                    break;
                }
                else if (typelist.get(i) instanceof ArrayType && ((ArrayType) typelist.get(i)).pointer.equals(EvalListenerFirst.nameList.get(j))) {
                    got = true;
                    break;
                }
            }
            if (!got) {
                throw new CompilationError("Undefined Type!");
            }
            tmpClassType.typelist.add(typelist.get(i));
            if (tmpClassType.namelist.contains(namelist.get(i))) {
                throw new CompilationError("Double Declaration!");
            }
            tmpClassType.namelist.add(namelist.get(i));
            //System.out.println(namelist.get(i));
        }
        symbolTable.addType(tmpClassType);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterEndofClass(MParserParser.EndofClassContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitEndofClass(MParserParser.EndofClassContext ctx) {
        String tmptype = ctx.type().getText();
        String tmpname = ctx.ID().getText();
        typelist.add(Type.toType(tmptype));
        namelist.add(tmpname);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterStmtofClass(MParserParser.StmtofClassContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitStmtofClass(MParserParser.StmtofClassContext ctx) {
        String tmptype = ctx.type().getText();
        String tmpname = ctx.ID().getText();
        typelist.add(Type.toType(tmptype));
        namelist.add(tmpname);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetFuncDecl(MParserParser.GetFuncDeclContext ctx) {
        typelist.clear();
        namelist.clear();
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetFuncDecl(MParserParser.GetFuncDeclContext ctx) {
        String name = ctx.func_name().getText();
        for (int i = 0; i < symbolTable.typetable.size(); ++ i) {
            Type tmp = symbolTable.typetable.get(i);
            if (tmp instanceof FunctionType && ((FunctionType) tmp).name.equals(name)) {
                throw new CompilationError("Double Declaration!");
            }
        }
        String type = ctx.type().getText();
        FunctionType tmpFunction = new FunctionType(name, Type.toType(type));
        boolean spy = true;
        if (Type.toType(type) instanceof FunctionType)
            throw new CompilationError("Type Error!");
        if (Type.toType(type) instanceof ClassType) {
            spy = false;
            for (int i = 0; i < symbolTable.typetable.size(); ++ i) {
                if (symbolTable.typetable.get(i) instanceof ClassType && tmpFunction.type.equals(symbolTable.typetable.get(i))) {
                    spy = true;
                    break;
                }
            }
        }
        if (!spy)
            throw new CompilationError("Undefined Type!");
        for (int i = 0; i < typelist.size(); ++ i) {
            boolean got = false;
            for (int j = 0; j < EvalListenerFirst.nameList.size(); ++ j) {
                if (EvalListenerFirst.nameList.get(j).equals(typelist.get(i))) {
                    got = true;
                    break;
                }
                else if (typelist.get(i) instanceof ArrayType && ((ArrayType) typelist.get(i)).pointer.equals(EvalListenerFirst.nameList.get(j))) {
                    got = true;
                    break;
                }
            }
            //System.out.print(((ArrayType)typelist.get(i)).pointer);
            if (!got) {
                throw new CompilationError("Undefined Type!");
            }
            tmpFunction.typelist.add(typelist.get(i));
            if (tmpFunction.namelist.contains(namelist.get(i))) {
                throw new CompilationError("Double Declaration!");
            }
            tmpFunction.namelist.add(namelist.get(i));
        }
        //System.out.println(name);
        symbolTable.addType(tmpFunction);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetVFuncDecl(MParserParser.GetVFuncDeclContext ctx) {
        typelist.clear();
        namelist.clear();
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetVFuncDecl(MParserParser.GetVFuncDeclContext ctx) {
        String name = ctx.func_name().getText();
        String type = "void";
        FunctionType tmpFunction = new FunctionType(name, Type.toType(type));
        for (int i = 0; i < typelist.size(); ++ i) {
            boolean got = false;
            for (int j = 0; j < EvalListenerFirst.nameList.size(); ++ j) {
                if (EvalListenerFirst.nameList.get(j).equals(typelist.get(i))) {
                    got = true;
                    break;
                }
                else if (typelist.get(i) instanceof ArrayType && ((ArrayType) typelist.get(i)).pointer.equals(EvalListenerFirst.nameList.get(j))) {
                    got = true;
                    break;
                }
            }
            if (!got) {
                throw new CompilationError("Undefined Type!");
            }
            tmpFunction.typelist.add(typelist.get(i));
            //System.out.println(typelist.get(typelist.size() - 1 - i));
            if (tmpFunction.namelist.contains(namelist.get(i))) {
                throw new CompilationError("Double Declaration!");
            }
            tmpFunction.namelist.add(namelist.get(i));
        }
        symbolTable.addType(tmpFunction);
    }
    @Override public void enterParaList(MParserParser.ParaListContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitParaList(MParserParser.ParaListContext ctx) {
        String tmptype = ctx.type().getText();
        String tmpname = ctx.ID().getText();
        typelist.add(Type.toType(tmptype));
        namelist.add(tmpname);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterNullParaList(MParserParser.NullParaListContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitNullParaList(MParserParser.NullParaListContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterType(MParserParser.TypeContext ctx) { }
    @Override public void enterDemension(MParserParser.DemensionContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitDemension(MParserParser.DemensionContext ctx) { }


    @Override public void enterSUGAR(MParserParser.SUGARContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitSUGAR(MParserParser.SUGARContext ctx) { }
    @Override public void enterFunc_name(MParserParser.Func_nameContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitFunc_name(MParserParser.Func_nameContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */

    @Override public void enterPositive(MParserParser.PositiveContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitPositive(MParserParser.PositiveContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterNagative(MParserParser.NagativeContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitNagative(MParserParser.NagativeContext ctx) { }
    @Override public void exitType(MParserParser.TypeContext ctx) { }
    @Override public void enterNullBlock(MParserParser.NullBlockContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitNullBlock(MParserParser.NullBlockContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterMultiBlock(MParserParser.MultiBlockContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitMultiBlock(MParserParser.MultiBlockContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterSingleBS(MParserParser.SingleBSContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitSingleBS(MParserParser.SingleBSContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterMultiBS(MParserParser.MultiBSContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitMultiBS(MParserParser.MultiBSContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterBSisS(MParserParser.BSisSContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitBSisS(MParserParser.BSisSContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterBSisVarDecl(MParserParser.BSisVarDeclContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitBSisVarDecl(MParserParser.BSisVarDeclContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterCompStmt(MParserParser.CompStmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitCompStmt(MParserParser.CompStmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterExprStmt(MParserParser.ExprStmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitExprStmt(MParserParser.ExprStmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterSelectStmt(MParserParser.SelectStmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitSelectStmt(MParserParser.SelectStmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterIterStmt(MParserParser.IterStmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitIterStmt(MParserParser.IterStmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterJumpStmt(MParserParser.JumpStmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitJumpStmt(MParserParser.JumpStmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterVarDecl(MParserParser.VarDeclContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitVarDecl(MParserParser.VarDeclContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterVarDeclValue(MParserParser.VarDeclValueContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitVarDeclValue(MParserParser.VarDeclValueContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */

    @Override public void enterReturnExpr(MParserParser.ReturnExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitReturnExpr(MParserParser.ReturnExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterBreakExpr(MParserParser.BreakExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitBreakExpr(MParserParser.BreakExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterContinueExpr(MParserParser.ContinueExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitContinueExpr(MParserParser.ContinueExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterSelection_stmt(MParserParser.Selection_stmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitSelection_stmt(MParserParser.Selection_stmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterIteration_stmt(MParserParser.Iteration_stmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitIteration_stmt(MParserParser.Iteration_stmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterCompound_stmt(MParserParser.Compound_stmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitCompound_stmt(MParserParser.Compound_stmtContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterStmt_list(MParserParser.Stmt_listContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitStmt_list(MParserParser.Stmt_listContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGeneral_expr(MParserParser.General_exprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGeneral_expr(MParserParser.General_exprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetSub(MParserParser.GetSubContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetSub(MParserParser.GetSubContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetPostInc(MParserParser.GetPostIncContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetPostInc(MParserParser.GetPostIncContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetFunc(MParserParser.GetFuncContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetFunc(MParserParser.GetFuncContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetPostDec(MParserParser.GetPostDecContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetPostDec(MParserParser.GetPostDecContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetConstant(MParserParser.GetConstantContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetConstant(MParserParser.GetConstantContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetField(MParserParser.GetFieldContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetField(MParserParser.GetFieldContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetPostExpr(MParserParser.GetPostExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetPostExpr(MParserParser.GetPostExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetPreInc(MParserParser.GetPreIncContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetPreInc(MParserParser.GetPreIncContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetPreDec(MParserParser.GetPreDecContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetPreDec(MParserParser.GetPreDecContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetNotExpr(MParserParser.GetNotExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetNotExpr(MParserParser.GetNotExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterUnary_operator(MParserParser.Unary_operatorContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitUnary_operator(MParserParser.Unary_operatorContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetUnaryExpr(MParserParser.GetUnaryExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetUnaryExpr(MParserParser.GetUnaryExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetNew(MParserParser.GetNewContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetNew(MParserParser.GetNewContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetDivide(MParserParser.GetDivideContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetDivide(MParserParser.GetDivideContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetMultiply(MParserParser.GetMultiplyContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetMultiply(MParserParser.GetMultiplyContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetCreationExpr(MParserParser.GetCreationExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetCreationExpr(MParserParser.GetCreationExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetModulo(MParserParser.GetModuloContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetModulo(MParserParser.GetModuloContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetAdd(MParserParser.GetAddContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetAdd(MParserParser.GetAddContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetMinus(MParserParser.GetMinusContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetMinus(MParserParser.GetMinusContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetMultiplyExpr(MParserParser.GetMultiplyExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetMultiplyExpr(MParserParser.GetMultiplyExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetAddExpr(MParserParser.GetAddExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetAddExpr(MParserParser.GetAddExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetRShift(MParserParser.GetRShiftContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetRShift(MParserParser.GetRShiftContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetLShift(MParserParser.GetLShiftContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetLShift(MParserParser.GetLShiftContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetGreater(MParserParser.GetGreaterContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetGreater(MParserParser.GetGreaterContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetShiftExpr(MParserParser.GetShiftExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetShiftExpr(MParserParser.GetShiftExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetLE(MParserParser.GetLEContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetLE(MParserParser.GetLEContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetLess(MParserParser.GetLessContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetLess(MParserParser.GetLessContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetGE(MParserParser.GetGEContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetGE(MParserParser.GetGEContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetNotEqualTo(MParserParser.GetNotEqualToContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetNotEqualTo(MParserParser.GetNotEqualToContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetRelationalExpr(MParserParser.GetRelationalExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetRelationalExpr(MParserParser.GetRelationalExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetEqualTo(MParserParser.GetEqualToContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetEqualTo(MParserParser.GetEqualToContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetEqualExpr(MParserParser.GetEqualExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetEqualExpr(MParserParser.GetEqualExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetAnd(MParserParser.GetAndContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetAnd(MParserParser.GetAndContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetAndExpr(MParserParser.GetAndExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetAndExpr(MParserParser.GetAndExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetXor(MParserParser.GetXorContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetXor(MParserParser.GetXorContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetXorExpr(MParserParser.GetXorExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetXorExpr(MParserParser.GetXorExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetOr(MParserParser.GetOrContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetOr(MParserParser.GetOrContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetOrExpr(MParserParser.GetOrExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetOrExpr(MParserParser.GetOrExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetLogicAnd(MParserParser.GetLogicAndContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetLogicAnd(MParserParser.GetLogicAndContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetLogicAndExpr(MParserParser.GetLogicAndExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetLogicAndExpr(MParserParser.GetLogicAndExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetLogicOr(MParserParser.GetLogicOrContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetLogicOr(MParserParser.GetLogicOrContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetLogicOrExpr(MParserParser.GetLogicOrExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetLogicOrExpr(MParserParser.GetLogicOrExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetAssignment(MParserParser.GetAssignmentContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetAssignment(MParserParser.GetAssignmentContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterAssignment_operator(MParserParser.Assignment_operatorContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitAssignment_operator(MParserParser.Assignment_operatorContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetAssignmentExpr(MParserParser.GetAssignmentExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetAssignmentExpr(MParserParser.GetAssignmentExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterGetArguList(MParserParser.GetArguListContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitGetArguList(MParserParser.GetArguListContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterExpr(MParserParser.ExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitExpr(MParserParser.ExprContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterConstant(MParserParser.ConstantContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitConstant(MParserParser.ConstantContext ctx) { }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterEveryRule(ParserRuleContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitEveryRule(ParserRuleContext ctx) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void visitTerminal(TerminalNode node) { }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void visitErrorNode(ErrorNode node) { }
}