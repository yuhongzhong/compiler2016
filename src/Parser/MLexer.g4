lexer grammar MLexer;


BOOL : 'bool' ;
INT : 'int' ;
STRING : 'string' ;
NULL : 'null' ;
VOID : 'void' ;
FALSE : 'false' ;
TRUE : 'true' ;
IF : 'if' ;
ELSE : 'else' ;
FOR : 'for' ;
WHILE : 'while' ;
BREAK : 'break' ;
CONTINUE : 'continue' ;
RETURN : 'return' ;
NEW : 'new' ;
CLASS : 'class' ;
SUGAR : 'size' | 'length' | 'parseInt' | 'ord' | 'substring' ;
CSTRING : '"' (ESC|.)*? '"';
fragment ESC : '\\' [btnr"\\] ;
ID : NONDIGIT (NONDIGIT | DIGIT)* ;
NUMBER : DIGIT+;
NONDIGIT : [a-zA-Z_];
DIGIT : [0-9];
WHITESPACE
    :   [ \t]+
        -> skip
    ;
NEWLINE
    :   (   '\r' '\n'?
        |   '\n'
        )
        -> skip
    ;
LineComment
    :   '//' ~[\r\n]*
        -> skip
    ;