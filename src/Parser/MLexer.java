// Generated from /Users/yhzmiao/IdeaProjects/Compiler/src/Parser/MLexer.g4 by ANTLR 4.5.1
package Parser;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.LexerATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		BOOL=1, INT=2, STRING=3, NULL=4, VOID=5, FALSE=6, TRUE=7, IF=8, ELSE=9, 
		FOR=10, WHILE=11, BREAK=12, CONTINUE=13, RETURN=14, NEW=15, CLASS=16, 
		SUGAR=17, CSTRING=18, ID=19, NUMBER=20, NONDIGIT=21, DIGIT=22, WHITESPACE=23, 
		NEWLINE=24, LineComment=25;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"BOOL", "INT", "STRING", "NULL", "VOID", "FALSE", "TRUE", "IF", "ELSE", 
		"FOR", "WHILE", "BREAK", "CONTINUE", "RETURN", "NEW", "CLASS", "SUGAR", 
		"CSTRING", "ESC", "ID", "NUMBER", "NONDIGIT", "DIGIT", "WHITESPACE", "NEWLINE", 
		"LineComment"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'bool'", "'int'", "'string'", "'null'", "'void'", "'false'", "'true'", 
		"'if'", "'else'", "'for'", "'while'", "'break'", "'continue'", "'return'", 
		"'new'", "'class'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "BOOL", "INT", "STRING", "NULL", "VOID", "FALSE", "TRUE", "IF", 
		"ELSE", "FOR", "WHILE", "BREAK", "CONTINUE", "RETURN", "NEW", "CLASS", 
		"SUGAR", "CSTRING", "ID", "NUMBER", "NONDIGIT", "DIGIT", "WHITESPACE", 
		"NEWLINE", "LineComment"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public MLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "MLexer.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\33\u00e7\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\13"+
		"\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3"+
		"\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3"+
		"\17\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3"+
		"\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3"+
		"\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\5\22\u00ad"+
		"\n\22\3\23\3\23\3\23\7\23\u00b2\n\23\f\23\16\23\u00b5\13\23\3\23\3\23"+
		"\3\24\3\24\3\24\3\25\3\25\3\25\7\25\u00bf\n\25\f\25\16\25\u00c2\13\25"+
		"\3\26\6\26\u00c5\n\26\r\26\16\26\u00c6\3\27\3\27\3\30\3\30\3\31\6\31\u00ce"+
		"\n\31\r\31\16\31\u00cf\3\31\3\31\3\32\3\32\5\32\u00d6\n\32\3\32\5\32\u00d9"+
		"\n\32\3\32\3\32\3\33\3\33\3\33\3\33\7\33\u00e1\n\33\f\33\16\33\u00e4\13"+
		"\33\3\33\3\33\3\u00b3\2\34\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25"+
		"\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\2)\25+\26-\27/\30\61\31"+
		"\63\32\65\33\3\2\7\b\2$$^^ddppttvv\5\2C\\aac|\3\2\62;\4\2\13\13\"\"\4"+
		"\2\f\f\17\17\u00f2\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13"+
		"\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2"+
		"\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2"+
		"!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3"+
		"\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\3\67\3\2\2\2\5<\3\2\2\2"+
		"\7@\3\2\2\2\tG\3\2\2\2\13L\3\2\2\2\rQ\3\2\2\2\17W\3\2\2\2\21\\\3\2\2\2"+
		"\23_\3\2\2\2\25d\3\2\2\2\27h\3\2\2\2\31n\3\2\2\2\33t\3\2\2\2\35}\3\2\2"+
		"\2\37\u0084\3\2\2\2!\u0088\3\2\2\2#\u00ac\3\2\2\2%\u00ae\3\2\2\2\'\u00b8"+
		"\3\2\2\2)\u00bb\3\2\2\2+\u00c4\3\2\2\2-\u00c8\3\2\2\2/\u00ca\3\2\2\2\61"+
		"\u00cd\3\2\2\2\63\u00d8\3\2\2\2\65\u00dc\3\2\2\2\678\7d\2\289\7q\2\29"+
		":\7q\2\2:;\7n\2\2;\4\3\2\2\2<=\7k\2\2=>\7p\2\2>?\7v\2\2?\6\3\2\2\2@A\7"+
		"u\2\2AB\7v\2\2BC\7t\2\2CD\7k\2\2DE\7p\2\2EF\7i\2\2F\b\3\2\2\2GH\7p\2\2"+
		"HI\7w\2\2IJ\7n\2\2JK\7n\2\2K\n\3\2\2\2LM\7x\2\2MN\7q\2\2NO\7k\2\2OP\7"+
		"f\2\2P\f\3\2\2\2QR\7h\2\2RS\7c\2\2ST\7n\2\2TU\7u\2\2UV\7g\2\2V\16\3\2"+
		"\2\2WX\7v\2\2XY\7t\2\2YZ\7w\2\2Z[\7g\2\2[\20\3\2\2\2\\]\7k\2\2]^\7h\2"+
		"\2^\22\3\2\2\2_`\7g\2\2`a\7n\2\2ab\7u\2\2bc\7g\2\2c\24\3\2\2\2de\7h\2"+
		"\2ef\7q\2\2fg\7t\2\2g\26\3\2\2\2hi\7y\2\2ij\7j\2\2jk\7k\2\2kl\7n\2\2l"+
		"m\7g\2\2m\30\3\2\2\2no\7d\2\2op\7t\2\2pq\7g\2\2qr\7c\2\2rs\7m\2\2s\32"+
		"\3\2\2\2tu\7e\2\2uv\7q\2\2vw\7p\2\2wx\7v\2\2xy\7k\2\2yz\7p\2\2z{\7w\2"+
		"\2{|\7g\2\2|\34\3\2\2\2}~\7t\2\2~\177\7g\2\2\177\u0080\7v\2\2\u0080\u0081"+
		"\7w\2\2\u0081\u0082\7t\2\2\u0082\u0083\7p\2\2\u0083\36\3\2\2\2\u0084\u0085"+
		"\7p\2\2\u0085\u0086\7g\2\2\u0086\u0087\7y\2\2\u0087 \3\2\2\2\u0088\u0089"+
		"\7e\2\2\u0089\u008a\7n\2\2\u008a\u008b\7c\2\2\u008b\u008c\7u\2\2\u008c"+
		"\u008d\7u\2\2\u008d\"\3\2\2\2\u008e\u008f\7u\2\2\u008f\u0090\7k\2\2\u0090"+
		"\u0091\7|\2\2\u0091\u00ad\7g\2\2\u0092\u0093\7n\2\2\u0093\u0094\7g\2\2"+
		"\u0094\u0095\7p\2\2\u0095\u0096\7i\2\2\u0096\u0097\7v\2\2\u0097\u00ad"+
		"\7j\2\2\u0098\u0099\7r\2\2\u0099\u009a\7c\2\2\u009a\u009b\7t\2\2\u009b"+
		"\u009c\7u\2\2\u009c\u009d\7g\2\2\u009d\u009e\7K\2\2\u009e\u009f\7p\2\2"+
		"\u009f\u00ad\7v\2\2\u00a0\u00a1\7q\2\2\u00a1\u00a2\7t\2\2\u00a2\u00ad"+
		"\7f\2\2\u00a3\u00a4\7u\2\2\u00a4\u00a5\7w\2\2\u00a5\u00a6\7d\2\2\u00a6"+
		"\u00a7\7u\2\2\u00a7\u00a8\7v\2\2\u00a8\u00a9\7t\2\2\u00a9\u00aa\7k\2\2"+
		"\u00aa\u00ab\7p\2\2\u00ab\u00ad\7i\2\2\u00ac\u008e\3\2\2\2\u00ac\u0092"+
		"\3\2\2\2\u00ac\u0098\3\2\2\2\u00ac\u00a0\3\2\2\2\u00ac\u00a3\3\2\2\2\u00ad"+
		"$\3\2\2\2\u00ae\u00b3\7$\2\2\u00af\u00b2\5\'\24\2\u00b0\u00b2\13\2\2\2"+
		"\u00b1\u00af\3\2\2\2\u00b1\u00b0\3\2\2\2\u00b2\u00b5\3\2\2\2\u00b3\u00b4"+
		"\3\2\2\2\u00b3\u00b1\3\2\2\2\u00b4\u00b6\3\2\2\2\u00b5\u00b3\3\2\2\2\u00b6"+
		"\u00b7\7$\2\2\u00b7&\3\2\2\2\u00b8\u00b9\7^\2\2\u00b9\u00ba\t\2\2\2\u00ba"+
		"(\3\2\2\2\u00bb\u00c0\5-\27\2\u00bc\u00bf\5-\27\2\u00bd\u00bf\5/\30\2"+
		"\u00be\u00bc\3\2\2\2\u00be\u00bd\3\2\2\2\u00bf\u00c2\3\2\2\2\u00c0\u00be"+
		"\3\2\2\2\u00c0\u00c1\3\2\2\2\u00c1*\3\2\2\2\u00c2\u00c0\3\2\2\2\u00c3"+
		"\u00c5\5/\30\2\u00c4\u00c3\3\2\2\2\u00c5\u00c6\3\2\2\2\u00c6\u00c4\3\2"+
		"\2\2\u00c6\u00c7\3\2\2\2\u00c7,\3\2\2\2\u00c8\u00c9\t\3\2\2\u00c9.\3\2"+
		"\2\2\u00ca\u00cb\t\4\2\2\u00cb\60\3\2\2\2\u00cc\u00ce\t\5\2\2\u00cd\u00cc"+
		"\3\2\2\2\u00ce\u00cf\3\2\2\2\u00cf\u00cd\3\2\2\2\u00cf\u00d0\3\2\2\2\u00d0"+
		"\u00d1\3\2\2\2\u00d1\u00d2\b\31\2\2\u00d2\62\3\2\2\2\u00d3\u00d5\7\17"+
		"\2\2\u00d4\u00d6\7\f\2\2\u00d5\u00d4\3\2\2\2\u00d5\u00d6\3\2\2\2\u00d6"+
		"\u00d9\3\2\2\2\u00d7\u00d9\7\f\2\2\u00d8\u00d3\3\2\2\2\u00d8\u00d7\3\2"+
		"\2\2\u00d9\u00da\3\2\2\2\u00da\u00db\b\32\2\2\u00db\64\3\2\2\2\u00dc\u00dd"+
		"\7\61\2\2\u00dd\u00de\7\61\2\2\u00de\u00e2\3\2\2\2\u00df\u00e1\n\6\2\2"+
		"\u00e0\u00df\3\2\2\2\u00e1\u00e4\3\2\2\2\u00e2\u00e0\3\2\2\2\u00e2\u00e3"+
		"\3\2\2\2\u00e3\u00e5\3\2\2\2\u00e4\u00e2\3\2\2\2\u00e5\u00e6\b\33\2\2"+
		"\u00e6\66\3\2\2\2\r\2\u00ac\u00b1\u00b3\u00be\u00c0\u00c6\u00cf\u00d5"+
		"\u00d8\u00e2\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}