grammar MParser;
import MLexer;

gra
    :   program
    ;

program
    :   class_decl program  #   getClassDP
    |   func_decl program   #   getFuncDP
    |   var_decl program    #   getVarDP
    |   #   getNullDP
    ;

class_decl
    :   CLASS ID '{' class_body '}' #   classDecl
    ;

class_body
    :   type ID ';' #   endofClass
    |   type ID ';' class_body  #   stmtofClass
    ;

func_decl
    :   type func_name '(' parameter_list ')' block    #   getFuncDecl
    |   VOID func_name '(' parameter_list ')' block    #   getVFuncDecl
    ;

func_name
    :   ID
    ;

parameter_list
    :   type ID (',' parameter_list)?   #   paraList
    |   #   nullParaList
    ;

type
    :   INT
    |   STRING
    |   BOOL
    |   ID
    |   type '['']'
    ;

block
    :   '{''}'  #   NullBlock
    |   '{'block_stmt_list'}'   #   MultiBlock
    ;

block_stmt_list
    :   block_stmt  #   SingleBS
    |   block_stmt block_stmt_list  #   MultiBS
    ;

block_stmt
    :   stmt    #   BSisS
    |   var_decl    #   BSisVarDecl
    ;

iteration_stmt
    :   WHILE '(' cond = expr ')' (block | stmt)
    |   FOR '(' init = expr? ';' cond = expr? ';'  step = expr? ')' (block | stmt)
    ;

stmt
    :   compound_stmt   #   CompStmt
    |   expr? ';'   #   ExprStmt
    |   selection_stmt  #   SelectStmt
    |   iteration_stmt  #   IterStmt
    |   jump_stmt   #   JumpStmt
    ;

var_decl
    :   type ID ';' #   varDecl
    |   type ID '=' expr ';'    #   varDeclValue
    ;

jump_stmt
    :   RETURN expr? ';'    #   ReturnExpr
    |   BREAK ';'   #   BreakExpr
    |   CONTINUE ';'    #   ContinueExpr
    ;

selection_stmt
    :   IF '(' expr ')' (firBlo = block | firStmt = stmt)
    |   IF '(' expr ')' (firBlo = block | firStmt = stmt) ELSE (secBlo = block | secStmt = stmt)
    ;


compound_stmt
    :   '{' stmt_list '}'
    |   '{' '}'
    ;

stmt_list
    :   stmt stmt_list
    |   stmt
    ;

general_expr
    :   ID
    |   CSTRING
    |   constant
    |   '('expr')'
    ;

postfix_expr
    :   general_expr    #   getConstant
    |   postfix_expr '[' expr ']'   #   getSub
    |   postfix_expr '(' argument_list? ')' #   getFunc
    |   postfix_expr '.' SUGAR '(' argument_list ? ')' #    SUGAR
    |   postfix_expr '.' ID #   getField
    |   postfix_expr '++'   #   getPostInc
    |   postfix_expr '--'   #   getPostDec
    ;

unary_expr
    :   postfix_expr    #   getPostExpr
    |   '++' unary_expr #   getPreInc
    |   '--' unary_expr #   getPreDec
    |   unary_operator multiplicative_expr  #   getNotExpr
    |   '+' multiplicative_expr # positive
    |   '-' multiplicative_expr # nagative
    ;

unary_operator
    :   '~' | '!'
    ;

creation_expr
    :   unary_expr  #   getUnaryExpr
    |   NEW type demension*    #   getNew
    ;

demension
    :   '[' expr? ']'
    ;

multiplicative_expr
    :   creation_expr   #   getCreationExpr
    |   m1 = multiplicative_expr '*' m2 = multiplicative_expr #   getMultiply
    |   m1 = multiplicative_expr '/' m2 = multiplicative_expr #   getDivide
    |   m1 = multiplicative_expr '%' m2 = multiplicative_expr #   getModulo
    ;

additive_expr
    :   multiplicative_expr #   getMultiplyExpr
    |   additive_expr '+' multiplicative_expr   #   getAdd
    |   additive_expr '-' multiplicative_expr   #   getMinus
    ;

shift_expr
    :   additive_expr   #   getAddExpr
    |   shift_expr '<<' additive_expr   #   getLShift
    |   shift_expr '>>' additive_expr   #   getRShift
    ;

relational_expr
    :   shift_expr  #   getShiftExpr
    |   relational_expr '<' shift_expr  #   getLess
    |   relational_expr '>' shift_expr  #   getGreater
    |   relational_expr '<=' shift_expr #   getLE
    |   relational_expr '>=' shift_expr #   getGE
    ;

equality_expr
    :   relational_expr #   getRelationalExpr
    |   equality_expr '==' relational_expr  #   getEqualTo
    |   equality_expr '!=' relational_expr  #   getNotEqualTo
    ;

and_expr
    :   equality_expr   #   getEqualExpr
    |   and_expr '&' equality_expr  #   getAnd
    ;

exclusive_or_expr
    :   and_expr    #   getAndExpr
    |   exclusive_or_expr '^' and_expr  #   getXor
    ;

inclusive_or_expr
    :   exclusive_or_expr   #   getXorExpr
    |   inclusive_or_expr '|' exclusive_or_expr #   getOr
    ;

logical_and_expr
    :   inclusive_or_expr   #   getOrExpr
    |   logical_and_expr '&&' inclusive_or_expr #   getLogicAnd
    ;

logical_or_expr
    :   logical_and_expr    #   getLogicAndExpr
    |   logical_or_expr '||' logical_and_expr   #   getLogicOr
    ;

assignment_expr
    :   logical_or_expr #   getLogicOrExpr
    |   unary_expr assignment_operator assignment_expr  #   getAssignment
    ;

assignment_operator
    : '='
    ;

argument_list
    :   assignment_expr #   getAssignmentExpr
    |   argument_list ',' assignment_expr   #   getArguList
    ;

expr
    :   assignment_expr
    ;

constant
    :   TRUE
    |   FALSE
    |   NULL
    |   NUMBER
    ;
