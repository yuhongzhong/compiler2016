// Generated from MParser.g4 by ANTLR 4.5
package Parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MParserParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, BOOL=33, INT=34, STRING=35, NULL=36, VOID=37, FALSE=38, TRUE=39, 
		IF=40, ELSE=41, FOR=42, WHILE=43, BREAK=44, CONTINUE=45, RETURN=46, NEW=47, 
		CLASS=48, SUGAR=49, CSTRING=50, ID=51, NUMBER=52, NONDIGIT=53, DIGIT=54, 
		WHITESPACE=55, NEWLINE=56, LineComment=57;
	public static final int
		RULE_gra = 0, RULE_program = 1, RULE_class_decl = 2, RULE_class_body = 3, 
		RULE_func_decl = 4, RULE_func_name = 5, RULE_parameter_list = 6, RULE_type = 7, 
		RULE_block = 8, RULE_block_stmt_list = 9, RULE_block_stmt = 10, RULE_iteration_stmt = 11, 
		RULE_stmt = 12, RULE_var_decl = 13, RULE_jump_stmt = 14, RULE_selection_stmt = 15, 
		RULE_compound_stmt = 16, RULE_stmt_list = 17, RULE_general_expr = 18, 
		RULE_postfix_expr = 19, RULE_unary_expr = 20, RULE_unary_operator = 21, 
		RULE_creation_expr = 22, RULE_demension = 23, RULE_multiplicative_expr = 24, 
		RULE_additive_expr = 25, RULE_shift_expr = 26, RULE_relational_expr = 27, 
		RULE_equality_expr = 28, RULE_and_expr = 29, RULE_exclusive_or_expr = 30, 
		RULE_inclusive_or_expr = 31, RULE_logical_and_expr = 32, RULE_logical_or_expr = 33, 
		RULE_assignment_expr = 34, RULE_assignment_operator = 35, RULE_argument_list = 36, 
		RULE_expr = 37, RULE_constant = 38;
	public static final String[] ruleNames = {
		"gra", "program", "class_decl", "class_body", "func_decl", "func_name", 
		"parameter_list", "type", "block", "block_stmt_list", "block_stmt", "iteration_stmt", 
		"stmt", "var_decl", "jump_stmt", "selection_stmt", "compound_stmt", "stmt_list", 
		"general_expr", "postfix_expr", "unary_expr", "unary_operator", "creation_expr", 
		"demension", "multiplicative_expr", "additive_expr", "shift_expr", "relational_expr", 
		"equality_expr", "and_expr", "exclusive_or_expr", "inclusive_or_expr", 
		"logical_and_expr", "logical_or_expr", "assignment_expr", "assignment_operator", 
		"argument_list", "expr", "constant"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'{'", "'}'", "';'", "'('", "')'", "','", "'['", "']'", "'='", "'.'", 
		"'++'", "'--'", "'+'", "'-'", "'~'", "'!'", "'*'", "'/'", "'%'", "'<<'", 
		"'>>'", "'<'", "'>'", "'<='", "'>='", "'=='", "'!='", "'&'", "'^'", "'|'", 
		"'&&'", "'||'", "'bool'", "'int'", "'string'", "'null'", "'void'", "'false'", 
		"'true'", "'if'", "'else'", "'for'", "'while'", "'break'", "'continue'", 
		"'return'", "'new'", "'class'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, "BOOL", "INT", "STRING", 
		"NULL", "VOID", "FALSE", "TRUE", "IF", "ELSE", "FOR", "WHILE", "BREAK", 
		"CONTINUE", "RETURN", "NEW", "CLASS", "SUGAR", "CSTRING", "ID", "NUMBER", 
		"NONDIGIT", "DIGIT", "WHITESPACE", "NEWLINE", "LineComment"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "MParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public MParserParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class GraContext extends ParserRuleContext {
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public GraContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_gra; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGra(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGra(this);
		}
	}

	public final GraContext gra() throws RecognitionException {
		GraContext _localctx = new GraContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_gra);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(78);
			program();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProgramContext extends ParserRuleContext {
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
	 
		public ProgramContext() { }
		public void copyFrom(ProgramContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GetClassDPContext extends ProgramContext {
		public Class_declContext class_decl() {
			return getRuleContext(Class_declContext.class,0);
		}
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public GetClassDPContext(ProgramContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetClassDP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetClassDP(this);
		}
	}
	public static class GetNullDPContext extends ProgramContext {
		public GetNullDPContext(ProgramContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetNullDP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetNullDP(this);
		}
	}
	public static class GetVarDPContext extends ProgramContext {
		public Var_declContext var_decl() {
			return getRuleContext(Var_declContext.class,0);
		}
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public GetVarDPContext(ProgramContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetVarDP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetVarDP(this);
		}
	}
	public static class GetFuncDPContext extends ProgramContext {
		public Func_declContext func_decl() {
			return getRuleContext(Func_declContext.class,0);
		}
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public GetFuncDPContext(ProgramContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetFuncDP(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetFuncDP(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_program);
		try {
			setState(90);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				_localctx = new GetClassDPContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(80);
				class_decl();
				setState(81);
				program();
				}
				break;
			case 2:
				_localctx = new GetFuncDPContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(83);
				func_decl();
				setState(84);
				program();
				}
				break;
			case 3:
				_localctx = new GetVarDPContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(86);
				var_decl();
				setState(87);
				program();
				}
				break;
			case 4:
				_localctx = new GetNullDPContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class_declContext extends ParserRuleContext {
		public Class_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class_decl; }
	 
		public Class_declContext() { }
		public void copyFrom(Class_declContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ClassDeclContext extends Class_declContext {
		public TerminalNode CLASS() { return getToken(MParserParser.CLASS, 0); }
		public TerminalNode ID() { return getToken(MParserParser.ID, 0); }
		public Class_bodyContext class_body() {
			return getRuleContext(Class_bodyContext.class,0);
		}
		public ClassDeclContext(Class_declContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterClassDecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitClassDecl(this);
		}
	}

	public final Class_declContext class_decl() throws RecognitionException {
		Class_declContext _localctx = new Class_declContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_class_decl);
		try {
			_localctx = new ClassDeclContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(92);
			match(CLASS);
			setState(93);
			match(ID);
			setState(94);
			match(T__0);
			setState(95);
			class_body();
			setState(96);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class_bodyContext extends ParserRuleContext {
		public Class_bodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class_body; }
	 
		public Class_bodyContext() { }
		public void copyFrom(Class_bodyContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class StmtofClassContext extends Class_bodyContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(MParserParser.ID, 0); }
		public Class_bodyContext class_body() {
			return getRuleContext(Class_bodyContext.class,0);
		}
		public StmtofClassContext(Class_bodyContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterStmtofClass(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitStmtofClass(this);
		}
	}
	public static class EndofClassContext extends Class_bodyContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(MParserParser.ID, 0); }
		public EndofClassContext(Class_bodyContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterEndofClass(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitEndofClass(this);
		}
	}

	public final Class_bodyContext class_body() throws RecognitionException {
		Class_bodyContext _localctx = new Class_bodyContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_class_body);
		try {
			setState(107);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				_localctx = new EndofClassContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(98);
				type(0);
				setState(99);
				match(ID);
				setState(100);
				match(T__2);
				}
				break;
			case 2:
				_localctx = new StmtofClassContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(102);
				type(0);
				setState(103);
				match(ID);
				setState(104);
				match(T__2);
				setState(105);
				class_body();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Func_declContext extends ParserRuleContext {
		public Func_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_func_decl; }
	 
		public Func_declContext() { }
		public void copyFrom(Func_declContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GetVFuncDeclContext extends Func_declContext {
		public TerminalNode VOID() { return getToken(MParserParser.VOID, 0); }
		public Func_nameContext func_name() {
			return getRuleContext(Func_nameContext.class,0);
		}
		public Parameter_listContext parameter_list() {
			return getRuleContext(Parameter_listContext.class,0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public GetVFuncDeclContext(Func_declContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetVFuncDecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetVFuncDecl(this);
		}
	}
	public static class GetFuncDeclContext extends Func_declContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public Func_nameContext func_name() {
			return getRuleContext(Func_nameContext.class,0);
		}
		public Parameter_listContext parameter_list() {
			return getRuleContext(Parameter_listContext.class,0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public GetFuncDeclContext(Func_declContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetFuncDecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetFuncDecl(this);
		}
	}

	public final Func_declContext func_decl() throws RecognitionException {
		Func_declContext _localctx = new Func_declContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_func_decl);
		try {
			setState(123);
			switch (_input.LA(1)) {
			case BOOL:
			case INT:
			case STRING:
			case ID:
				_localctx = new GetFuncDeclContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(109);
				type(0);
				setState(110);
				func_name();
				setState(111);
				match(T__3);
				setState(112);
				parameter_list();
				setState(113);
				match(T__4);
				setState(114);
				block();
				}
				break;
			case VOID:
				_localctx = new GetVFuncDeclContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(116);
				match(VOID);
				setState(117);
				func_name();
				setState(118);
				match(T__3);
				setState(119);
				parameter_list();
				setState(120);
				match(T__4);
				setState(121);
				block();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Func_nameContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(MParserParser.ID, 0); }
		public Func_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_func_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterFunc_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitFunc_name(this);
		}
	}

	public final Func_nameContext func_name() throws RecognitionException {
		Func_nameContext _localctx = new Func_nameContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_func_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(125);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Parameter_listContext extends ParserRuleContext {
		public Parameter_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter_list; }
	 
		public Parameter_listContext() { }
		public void copyFrom(Parameter_listContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NullParaListContext extends Parameter_listContext {
		public NullParaListContext(Parameter_listContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterNullParaList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitNullParaList(this);
		}
	}
	public static class ParaListContext extends Parameter_listContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(MParserParser.ID, 0); }
		public Parameter_listContext parameter_list() {
			return getRuleContext(Parameter_listContext.class,0);
		}
		public ParaListContext(Parameter_listContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterParaList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitParaList(this);
		}
	}

	public final Parameter_listContext parameter_list() throws RecognitionException {
		Parameter_listContext _localctx = new Parameter_listContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_parameter_list);
		int _la;
		try {
			setState(134);
			switch (_input.LA(1)) {
			case BOOL:
			case INT:
			case STRING:
			case ID:
				_localctx = new ParaListContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(127);
				type(0);
				setState(128);
				match(ID);
				setState(131);
				_la = _input.LA(1);
				if (_la==T__5) {
					{
					setState(129);
					match(T__5);
					setState(130);
					parameter_list();
					}
				}

				}
				break;
			case T__4:
				_localctx = new NullParaListContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(MParserParser.INT, 0); }
		public TerminalNode STRING() { return getToken(MParserParser.STRING, 0); }
		public TerminalNode BOOL() { return getToken(MParserParser.BOOL, 0); }
		public TerminalNode ID() { return getToken(MParserParser.ID, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitType(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		return type(0);
	}

	private TypeContext type(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		TypeContext _localctx = new TypeContext(_ctx, _parentState);
		TypeContext _prevctx = _localctx;
		int _startState = 14;
		enterRecursionRule(_localctx, 14, RULE_type, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(141);
			switch (_input.LA(1)) {
			case INT:
				{
				setState(137);
				match(INT);
				}
				break;
			case STRING:
				{
				setState(138);
				match(STRING);
				}
				break;
			case BOOL:
				{
				setState(139);
				match(BOOL);
				}
				break;
			case ID:
				{
				setState(140);
				match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(148);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new TypeContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_type);
					setState(143);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(144);
					match(T__6);
					setState(145);
					match(T__7);
					}
					} 
				}
				setState(150);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
	 
		public BlockContext() { }
		public void copyFrom(BlockContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NullBlockContext extends BlockContext {
		public NullBlockContext(BlockContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterNullBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitNullBlock(this);
		}
	}
	public static class MultiBlockContext extends BlockContext {
		public Block_stmt_listContext block_stmt_list() {
			return getRuleContext(Block_stmt_listContext.class,0);
		}
		public MultiBlockContext(BlockContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterMultiBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitMultiBlock(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_block);
		try {
			setState(157);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				_localctx = new NullBlockContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(151);
				match(T__0);
				setState(152);
				match(T__1);
				}
				break;
			case 2:
				_localctx = new MultiBlockContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(153);
				match(T__0);
				setState(154);
				block_stmt_list();
				setState(155);
				match(T__1);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Block_stmt_listContext extends ParserRuleContext {
		public Block_stmt_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block_stmt_list; }
	 
		public Block_stmt_listContext() { }
		public void copyFrom(Block_stmt_listContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class MultiBSContext extends Block_stmt_listContext {
		public Block_stmtContext block_stmt() {
			return getRuleContext(Block_stmtContext.class,0);
		}
		public Block_stmt_listContext block_stmt_list() {
			return getRuleContext(Block_stmt_listContext.class,0);
		}
		public MultiBSContext(Block_stmt_listContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterMultiBS(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitMultiBS(this);
		}
	}
	public static class SingleBSContext extends Block_stmt_listContext {
		public Block_stmtContext block_stmt() {
			return getRuleContext(Block_stmtContext.class,0);
		}
		public SingleBSContext(Block_stmt_listContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterSingleBS(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitSingleBS(this);
		}
	}

	public final Block_stmt_listContext block_stmt_list() throws RecognitionException {
		Block_stmt_listContext _localctx = new Block_stmt_listContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_block_stmt_list);
		try {
			setState(163);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				_localctx = new SingleBSContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(159);
				block_stmt();
				}
				break;
			case 2:
				_localctx = new MultiBSContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(160);
				block_stmt();
				setState(161);
				block_stmt_list();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Block_stmtContext extends ParserRuleContext {
		public Block_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block_stmt; }
	 
		public Block_stmtContext() { }
		public void copyFrom(Block_stmtContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BSisSContext extends Block_stmtContext {
		public StmtContext stmt() {
			return getRuleContext(StmtContext.class,0);
		}
		public BSisSContext(Block_stmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterBSisS(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitBSisS(this);
		}
	}
	public static class BSisVarDeclContext extends Block_stmtContext {
		public Var_declContext var_decl() {
			return getRuleContext(Var_declContext.class,0);
		}
		public BSisVarDeclContext(Block_stmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterBSisVarDecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitBSisVarDecl(this);
		}
	}

	public final Block_stmtContext block_stmt() throws RecognitionException {
		Block_stmtContext _localctx = new Block_stmtContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_block_stmt);
		try {
			setState(167);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				_localctx = new BSisSContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(165);
				stmt();
				}
				break;
			case 2:
				_localctx = new BSisVarDeclContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(166);
				var_decl();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Iteration_stmtContext extends ParserRuleContext {
		public ExprContext cond;
		public ExprContext init;
		public ExprContext step;
		public TerminalNode WHILE() { return getToken(MParserParser.WHILE, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public StmtContext stmt() {
			return getRuleContext(StmtContext.class,0);
		}
		public TerminalNode FOR() { return getToken(MParserParser.FOR, 0); }
		public Iteration_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_iteration_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterIteration_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitIteration_stmt(this);
		}
	}

	public final Iteration_stmtContext iteration_stmt() throws RecognitionException {
		Iteration_stmtContext _localctx = new Iteration_stmtContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_iteration_stmt);
		int _la;
		try {
			setState(195);
			switch (_input.LA(1)) {
			case WHILE:
				enterOuterAlt(_localctx, 1);
				{
				setState(169);
				match(WHILE);
				setState(170);
				match(T__3);
				setState(171);
				((Iteration_stmtContext)_localctx).cond = expr();
				setState(172);
				match(T__4);
				setState(175);
				switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
				case 1:
					{
					setState(173);
					block();
					}
					break;
				case 2:
					{
					setState(174);
					stmt();
					}
					break;
				}
				}
				break;
			case FOR:
				enterOuterAlt(_localctx, 2);
				{
				setState(177);
				match(FOR);
				setState(178);
				match(T__3);
				setState(180);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << NULL) | (1L << FALSE) | (1L << TRUE) | (1L << NEW) | (1L << CSTRING) | (1L << ID) | (1L << NUMBER))) != 0)) {
					{
					setState(179);
					((Iteration_stmtContext)_localctx).init = expr();
					}
				}

				setState(182);
				match(T__2);
				setState(184);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << NULL) | (1L << FALSE) | (1L << TRUE) | (1L << NEW) | (1L << CSTRING) | (1L << ID) | (1L << NUMBER))) != 0)) {
					{
					setState(183);
					((Iteration_stmtContext)_localctx).cond = expr();
					}
				}

				setState(186);
				match(T__2);
				setState(188);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << NULL) | (1L << FALSE) | (1L << TRUE) | (1L << NEW) | (1L << CSTRING) | (1L << ID) | (1L << NUMBER))) != 0)) {
					{
					setState(187);
					((Iteration_stmtContext)_localctx).step = expr();
					}
				}

				setState(190);
				match(T__4);
				setState(193);
				switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
				case 1:
					{
					setState(191);
					block();
					}
					break;
				case 2:
					{
					setState(192);
					stmt();
					}
					break;
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StmtContext extends ParserRuleContext {
		public StmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stmt; }
	 
		public StmtContext() { }
		public void copyFrom(StmtContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class IterStmtContext extends StmtContext {
		public Iteration_stmtContext iteration_stmt() {
			return getRuleContext(Iteration_stmtContext.class,0);
		}
		public IterStmtContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterIterStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitIterStmt(this);
		}
	}
	public static class CompStmtContext extends StmtContext {
		public Compound_stmtContext compound_stmt() {
			return getRuleContext(Compound_stmtContext.class,0);
		}
		public CompStmtContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterCompStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitCompStmt(this);
		}
	}
	public static class ExprStmtContext extends StmtContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ExprStmtContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterExprStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitExprStmt(this);
		}
	}
	public static class JumpStmtContext extends StmtContext {
		public Jump_stmtContext jump_stmt() {
			return getRuleContext(Jump_stmtContext.class,0);
		}
		public JumpStmtContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterJumpStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitJumpStmt(this);
		}
	}
	public static class SelectStmtContext extends StmtContext {
		public Selection_stmtContext selection_stmt() {
			return getRuleContext(Selection_stmtContext.class,0);
		}
		public SelectStmtContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterSelectStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitSelectStmt(this);
		}
	}

	public final StmtContext stmt() throws RecognitionException {
		StmtContext _localctx = new StmtContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_stmt);
		int _la;
		try {
			setState(205);
			switch (_input.LA(1)) {
			case T__0:
				_localctx = new CompStmtContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(197);
				compound_stmt();
				}
				break;
			case T__2:
			case T__3:
			case T__10:
			case T__11:
			case T__12:
			case T__13:
			case T__14:
			case T__15:
			case NULL:
			case FALSE:
			case TRUE:
			case NEW:
			case CSTRING:
			case ID:
			case NUMBER:
				_localctx = new ExprStmtContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(199);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << NULL) | (1L << FALSE) | (1L << TRUE) | (1L << NEW) | (1L << CSTRING) | (1L << ID) | (1L << NUMBER))) != 0)) {
					{
					setState(198);
					expr();
					}
				}

				setState(201);
				match(T__2);
				}
				break;
			case IF:
				_localctx = new SelectStmtContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(202);
				selection_stmt();
				}
				break;
			case FOR:
			case WHILE:
				_localctx = new IterStmtContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(203);
				iteration_stmt();
				}
				break;
			case BREAK:
			case CONTINUE:
			case RETURN:
				_localctx = new JumpStmtContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(204);
				jump_stmt();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_declContext extends ParserRuleContext {
		public Var_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_decl; }
	 
		public Var_declContext() { }
		public void copyFrom(Var_declContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class VarDeclValueContext extends Var_declContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(MParserParser.ID, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public VarDeclValueContext(Var_declContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterVarDeclValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitVarDeclValue(this);
		}
	}
	public static class VarDeclContext extends Var_declContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(MParserParser.ID, 0); }
		public VarDeclContext(Var_declContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterVarDecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitVarDecl(this);
		}
	}

	public final Var_declContext var_decl() throws RecognitionException {
		Var_declContext _localctx = new Var_declContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_var_decl);
		try {
			setState(217);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				_localctx = new VarDeclContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(207);
				type(0);
				setState(208);
				match(ID);
				setState(209);
				match(T__2);
				}
				break;
			case 2:
				_localctx = new VarDeclValueContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(211);
				type(0);
				setState(212);
				match(ID);
				setState(213);
				match(T__8);
				setState(214);
				expr();
				setState(215);
				match(T__2);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Jump_stmtContext extends ParserRuleContext {
		public Jump_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_jump_stmt; }
	 
		public Jump_stmtContext() { }
		public void copyFrom(Jump_stmtContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ContinueExprContext extends Jump_stmtContext {
		public TerminalNode CONTINUE() { return getToken(MParserParser.CONTINUE, 0); }
		public ContinueExprContext(Jump_stmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterContinueExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitContinueExpr(this);
		}
	}
	public static class ReturnExprContext extends Jump_stmtContext {
		public TerminalNode RETURN() { return getToken(MParserParser.RETURN, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ReturnExprContext(Jump_stmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterReturnExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitReturnExpr(this);
		}
	}
	public static class BreakExprContext extends Jump_stmtContext {
		public TerminalNode BREAK() { return getToken(MParserParser.BREAK, 0); }
		public BreakExprContext(Jump_stmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterBreakExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitBreakExpr(this);
		}
	}

	public final Jump_stmtContext jump_stmt() throws RecognitionException {
		Jump_stmtContext _localctx = new Jump_stmtContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_jump_stmt);
		int _la;
		try {
			setState(228);
			switch (_input.LA(1)) {
			case RETURN:
				_localctx = new ReturnExprContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(219);
				match(RETURN);
				setState(221);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << NULL) | (1L << FALSE) | (1L << TRUE) | (1L << NEW) | (1L << CSTRING) | (1L << ID) | (1L << NUMBER))) != 0)) {
					{
					setState(220);
					expr();
					}
				}

				setState(223);
				match(T__2);
				}
				break;
			case BREAK:
				_localctx = new BreakExprContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(224);
				match(BREAK);
				setState(225);
				match(T__2);
				}
				break;
			case CONTINUE:
				_localctx = new ContinueExprContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(226);
				match(CONTINUE);
				setState(227);
				match(T__2);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Selection_stmtContext extends ParserRuleContext {
		public BlockContext firBlo;
		public StmtContext firStmt;
		public BlockContext secBlo;
		public StmtContext secStmt;
		public TerminalNode IF() { return getToken(MParserParser.IF, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public List<BlockContext> block() {
			return getRuleContexts(BlockContext.class);
		}
		public BlockContext block(int i) {
			return getRuleContext(BlockContext.class,i);
		}
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public TerminalNode ELSE() { return getToken(MParserParser.ELSE, 0); }
		public Selection_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selection_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterSelection_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitSelection_stmt(this);
		}
	}

	public final Selection_stmtContext selection_stmt() throws RecognitionException {
		Selection_stmtContext _localctx = new Selection_stmtContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_selection_stmt);
		try {
			setState(251);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(230);
				match(IF);
				setState(231);
				match(T__3);
				setState(232);
				expr();
				setState(233);
				match(T__4);
				setState(236);
				switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
				case 1:
					{
					setState(234);
					((Selection_stmtContext)_localctx).firBlo = block();
					}
					break;
				case 2:
					{
					setState(235);
					((Selection_stmtContext)_localctx).firStmt = stmt();
					}
					break;
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(238);
				match(IF);
				setState(239);
				match(T__3);
				setState(240);
				expr();
				setState(241);
				match(T__4);
				setState(244);
				switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
				case 1:
					{
					setState(242);
					((Selection_stmtContext)_localctx).firBlo = block();
					}
					break;
				case 2:
					{
					setState(243);
					((Selection_stmtContext)_localctx).firStmt = stmt();
					}
					break;
				}
				setState(246);
				match(ELSE);
				setState(249);
				switch ( getInterpreter().adaptivePredict(_input,23,_ctx) ) {
				case 1:
					{
					setState(247);
					((Selection_stmtContext)_localctx).secBlo = block();
					}
					break;
				case 2:
					{
					setState(248);
					((Selection_stmtContext)_localctx).secStmt = stmt();
					}
					break;
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Compound_stmtContext extends ParserRuleContext {
		public Stmt_listContext stmt_list() {
			return getRuleContext(Stmt_listContext.class,0);
		}
		public Compound_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compound_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterCompound_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitCompound_stmt(this);
		}
	}

	public final Compound_stmtContext compound_stmt() throws RecognitionException {
		Compound_stmtContext _localctx = new Compound_stmtContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_compound_stmt);
		try {
			setState(259);
			switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(253);
				match(T__0);
				setState(254);
				stmt_list();
				setState(255);
				match(T__1);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(257);
				match(T__0);
				setState(258);
				match(T__1);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Stmt_listContext extends ParserRuleContext {
		public StmtContext stmt() {
			return getRuleContext(StmtContext.class,0);
		}
		public Stmt_listContext stmt_list() {
			return getRuleContext(Stmt_listContext.class,0);
		}
		public Stmt_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stmt_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterStmt_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitStmt_list(this);
		}
	}

	public final Stmt_listContext stmt_list() throws RecognitionException {
		Stmt_listContext _localctx = new Stmt_listContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_stmt_list);
		try {
			setState(265);
			switch ( getInterpreter().adaptivePredict(_input,26,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(261);
				stmt();
				setState(262);
				stmt_list();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(264);
				stmt();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class General_exprContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(MParserParser.ID, 0); }
		public TerminalNode CSTRING() { return getToken(MParserParser.CSTRING, 0); }
		public ConstantContext constant() {
			return getRuleContext(ConstantContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public General_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_general_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGeneral_expr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGeneral_expr(this);
		}
	}

	public final General_exprContext general_expr() throws RecognitionException {
		General_exprContext _localctx = new General_exprContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_general_expr);
		try {
			setState(274);
			switch (_input.LA(1)) {
			case ID:
				enterOuterAlt(_localctx, 1);
				{
				setState(267);
				match(ID);
				}
				break;
			case CSTRING:
				enterOuterAlt(_localctx, 2);
				{
				setState(268);
				match(CSTRING);
				}
				break;
			case NULL:
			case FALSE:
			case TRUE:
			case NUMBER:
				enterOuterAlt(_localctx, 3);
				{
				setState(269);
				constant();
				}
				break;
			case T__3:
				enterOuterAlt(_localctx, 4);
				{
				setState(270);
				match(T__3);
				setState(271);
				expr();
				setState(272);
				match(T__4);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Postfix_exprContext extends ParserRuleContext {
		public Postfix_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postfix_expr; }
	 
		public Postfix_exprContext() { }
		public void copyFrom(Postfix_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GetSubContext extends Postfix_exprContext {
		public Postfix_exprContext postfix_expr() {
			return getRuleContext(Postfix_exprContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public GetSubContext(Postfix_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetSub(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetSub(this);
		}
	}
	public static class GetPostIncContext extends Postfix_exprContext {
		public Postfix_exprContext postfix_expr() {
			return getRuleContext(Postfix_exprContext.class,0);
		}
		public GetPostIncContext(Postfix_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetPostInc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetPostInc(this);
		}
	}
	public static class GetFuncContext extends Postfix_exprContext {
		public Postfix_exprContext postfix_expr() {
			return getRuleContext(Postfix_exprContext.class,0);
		}
		public Argument_listContext argument_list() {
			return getRuleContext(Argument_listContext.class,0);
		}
		public GetFuncContext(Postfix_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetFunc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetFunc(this);
		}
	}
	public static class GetPostDecContext extends Postfix_exprContext {
		public Postfix_exprContext postfix_expr() {
			return getRuleContext(Postfix_exprContext.class,0);
		}
		public GetPostDecContext(Postfix_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetPostDec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetPostDec(this);
		}
	}
	public static class GetConstantContext extends Postfix_exprContext {
		public General_exprContext general_expr() {
			return getRuleContext(General_exprContext.class,0);
		}
		public GetConstantContext(Postfix_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetConstant(this);
		}
	}
	public static class GetFieldContext extends Postfix_exprContext {
		public Postfix_exprContext postfix_expr() {
			return getRuleContext(Postfix_exprContext.class,0);
		}
		public TerminalNode ID() { return getToken(MParserParser.ID, 0); }
		public GetFieldContext(Postfix_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetField(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetField(this);
		}
	}
	public static class SUGARContext extends Postfix_exprContext {
		public Postfix_exprContext postfix_expr() {
			return getRuleContext(Postfix_exprContext.class,0);
		}
		public TerminalNode SUGAR() { return getToken(MParserParser.SUGAR, 0); }
		public Argument_listContext argument_list() {
			return getRuleContext(Argument_listContext.class,0);
		}
		public SUGARContext(Postfix_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterSUGAR(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitSUGAR(this);
		}
	}

	public final Postfix_exprContext postfix_expr() throws RecognitionException {
		return postfix_expr(0);
	}

	private Postfix_exprContext postfix_expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Postfix_exprContext _localctx = new Postfix_exprContext(_ctx, _parentState);
		Postfix_exprContext _prevctx = _localctx;
		int _startState = 38;
		enterRecursionRule(_localctx, 38, RULE_postfix_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new GetConstantContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(277);
			general_expr();
			}
			_ctx.stop = _input.LT(-1);
			setState(307);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,31,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(305);
					switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
					case 1:
						{
						_localctx = new GetSubContext(new Postfix_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_postfix_expr);
						setState(279);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(280);
						match(T__6);
						setState(281);
						expr();
						setState(282);
						match(T__7);
						}
						break;
					case 2:
						{
						_localctx = new GetFuncContext(new Postfix_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_postfix_expr);
						setState(284);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(285);
						match(T__3);
						setState(287);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << NULL) | (1L << FALSE) | (1L << TRUE) | (1L << NEW) | (1L << CSTRING) | (1L << ID) | (1L << NUMBER))) != 0)) {
							{
							setState(286);
							argument_list(0);
							}
						}

						setState(289);
						match(T__4);
						}
						break;
					case 3:
						{
						_localctx = new SUGARContext(new Postfix_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_postfix_expr);
						setState(290);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(291);
						match(T__9);
						setState(292);
						match(SUGAR);
						setState(293);
						match(T__3);
						setState(295);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << NULL) | (1L << FALSE) | (1L << TRUE) | (1L << NEW) | (1L << CSTRING) | (1L << ID) | (1L << NUMBER))) != 0)) {
							{
							setState(294);
							argument_list(0);
							}
						}

						setState(297);
						match(T__4);
						}
						break;
					case 4:
						{
						_localctx = new GetFieldContext(new Postfix_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_postfix_expr);
						setState(298);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(299);
						match(T__9);
						setState(300);
						match(ID);
						}
						break;
					case 5:
						{
						_localctx = new GetPostIncContext(new Postfix_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_postfix_expr);
						setState(301);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(302);
						match(T__10);
						}
						break;
					case 6:
						{
						_localctx = new GetPostDecContext(new Postfix_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_postfix_expr);
						setState(303);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(304);
						match(T__11);
						}
						break;
					}
					} 
				}
				setState(309);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,31,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Unary_exprContext extends ParserRuleContext {
		public Unary_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unary_expr; }
	 
		public Unary_exprContext() { }
		public void copyFrom(Unary_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GetPostExprContext extends Unary_exprContext {
		public Postfix_exprContext postfix_expr() {
			return getRuleContext(Postfix_exprContext.class,0);
		}
		public GetPostExprContext(Unary_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetPostExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetPostExpr(this);
		}
	}
	public static class GetPreIncContext extends Unary_exprContext {
		public Unary_exprContext unary_expr() {
			return getRuleContext(Unary_exprContext.class,0);
		}
		public GetPreIncContext(Unary_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetPreInc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetPreInc(this);
		}
	}
	public static class GetNotExprContext extends Unary_exprContext {
		public Unary_operatorContext unary_operator() {
			return getRuleContext(Unary_operatorContext.class,0);
		}
		public Multiplicative_exprContext multiplicative_expr() {
			return getRuleContext(Multiplicative_exprContext.class,0);
		}
		public GetNotExprContext(Unary_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetNotExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetNotExpr(this);
		}
	}
	public static class PositiveContext extends Unary_exprContext {
		public Multiplicative_exprContext multiplicative_expr() {
			return getRuleContext(Multiplicative_exprContext.class,0);
		}
		public PositiveContext(Unary_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterPositive(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitPositive(this);
		}
	}
	public static class GetPreDecContext extends Unary_exprContext {
		public Unary_exprContext unary_expr() {
			return getRuleContext(Unary_exprContext.class,0);
		}
		public GetPreDecContext(Unary_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetPreDec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetPreDec(this);
		}
	}
	public static class NagativeContext extends Unary_exprContext {
		public Multiplicative_exprContext multiplicative_expr() {
			return getRuleContext(Multiplicative_exprContext.class,0);
		}
		public NagativeContext(Unary_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterNagative(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitNagative(this);
		}
	}

	public final Unary_exprContext unary_expr() throws RecognitionException {
		Unary_exprContext _localctx = new Unary_exprContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_unary_expr);
		try {
			setState(322);
			switch (_input.LA(1)) {
			case T__3:
			case NULL:
			case FALSE:
			case TRUE:
			case CSTRING:
			case ID:
			case NUMBER:
				_localctx = new GetPostExprContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(310);
				postfix_expr(0);
				}
				break;
			case T__10:
				_localctx = new GetPreIncContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(311);
				match(T__10);
				setState(312);
				unary_expr();
				}
				break;
			case T__11:
				_localctx = new GetPreDecContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(313);
				match(T__11);
				setState(314);
				unary_expr();
				}
				break;
			case T__14:
			case T__15:
				_localctx = new GetNotExprContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(315);
				unary_operator();
				setState(316);
				multiplicative_expr(0);
				}
				break;
			case T__12:
				_localctx = new PositiveContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(318);
				match(T__12);
				setState(319);
				multiplicative_expr(0);
				}
				break;
			case T__13:
				_localctx = new NagativeContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(320);
				match(T__13);
				setState(321);
				multiplicative_expr(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Unary_operatorContext extends ParserRuleContext {
		public Unary_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unary_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterUnary_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitUnary_operator(this);
		}
	}

	public final Unary_operatorContext unary_operator() throws RecognitionException {
		Unary_operatorContext _localctx = new Unary_operatorContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_unary_operator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(324);
			_la = _input.LA(1);
			if ( !(_la==T__14 || _la==T__15) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Creation_exprContext extends ParserRuleContext {
		public Creation_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_creation_expr; }
	 
		public Creation_exprContext() { }
		public void copyFrom(Creation_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GetNewContext extends Creation_exprContext {
		public TerminalNode NEW() { return getToken(MParserParser.NEW, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public List<DemensionContext> demension() {
			return getRuleContexts(DemensionContext.class);
		}
		public DemensionContext demension(int i) {
			return getRuleContext(DemensionContext.class,i);
		}
		public GetNewContext(Creation_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetNew(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetNew(this);
		}
	}
	public static class GetUnaryExprContext extends Creation_exprContext {
		public Unary_exprContext unary_expr() {
			return getRuleContext(Unary_exprContext.class,0);
		}
		public GetUnaryExprContext(Creation_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetUnaryExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetUnaryExpr(this);
		}
	}

	public final Creation_exprContext creation_expr() throws RecognitionException {
		Creation_exprContext _localctx = new Creation_exprContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_creation_expr);
		try {
			int _alt;
			setState(335);
			switch (_input.LA(1)) {
			case T__3:
			case T__10:
			case T__11:
			case T__12:
			case T__13:
			case T__14:
			case T__15:
			case NULL:
			case FALSE:
			case TRUE:
			case CSTRING:
			case ID:
			case NUMBER:
				_localctx = new GetUnaryExprContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(326);
				unary_expr();
				}
				break;
			case NEW:
				_localctx = new GetNewContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(327);
				match(NEW);
				setState(328);
				type(0);
				setState(332);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(329);
						demension();
						}
						} 
					}
					setState(334);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DemensionContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public DemensionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_demension; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterDemension(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitDemension(this);
		}
	}

	public final DemensionContext demension() throws RecognitionException {
		DemensionContext _localctx = new DemensionContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_demension);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(337);
			match(T__6);
			setState(339);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << NULL) | (1L << FALSE) | (1L << TRUE) | (1L << NEW) | (1L << CSTRING) | (1L << ID) | (1L << NUMBER))) != 0)) {
				{
				setState(338);
				expr();
				}
			}

			setState(341);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Multiplicative_exprContext extends ParserRuleContext {
		public Multiplicative_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiplicative_expr; }
	 
		public Multiplicative_exprContext() { }
		public void copyFrom(Multiplicative_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GetDivideContext extends Multiplicative_exprContext {
		public Multiplicative_exprContext m1;
		public Multiplicative_exprContext m2;
		public List<Multiplicative_exprContext> multiplicative_expr() {
			return getRuleContexts(Multiplicative_exprContext.class);
		}
		public Multiplicative_exprContext multiplicative_expr(int i) {
			return getRuleContext(Multiplicative_exprContext.class,i);
		}
		public GetDivideContext(Multiplicative_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetDivide(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetDivide(this);
		}
	}
	public static class GetMultiplyContext extends Multiplicative_exprContext {
		public Multiplicative_exprContext m1;
		public Multiplicative_exprContext m2;
		public List<Multiplicative_exprContext> multiplicative_expr() {
			return getRuleContexts(Multiplicative_exprContext.class);
		}
		public Multiplicative_exprContext multiplicative_expr(int i) {
			return getRuleContext(Multiplicative_exprContext.class,i);
		}
		public GetMultiplyContext(Multiplicative_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetMultiply(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetMultiply(this);
		}
	}
	public static class GetCreationExprContext extends Multiplicative_exprContext {
		public Creation_exprContext creation_expr() {
			return getRuleContext(Creation_exprContext.class,0);
		}
		public GetCreationExprContext(Multiplicative_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetCreationExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetCreationExpr(this);
		}
	}
	public static class GetModuloContext extends Multiplicative_exprContext {
		public Multiplicative_exprContext m1;
		public Multiplicative_exprContext m2;
		public List<Multiplicative_exprContext> multiplicative_expr() {
			return getRuleContexts(Multiplicative_exprContext.class);
		}
		public Multiplicative_exprContext multiplicative_expr(int i) {
			return getRuleContext(Multiplicative_exprContext.class,i);
		}
		public GetModuloContext(Multiplicative_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetModulo(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetModulo(this);
		}
	}

	public final Multiplicative_exprContext multiplicative_expr() throws RecognitionException {
		return multiplicative_expr(0);
	}

	private Multiplicative_exprContext multiplicative_expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Multiplicative_exprContext _localctx = new Multiplicative_exprContext(_ctx, _parentState);
		Multiplicative_exprContext _prevctx = _localctx;
		int _startState = 48;
		enterRecursionRule(_localctx, 48, RULE_multiplicative_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new GetCreationExprContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(344);
			creation_expr();
			}
			_ctx.stop = _input.LT(-1);
			setState(357);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,37,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(355);
					switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
					case 1:
						{
						_localctx = new GetMultiplyContext(new Multiplicative_exprContext(_parentctx, _parentState));
						((GetMultiplyContext)_localctx).m1 = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_multiplicative_expr);
						setState(346);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(347);
						match(T__16);
						setState(348);
						((GetMultiplyContext)_localctx).m2 = multiplicative_expr(4);
						}
						break;
					case 2:
						{
						_localctx = new GetDivideContext(new Multiplicative_exprContext(_parentctx, _parentState));
						((GetDivideContext)_localctx).m1 = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_multiplicative_expr);
						setState(349);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(350);
						match(T__17);
						setState(351);
						((GetDivideContext)_localctx).m2 = multiplicative_expr(3);
						}
						break;
					case 3:
						{
						_localctx = new GetModuloContext(new Multiplicative_exprContext(_parentctx, _parentState));
						((GetModuloContext)_localctx).m1 = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_multiplicative_expr);
						setState(352);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(353);
						match(T__18);
						setState(354);
						((GetModuloContext)_localctx).m2 = multiplicative_expr(2);
						}
						break;
					}
					} 
				}
				setState(359);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,37,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Additive_exprContext extends ParserRuleContext {
		public Additive_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_additive_expr; }
	 
		public Additive_exprContext() { }
		public void copyFrom(Additive_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GetAddContext extends Additive_exprContext {
		public Additive_exprContext additive_expr() {
			return getRuleContext(Additive_exprContext.class,0);
		}
		public Multiplicative_exprContext multiplicative_expr() {
			return getRuleContext(Multiplicative_exprContext.class,0);
		}
		public GetAddContext(Additive_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetAdd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetAdd(this);
		}
	}
	public static class GetMinusContext extends Additive_exprContext {
		public Additive_exprContext additive_expr() {
			return getRuleContext(Additive_exprContext.class,0);
		}
		public Multiplicative_exprContext multiplicative_expr() {
			return getRuleContext(Multiplicative_exprContext.class,0);
		}
		public GetMinusContext(Additive_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetMinus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetMinus(this);
		}
	}
	public static class GetMultiplyExprContext extends Additive_exprContext {
		public Multiplicative_exprContext multiplicative_expr() {
			return getRuleContext(Multiplicative_exprContext.class,0);
		}
		public GetMultiplyExprContext(Additive_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetMultiplyExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetMultiplyExpr(this);
		}
	}

	public final Additive_exprContext additive_expr() throws RecognitionException {
		return additive_expr(0);
	}

	private Additive_exprContext additive_expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Additive_exprContext _localctx = new Additive_exprContext(_ctx, _parentState);
		Additive_exprContext _prevctx = _localctx;
		int _startState = 50;
		enterRecursionRule(_localctx, 50, RULE_additive_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new GetMultiplyExprContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(361);
			multiplicative_expr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(371);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,39,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(369);
					switch ( getInterpreter().adaptivePredict(_input,38,_ctx) ) {
					case 1:
						{
						_localctx = new GetAddContext(new Additive_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_additive_expr);
						setState(363);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(364);
						match(T__12);
						setState(365);
						multiplicative_expr(0);
						}
						break;
					case 2:
						{
						_localctx = new GetMinusContext(new Additive_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_additive_expr);
						setState(366);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(367);
						match(T__13);
						setState(368);
						multiplicative_expr(0);
						}
						break;
					}
					} 
				}
				setState(373);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,39,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Shift_exprContext extends ParserRuleContext {
		public Shift_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_shift_expr; }
	 
		public Shift_exprContext() { }
		public void copyFrom(Shift_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GetAddExprContext extends Shift_exprContext {
		public Additive_exprContext additive_expr() {
			return getRuleContext(Additive_exprContext.class,0);
		}
		public GetAddExprContext(Shift_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetAddExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetAddExpr(this);
		}
	}
	public static class GetRShiftContext extends Shift_exprContext {
		public Shift_exprContext shift_expr() {
			return getRuleContext(Shift_exprContext.class,0);
		}
		public Additive_exprContext additive_expr() {
			return getRuleContext(Additive_exprContext.class,0);
		}
		public GetRShiftContext(Shift_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetRShift(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetRShift(this);
		}
	}
	public static class GetLShiftContext extends Shift_exprContext {
		public Shift_exprContext shift_expr() {
			return getRuleContext(Shift_exprContext.class,0);
		}
		public Additive_exprContext additive_expr() {
			return getRuleContext(Additive_exprContext.class,0);
		}
		public GetLShiftContext(Shift_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetLShift(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetLShift(this);
		}
	}

	public final Shift_exprContext shift_expr() throws RecognitionException {
		return shift_expr(0);
	}

	private Shift_exprContext shift_expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Shift_exprContext _localctx = new Shift_exprContext(_ctx, _parentState);
		Shift_exprContext _prevctx = _localctx;
		int _startState = 52;
		enterRecursionRule(_localctx, 52, RULE_shift_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new GetAddExprContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(375);
			additive_expr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(385);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,41,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(383);
					switch ( getInterpreter().adaptivePredict(_input,40,_ctx) ) {
					case 1:
						{
						_localctx = new GetLShiftContext(new Shift_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_shift_expr);
						setState(377);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(378);
						match(T__19);
						setState(379);
						additive_expr(0);
						}
						break;
					case 2:
						{
						_localctx = new GetRShiftContext(new Shift_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_shift_expr);
						setState(380);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(381);
						match(T__20);
						setState(382);
						additive_expr(0);
						}
						break;
					}
					} 
				}
				setState(387);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,41,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Relational_exprContext extends ParserRuleContext {
		public Relational_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relational_expr; }
	 
		public Relational_exprContext() { }
		public void copyFrom(Relational_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GetGreaterContext extends Relational_exprContext {
		public Relational_exprContext relational_expr() {
			return getRuleContext(Relational_exprContext.class,0);
		}
		public Shift_exprContext shift_expr() {
			return getRuleContext(Shift_exprContext.class,0);
		}
		public GetGreaterContext(Relational_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetGreater(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetGreater(this);
		}
	}
	public static class GetShiftExprContext extends Relational_exprContext {
		public Shift_exprContext shift_expr() {
			return getRuleContext(Shift_exprContext.class,0);
		}
		public GetShiftExprContext(Relational_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetShiftExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetShiftExpr(this);
		}
	}
	public static class GetLEContext extends Relational_exprContext {
		public Relational_exprContext relational_expr() {
			return getRuleContext(Relational_exprContext.class,0);
		}
		public Shift_exprContext shift_expr() {
			return getRuleContext(Shift_exprContext.class,0);
		}
		public GetLEContext(Relational_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetLE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetLE(this);
		}
	}
	public static class GetLessContext extends Relational_exprContext {
		public Relational_exprContext relational_expr() {
			return getRuleContext(Relational_exprContext.class,0);
		}
		public Shift_exprContext shift_expr() {
			return getRuleContext(Shift_exprContext.class,0);
		}
		public GetLessContext(Relational_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetLess(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetLess(this);
		}
	}
	public static class GetGEContext extends Relational_exprContext {
		public Relational_exprContext relational_expr() {
			return getRuleContext(Relational_exprContext.class,0);
		}
		public Shift_exprContext shift_expr() {
			return getRuleContext(Shift_exprContext.class,0);
		}
		public GetGEContext(Relational_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetGE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetGE(this);
		}
	}

	public final Relational_exprContext relational_expr() throws RecognitionException {
		return relational_expr(0);
	}

	private Relational_exprContext relational_expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Relational_exprContext _localctx = new Relational_exprContext(_ctx, _parentState);
		Relational_exprContext _prevctx = _localctx;
		int _startState = 54;
		enterRecursionRule(_localctx, 54, RULE_relational_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new GetShiftExprContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(389);
			shift_expr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(405);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,43,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(403);
					switch ( getInterpreter().adaptivePredict(_input,42,_ctx) ) {
					case 1:
						{
						_localctx = new GetLessContext(new Relational_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_relational_expr);
						setState(391);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(392);
						match(T__21);
						setState(393);
						shift_expr(0);
						}
						break;
					case 2:
						{
						_localctx = new GetGreaterContext(new Relational_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_relational_expr);
						setState(394);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(395);
						match(T__22);
						setState(396);
						shift_expr(0);
						}
						break;
					case 3:
						{
						_localctx = new GetLEContext(new Relational_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_relational_expr);
						setState(397);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(398);
						match(T__23);
						setState(399);
						shift_expr(0);
						}
						break;
					case 4:
						{
						_localctx = new GetGEContext(new Relational_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_relational_expr);
						setState(400);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(401);
						match(T__24);
						setState(402);
						shift_expr(0);
						}
						break;
					}
					} 
				}
				setState(407);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,43,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Equality_exprContext extends ParserRuleContext {
		public Equality_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equality_expr; }
	 
		public Equality_exprContext() { }
		public void copyFrom(Equality_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GetNotEqualToContext extends Equality_exprContext {
		public Equality_exprContext equality_expr() {
			return getRuleContext(Equality_exprContext.class,0);
		}
		public Relational_exprContext relational_expr() {
			return getRuleContext(Relational_exprContext.class,0);
		}
		public GetNotEqualToContext(Equality_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetNotEqualTo(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetNotEqualTo(this);
		}
	}
	public static class GetRelationalExprContext extends Equality_exprContext {
		public Relational_exprContext relational_expr() {
			return getRuleContext(Relational_exprContext.class,0);
		}
		public GetRelationalExprContext(Equality_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetRelationalExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetRelationalExpr(this);
		}
	}
	public static class GetEqualToContext extends Equality_exprContext {
		public Equality_exprContext equality_expr() {
			return getRuleContext(Equality_exprContext.class,0);
		}
		public Relational_exprContext relational_expr() {
			return getRuleContext(Relational_exprContext.class,0);
		}
		public GetEqualToContext(Equality_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetEqualTo(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetEqualTo(this);
		}
	}

	public final Equality_exprContext equality_expr() throws RecognitionException {
		return equality_expr(0);
	}

	private Equality_exprContext equality_expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Equality_exprContext _localctx = new Equality_exprContext(_ctx, _parentState);
		Equality_exprContext _prevctx = _localctx;
		int _startState = 56;
		enterRecursionRule(_localctx, 56, RULE_equality_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new GetRelationalExprContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(409);
			relational_expr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(419);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,45,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(417);
					switch ( getInterpreter().adaptivePredict(_input,44,_ctx) ) {
					case 1:
						{
						_localctx = new GetEqualToContext(new Equality_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_equality_expr);
						setState(411);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(412);
						match(T__25);
						setState(413);
						relational_expr(0);
						}
						break;
					case 2:
						{
						_localctx = new GetNotEqualToContext(new Equality_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_equality_expr);
						setState(414);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(415);
						match(T__26);
						setState(416);
						relational_expr(0);
						}
						break;
					}
					} 
				}
				setState(421);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,45,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class And_exprContext extends ParserRuleContext {
		public And_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_and_expr; }
	 
		public And_exprContext() { }
		public void copyFrom(And_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GetEqualExprContext extends And_exprContext {
		public Equality_exprContext equality_expr() {
			return getRuleContext(Equality_exprContext.class,0);
		}
		public GetEqualExprContext(And_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetEqualExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetEqualExpr(this);
		}
	}
	public static class GetAndContext extends And_exprContext {
		public And_exprContext and_expr() {
			return getRuleContext(And_exprContext.class,0);
		}
		public Equality_exprContext equality_expr() {
			return getRuleContext(Equality_exprContext.class,0);
		}
		public GetAndContext(And_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetAnd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetAnd(this);
		}
	}

	public final And_exprContext and_expr() throws RecognitionException {
		return and_expr(0);
	}

	private And_exprContext and_expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		And_exprContext _localctx = new And_exprContext(_ctx, _parentState);
		And_exprContext _prevctx = _localctx;
		int _startState = 58;
		enterRecursionRule(_localctx, 58, RULE_and_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new GetEqualExprContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(423);
			equality_expr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(430);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,46,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new GetAndContext(new And_exprContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_and_expr);
					setState(425);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(426);
					match(T__27);
					setState(427);
					equality_expr(0);
					}
					} 
				}
				setState(432);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,46,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Exclusive_or_exprContext extends ParserRuleContext {
		public Exclusive_or_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exclusive_or_expr; }
	 
		public Exclusive_or_exprContext() { }
		public void copyFrom(Exclusive_or_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GetAndExprContext extends Exclusive_or_exprContext {
		public And_exprContext and_expr() {
			return getRuleContext(And_exprContext.class,0);
		}
		public GetAndExprContext(Exclusive_or_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetAndExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetAndExpr(this);
		}
	}
	public static class GetXorContext extends Exclusive_or_exprContext {
		public Exclusive_or_exprContext exclusive_or_expr() {
			return getRuleContext(Exclusive_or_exprContext.class,0);
		}
		public And_exprContext and_expr() {
			return getRuleContext(And_exprContext.class,0);
		}
		public GetXorContext(Exclusive_or_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetXor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetXor(this);
		}
	}

	public final Exclusive_or_exprContext exclusive_or_expr() throws RecognitionException {
		return exclusive_or_expr(0);
	}

	private Exclusive_or_exprContext exclusive_or_expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Exclusive_or_exprContext _localctx = new Exclusive_or_exprContext(_ctx, _parentState);
		Exclusive_or_exprContext _prevctx = _localctx;
		int _startState = 60;
		enterRecursionRule(_localctx, 60, RULE_exclusive_or_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new GetAndExprContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(434);
			and_expr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(441);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,47,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new GetXorContext(new Exclusive_or_exprContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_exclusive_or_expr);
					setState(436);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(437);
					match(T__28);
					setState(438);
					and_expr(0);
					}
					} 
				}
				setState(443);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,47,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Inclusive_or_exprContext extends ParserRuleContext {
		public Inclusive_or_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inclusive_or_expr; }
	 
		public Inclusive_or_exprContext() { }
		public void copyFrom(Inclusive_or_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GetXorExprContext extends Inclusive_or_exprContext {
		public Exclusive_or_exprContext exclusive_or_expr() {
			return getRuleContext(Exclusive_or_exprContext.class,0);
		}
		public GetXorExprContext(Inclusive_or_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetXorExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetXorExpr(this);
		}
	}
	public static class GetOrContext extends Inclusive_or_exprContext {
		public Inclusive_or_exprContext inclusive_or_expr() {
			return getRuleContext(Inclusive_or_exprContext.class,0);
		}
		public Exclusive_or_exprContext exclusive_or_expr() {
			return getRuleContext(Exclusive_or_exprContext.class,0);
		}
		public GetOrContext(Inclusive_or_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetOr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetOr(this);
		}
	}

	public final Inclusive_or_exprContext inclusive_or_expr() throws RecognitionException {
		return inclusive_or_expr(0);
	}

	private Inclusive_or_exprContext inclusive_or_expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Inclusive_or_exprContext _localctx = new Inclusive_or_exprContext(_ctx, _parentState);
		Inclusive_or_exprContext _prevctx = _localctx;
		int _startState = 62;
		enterRecursionRule(_localctx, 62, RULE_inclusive_or_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new GetXorExprContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(445);
			exclusive_or_expr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(452);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,48,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new GetOrContext(new Inclusive_or_exprContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_inclusive_or_expr);
					setState(447);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(448);
					match(T__29);
					setState(449);
					exclusive_or_expr(0);
					}
					} 
				}
				setState(454);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,48,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Logical_and_exprContext extends ParserRuleContext {
		public Logical_and_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logical_and_expr; }
	 
		public Logical_and_exprContext() { }
		public void copyFrom(Logical_and_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GetOrExprContext extends Logical_and_exprContext {
		public Inclusive_or_exprContext inclusive_or_expr() {
			return getRuleContext(Inclusive_or_exprContext.class,0);
		}
		public GetOrExprContext(Logical_and_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetOrExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetOrExpr(this);
		}
	}
	public static class GetLogicAndContext extends Logical_and_exprContext {
		public Logical_and_exprContext logical_and_expr() {
			return getRuleContext(Logical_and_exprContext.class,0);
		}
		public Inclusive_or_exprContext inclusive_or_expr() {
			return getRuleContext(Inclusive_or_exprContext.class,0);
		}
		public GetLogicAndContext(Logical_and_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetLogicAnd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetLogicAnd(this);
		}
	}

	public final Logical_and_exprContext logical_and_expr() throws RecognitionException {
		return logical_and_expr(0);
	}

	private Logical_and_exprContext logical_and_expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Logical_and_exprContext _localctx = new Logical_and_exprContext(_ctx, _parentState);
		Logical_and_exprContext _prevctx = _localctx;
		int _startState = 64;
		enterRecursionRule(_localctx, 64, RULE_logical_and_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new GetOrExprContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(456);
			inclusive_or_expr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(463);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,49,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new GetLogicAndContext(new Logical_and_exprContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_logical_and_expr);
					setState(458);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(459);
					match(T__30);
					setState(460);
					inclusive_or_expr(0);
					}
					} 
				}
				setState(465);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,49,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Logical_or_exprContext extends ParserRuleContext {
		public Logical_or_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logical_or_expr; }
	 
		public Logical_or_exprContext() { }
		public void copyFrom(Logical_or_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GetLogicAndExprContext extends Logical_or_exprContext {
		public Logical_and_exprContext logical_and_expr() {
			return getRuleContext(Logical_and_exprContext.class,0);
		}
		public GetLogicAndExprContext(Logical_or_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetLogicAndExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetLogicAndExpr(this);
		}
	}
	public static class GetLogicOrContext extends Logical_or_exprContext {
		public Logical_or_exprContext logical_or_expr() {
			return getRuleContext(Logical_or_exprContext.class,0);
		}
		public Logical_and_exprContext logical_and_expr() {
			return getRuleContext(Logical_and_exprContext.class,0);
		}
		public GetLogicOrContext(Logical_or_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetLogicOr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetLogicOr(this);
		}
	}

	public final Logical_or_exprContext logical_or_expr() throws RecognitionException {
		return logical_or_expr(0);
	}

	private Logical_or_exprContext logical_or_expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Logical_or_exprContext _localctx = new Logical_or_exprContext(_ctx, _parentState);
		Logical_or_exprContext _prevctx = _localctx;
		int _startState = 66;
		enterRecursionRule(_localctx, 66, RULE_logical_or_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new GetLogicAndExprContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(467);
			logical_and_expr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(474);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,50,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new GetLogicOrContext(new Logical_or_exprContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_logical_or_expr);
					setState(469);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(470);
					match(T__31);
					setState(471);
					logical_and_expr(0);
					}
					} 
				}
				setState(476);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,50,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Assignment_exprContext extends ParserRuleContext {
		public Assignment_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment_expr; }
	 
		public Assignment_exprContext() { }
		public void copyFrom(Assignment_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GetLogicOrExprContext extends Assignment_exprContext {
		public Logical_or_exprContext logical_or_expr() {
			return getRuleContext(Logical_or_exprContext.class,0);
		}
		public GetLogicOrExprContext(Assignment_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetLogicOrExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetLogicOrExpr(this);
		}
	}
	public static class GetAssignmentContext extends Assignment_exprContext {
		public Unary_exprContext unary_expr() {
			return getRuleContext(Unary_exprContext.class,0);
		}
		public Assignment_operatorContext assignment_operator() {
			return getRuleContext(Assignment_operatorContext.class,0);
		}
		public Assignment_exprContext assignment_expr() {
			return getRuleContext(Assignment_exprContext.class,0);
		}
		public GetAssignmentContext(Assignment_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetAssignment(this);
		}
	}

	public final Assignment_exprContext assignment_expr() throws RecognitionException {
		Assignment_exprContext _localctx = new Assignment_exprContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_assignment_expr);
		try {
			setState(482);
			switch ( getInterpreter().adaptivePredict(_input,51,_ctx) ) {
			case 1:
				_localctx = new GetLogicOrExprContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(477);
				logical_or_expr(0);
				}
				break;
			case 2:
				_localctx = new GetAssignmentContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(478);
				unary_expr();
				setState(479);
				assignment_operator();
				setState(480);
				assignment_expr();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_operatorContext extends ParserRuleContext {
		public Assignment_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterAssignment_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitAssignment_operator(this);
		}
	}

	public final Assignment_operatorContext assignment_operator() throws RecognitionException {
		Assignment_operatorContext _localctx = new Assignment_operatorContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_assignment_operator);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(484);
			match(T__8);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Argument_listContext extends ParserRuleContext {
		public Argument_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argument_list; }
	 
		public Argument_listContext() { }
		public void copyFrom(Argument_listContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GetAssignmentExprContext extends Argument_listContext {
		public Assignment_exprContext assignment_expr() {
			return getRuleContext(Assignment_exprContext.class,0);
		}
		public GetAssignmentExprContext(Argument_listContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetAssignmentExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetAssignmentExpr(this);
		}
	}
	public static class GetArguListContext extends Argument_listContext {
		public Argument_listContext argument_list() {
			return getRuleContext(Argument_listContext.class,0);
		}
		public Assignment_exprContext assignment_expr() {
			return getRuleContext(Assignment_exprContext.class,0);
		}
		public GetArguListContext(Argument_listContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterGetArguList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitGetArguList(this);
		}
	}

	public final Argument_listContext argument_list() throws RecognitionException {
		return argument_list(0);
	}

	private Argument_listContext argument_list(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Argument_listContext _localctx = new Argument_listContext(_ctx, _parentState);
		Argument_listContext _prevctx = _localctx;
		int _startState = 72;
		enterRecursionRule(_localctx, 72, RULE_argument_list, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new GetAssignmentExprContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(487);
			assignment_expr();
			}
			_ctx.stop = _input.LT(-1);
			setState(494);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,52,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new GetArguListContext(new Argument_listContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_argument_list);
					setState(489);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(490);
					match(T__5);
					setState(491);
					assignment_expr();
					}
					} 
				}
				setState(496);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,52,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public Assignment_exprContext assignment_expr() {
			return getRuleContext(Assignment_exprContext.class,0);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitExpr(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_expr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(497);
			assignment_expr();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstantContext extends ParserRuleContext {
		public TerminalNode TRUE() { return getToken(MParserParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(MParserParser.FALSE, 0); }
		public TerminalNode NULL() { return getToken(MParserParser.NULL, 0); }
		public TerminalNode NUMBER() { return getToken(MParserParser.NUMBER, 0); }
		public ConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).enterConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MParserListener ) ((MParserListener)listener).exitConstant(this);
		}
	}

	public final ConstantContext constant() throws RecognitionException {
		ConstantContext _localctx = new ConstantContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_constant);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(499);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NULL) | (1L << FALSE) | (1L << TRUE) | (1L << NUMBER))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 7:
			return type_sempred((TypeContext)_localctx, predIndex);
		case 19:
			return postfix_expr_sempred((Postfix_exprContext)_localctx, predIndex);
		case 24:
			return multiplicative_expr_sempred((Multiplicative_exprContext)_localctx, predIndex);
		case 25:
			return additive_expr_sempred((Additive_exprContext)_localctx, predIndex);
		case 26:
			return shift_expr_sempred((Shift_exprContext)_localctx, predIndex);
		case 27:
			return relational_expr_sempred((Relational_exprContext)_localctx, predIndex);
		case 28:
			return equality_expr_sempred((Equality_exprContext)_localctx, predIndex);
		case 29:
			return and_expr_sempred((And_exprContext)_localctx, predIndex);
		case 30:
			return exclusive_or_expr_sempred((Exclusive_or_exprContext)_localctx, predIndex);
		case 31:
			return inclusive_or_expr_sempred((Inclusive_or_exprContext)_localctx, predIndex);
		case 32:
			return logical_and_expr_sempred((Logical_and_exprContext)_localctx, predIndex);
		case 33:
			return logical_or_expr_sempred((Logical_or_exprContext)_localctx, predIndex);
		case 36:
			return argument_list_sempred((Argument_listContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean type_sempred(TypeContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean postfix_expr_sempred(Postfix_exprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 6);
		case 2:
			return precpred(_ctx, 5);
		case 3:
			return precpred(_ctx, 4);
		case 4:
			return precpred(_ctx, 3);
		case 5:
			return precpred(_ctx, 2);
		case 6:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean multiplicative_expr_sempred(Multiplicative_exprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 7:
			return precpred(_ctx, 3);
		case 8:
			return precpred(_ctx, 2);
		case 9:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean additive_expr_sempred(Additive_exprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 10:
			return precpred(_ctx, 2);
		case 11:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean shift_expr_sempred(Shift_exprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 12:
			return precpred(_ctx, 2);
		case 13:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean relational_expr_sempred(Relational_exprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 14:
			return precpred(_ctx, 4);
		case 15:
			return precpred(_ctx, 3);
		case 16:
			return precpred(_ctx, 2);
		case 17:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean equality_expr_sempred(Equality_exprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 18:
			return precpred(_ctx, 2);
		case 19:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean and_expr_sempred(And_exprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 20:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean exclusive_or_expr_sempred(Exclusive_or_exprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 21:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean inclusive_or_expr_sempred(Inclusive_or_exprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 22:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean logical_and_expr_sempred(Logical_and_exprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 23:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean logical_or_expr_sempred(Logical_or_exprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 24:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean argument_list_sempred(Argument_listContext _localctx, int predIndex) {
		switch (predIndex) {
		case 25:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3;\u01f8\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\3\2\3\2\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3]\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5"+
		"\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5n\n\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\5\6~\n\6\3\7\3\7\3\b\3\b\3\b\3\b\5\b\u0086\n"+
		"\b\3\b\5\b\u0089\n\b\3\t\3\t\3\t\3\t\3\t\5\t\u0090\n\t\3\t\3\t\3\t\7\t"+
		"\u0095\n\t\f\t\16\t\u0098\13\t\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u00a0\n\n\3"+
		"\13\3\13\3\13\3\13\5\13\u00a6\n\13\3\f\3\f\5\f\u00aa\n\f\3\r\3\r\3\r\3"+
		"\r\3\r\3\r\5\r\u00b2\n\r\3\r\3\r\3\r\5\r\u00b7\n\r\3\r\3\r\5\r\u00bb\n"+
		"\r\3\r\3\r\5\r\u00bf\n\r\3\r\3\r\3\r\5\r\u00c4\n\r\5\r\u00c6\n\r\3\16"+
		"\3\16\5\16\u00ca\n\16\3\16\3\16\3\16\3\16\5\16\u00d0\n\16\3\17\3\17\3"+
		"\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u00dc\n\17\3\20\3\20\5\20"+
		"\u00e0\n\20\3\20\3\20\3\20\3\20\3\20\5\20\u00e7\n\20\3\21\3\21\3\21\3"+
		"\21\3\21\3\21\5\21\u00ef\n\21\3\21\3\21\3\21\3\21\3\21\3\21\5\21\u00f7"+
		"\n\21\3\21\3\21\3\21\5\21\u00fc\n\21\5\21\u00fe\n\21\3\22\3\22\3\22\3"+
		"\22\3\22\3\22\5\22\u0106\n\22\3\23\3\23\3\23\3\23\5\23\u010c\n\23\3\24"+
		"\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u0115\n\24\3\25\3\25\3\25\3\25\3\25"+
		"\3\25\3\25\3\25\3\25\3\25\3\25\5\25\u0122\n\25\3\25\3\25\3\25\3\25\3\25"+
		"\3\25\5\25\u012a\n\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\7\25\u0134"+
		"\n\25\f\25\16\25\u0137\13\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3"+
		"\26\3\26\3\26\3\26\5\26\u0145\n\26\3\27\3\27\3\30\3\30\3\30\3\30\7\30"+
		"\u014d\n\30\f\30\16\30\u0150\13\30\5\30\u0152\n\30\3\31\3\31\5\31\u0156"+
		"\n\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32"+
		"\3\32\7\32\u0166\n\32\f\32\16\32\u0169\13\32\3\33\3\33\3\33\3\33\3\33"+
		"\3\33\3\33\3\33\3\33\7\33\u0174\n\33\f\33\16\33\u0177\13\33\3\34\3\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\3\34\7\34\u0182\n\34\f\34\16\34\u0185\13"+
		"\34\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3"+
		"\35\3\35\7\35\u0196\n\35\f\35\16\35\u0199\13\35\3\36\3\36\3\36\3\36\3"+
		"\36\3\36\3\36\3\36\3\36\7\36\u01a4\n\36\f\36\16\36\u01a7\13\36\3\37\3"+
		"\37\3\37\3\37\3\37\3\37\7\37\u01af\n\37\f\37\16\37\u01b2\13\37\3 \3 \3"+
		" \3 \3 \3 \7 \u01ba\n \f \16 \u01bd\13 \3!\3!\3!\3!\3!\3!\7!\u01c5\n!"+
		"\f!\16!\u01c8\13!\3\"\3\"\3\"\3\"\3\"\3\"\7\"\u01d0\n\"\f\"\16\"\u01d3"+
		"\13\"\3#\3#\3#\3#\3#\3#\7#\u01db\n#\f#\16#\u01de\13#\3$\3$\3$\3$\3$\5"+
		"$\u01e5\n$\3%\3%\3&\3&\3&\3&\3&\3&\7&\u01ef\n&\f&\16&\u01f2\13&\3\'\3"+
		"\'\3(\3(\3(\2\17\20(\62\64\668:<>@BDJ)\2\4\6\b\n\f\16\20\22\24\26\30\32"+
		"\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLN\2\4\3\2\21\22\5\2&&()\66\66\u021a"+
		"\2P\3\2\2\2\4\\\3\2\2\2\6^\3\2\2\2\bm\3\2\2\2\n}\3\2\2\2\f\177\3\2\2\2"+
		"\16\u0088\3\2\2\2\20\u008f\3\2\2\2\22\u009f\3\2\2\2\24\u00a5\3\2\2\2\26"+
		"\u00a9\3\2\2\2\30\u00c5\3\2\2\2\32\u00cf\3\2\2\2\34\u00db\3\2\2\2\36\u00e6"+
		"\3\2\2\2 \u00fd\3\2\2\2\"\u0105\3\2\2\2$\u010b\3\2\2\2&\u0114\3\2\2\2"+
		"(\u0116\3\2\2\2*\u0144\3\2\2\2,\u0146\3\2\2\2.\u0151\3\2\2\2\60\u0153"+
		"\3\2\2\2\62\u0159\3\2\2\2\64\u016a\3\2\2\2\66\u0178\3\2\2\28\u0186\3\2"+
		"\2\2:\u019a\3\2\2\2<\u01a8\3\2\2\2>\u01b3\3\2\2\2@\u01be\3\2\2\2B\u01c9"+
		"\3\2\2\2D\u01d4\3\2\2\2F\u01e4\3\2\2\2H\u01e6\3\2\2\2J\u01e8\3\2\2\2L"+
		"\u01f3\3\2\2\2N\u01f5\3\2\2\2PQ\5\4\3\2Q\3\3\2\2\2RS\5\6\4\2ST\5\4\3\2"+
		"T]\3\2\2\2UV\5\n\6\2VW\5\4\3\2W]\3\2\2\2XY\5\34\17\2YZ\5\4\3\2Z]\3\2\2"+
		"\2[]\3\2\2\2\\R\3\2\2\2\\U\3\2\2\2\\X\3\2\2\2\\[\3\2\2\2]\5\3\2\2\2^_"+
		"\7\62\2\2_`\7\65\2\2`a\7\3\2\2ab\5\b\5\2bc\7\4\2\2c\7\3\2\2\2de\5\20\t"+
		"\2ef\7\65\2\2fg\7\5\2\2gn\3\2\2\2hi\5\20\t\2ij\7\65\2\2jk\7\5\2\2kl\5"+
		"\b\5\2ln\3\2\2\2md\3\2\2\2mh\3\2\2\2n\t\3\2\2\2op\5\20\t\2pq\5\f\7\2q"+
		"r\7\6\2\2rs\5\16\b\2st\7\7\2\2tu\5\22\n\2u~\3\2\2\2vw\7\'\2\2wx\5\f\7"+
		"\2xy\7\6\2\2yz\5\16\b\2z{\7\7\2\2{|\5\22\n\2|~\3\2\2\2}o\3\2\2\2}v\3\2"+
		"\2\2~\13\3\2\2\2\177\u0080\7\65\2\2\u0080\r\3\2\2\2\u0081\u0082\5\20\t"+
		"\2\u0082\u0085\7\65\2\2\u0083\u0084\7\b\2\2\u0084\u0086\5\16\b\2\u0085"+
		"\u0083\3\2\2\2\u0085\u0086\3\2\2\2\u0086\u0089\3\2\2\2\u0087\u0089\3\2"+
		"\2\2\u0088\u0081\3\2\2\2\u0088\u0087\3\2\2\2\u0089\17\3\2\2\2\u008a\u008b"+
		"\b\t\1\2\u008b\u0090\7$\2\2\u008c\u0090\7%\2\2\u008d\u0090\7#\2\2\u008e"+
		"\u0090\7\65\2\2\u008f\u008a\3\2\2\2\u008f\u008c\3\2\2\2\u008f\u008d\3"+
		"\2\2\2\u008f\u008e\3\2\2\2\u0090\u0096\3\2\2\2\u0091\u0092\f\3\2\2\u0092"+
		"\u0093\7\t\2\2\u0093\u0095\7\n\2\2\u0094\u0091\3\2\2\2\u0095\u0098\3\2"+
		"\2\2\u0096\u0094\3\2\2\2\u0096\u0097\3\2\2\2\u0097\21\3\2\2\2\u0098\u0096"+
		"\3\2\2\2\u0099\u009a\7\3\2\2\u009a\u00a0\7\4\2\2\u009b\u009c\7\3\2\2\u009c"+
		"\u009d\5\24\13\2\u009d\u009e\7\4\2\2\u009e\u00a0\3\2\2\2\u009f\u0099\3"+
		"\2\2\2\u009f\u009b\3\2\2\2\u00a0\23\3\2\2\2\u00a1\u00a6\5\26\f\2\u00a2"+
		"\u00a3\5\26\f\2\u00a3\u00a4\5\24\13\2\u00a4\u00a6\3\2\2\2\u00a5\u00a1"+
		"\3\2\2\2\u00a5\u00a2\3\2\2\2\u00a6\25\3\2\2\2\u00a7\u00aa\5\32\16\2\u00a8"+
		"\u00aa\5\34\17\2\u00a9\u00a7\3\2\2\2\u00a9\u00a8\3\2\2\2\u00aa\27\3\2"+
		"\2\2\u00ab\u00ac\7-\2\2\u00ac\u00ad\7\6\2\2\u00ad\u00ae\5L\'\2\u00ae\u00b1"+
		"\7\7\2\2\u00af\u00b2\5\22\n\2\u00b0\u00b2\5\32\16\2\u00b1\u00af\3\2\2"+
		"\2\u00b1\u00b0\3\2\2\2\u00b2\u00c6\3\2\2\2\u00b3\u00b4\7,\2\2\u00b4\u00b6"+
		"\7\6\2\2\u00b5\u00b7\5L\'\2\u00b6\u00b5\3\2\2\2\u00b6\u00b7\3\2\2\2\u00b7"+
		"\u00b8\3\2\2\2\u00b8\u00ba\7\5\2\2\u00b9\u00bb\5L\'\2\u00ba\u00b9\3\2"+
		"\2\2\u00ba\u00bb\3\2\2\2\u00bb\u00bc\3\2\2\2\u00bc\u00be\7\5\2\2\u00bd"+
		"\u00bf\5L\'\2\u00be\u00bd\3\2\2\2\u00be\u00bf\3\2\2\2\u00bf\u00c0\3\2"+
		"\2\2\u00c0\u00c3\7\7\2\2\u00c1\u00c4\5\22\n\2\u00c2\u00c4\5\32\16\2\u00c3"+
		"\u00c1\3\2\2\2\u00c3\u00c2\3\2\2\2\u00c4\u00c6\3\2\2\2\u00c5\u00ab\3\2"+
		"\2\2\u00c5\u00b3\3\2\2\2\u00c6\31\3\2\2\2\u00c7\u00d0\5\"\22\2\u00c8\u00ca"+
		"\5L\'\2\u00c9\u00c8\3\2\2\2\u00c9\u00ca\3\2\2\2\u00ca\u00cb\3\2\2\2\u00cb"+
		"\u00d0\7\5\2\2\u00cc\u00d0\5 \21\2\u00cd\u00d0\5\30\r\2\u00ce\u00d0\5"+
		"\36\20\2\u00cf\u00c7\3\2\2\2\u00cf\u00c9\3\2\2\2\u00cf\u00cc\3\2\2\2\u00cf"+
		"\u00cd\3\2\2\2\u00cf\u00ce\3\2\2\2\u00d0\33\3\2\2\2\u00d1\u00d2\5\20\t"+
		"\2\u00d2\u00d3\7\65\2\2\u00d3\u00d4\7\5\2\2\u00d4\u00dc\3\2\2\2\u00d5"+
		"\u00d6\5\20\t\2\u00d6\u00d7\7\65\2\2\u00d7\u00d8\7\13\2\2\u00d8\u00d9"+
		"\5L\'\2\u00d9\u00da\7\5\2\2\u00da\u00dc\3\2\2\2\u00db\u00d1\3\2\2\2\u00db"+
		"\u00d5\3\2\2\2\u00dc\35\3\2\2\2\u00dd\u00df\7\60\2\2\u00de\u00e0\5L\'"+
		"\2\u00df\u00de\3\2\2\2\u00df\u00e0\3\2\2\2\u00e0\u00e1\3\2\2\2\u00e1\u00e7"+
		"\7\5\2\2\u00e2\u00e3\7.\2\2\u00e3\u00e7\7\5\2\2\u00e4\u00e5\7/\2\2\u00e5"+
		"\u00e7\7\5\2\2\u00e6\u00dd\3\2\2\2\u00e6\u00e2\3\2\2\2\u00e6\u00e4\3\2"+
		"\2\2\u00e7\37\3\2\2\2\u00e8\u00e9\7*\2\2\u00e9\u00ea\7\6\2\2\u00ea\u00eb"+
		"\5L\'\2\u00eb\u00ee\7\7\2\2\u00ec\u00ef\5\22\n\2\u00ed\u00ef\5\32\16\2"+
		"\u00ee\u00ec\3\2\2\2\u00ee\u00ed\3\2\2\2\u00ef\u00fe\3\2\2\2\u00f0\u00f1"+
		"\7*\2\2\u00f1\u00f2\7\6\2\2\u00f2\u00f3\5L\'\2\u00f3\u00f6\7\7\2\2\u00f4"+
		"\u00f7\5\22\n\2\u00f5\u00f7\5\32\16\2\u00f6\u00f4\3\2\2\2\u00f6\u00f5"+
		"\3\2\2\2\u00f7\u00f8\3\2\2\2\u00f8\u00fb\7+\2\2\u00f9\u00fc\5\22\n\2\u00fa"+
		"\u00fc\5\32\16\2\u00fb\u00f9\3\2\2\2\u00fb\u00fa\3\2\2\2\u00fc\u00fe\3"+
		"\2\2\2\u00fd\u00e8\3\2\2\2\u00fd\u00f0\3\2\2\2\u00fe!\3\2\2\2\u00ff\u0100"+
		"\7\3\2\2\u0100\u0101\5$\23\2\u0101\u0102\7\4\2\2\u0102\u0106\3\2\2\2\u0103"+
		"\u0104\7\3\2\2\u0104\u0106\7\4\2\2\u0105\u00ff\3\2\2\2\u0105\u0103\3\2"+
		"\2\2\u0106#\3\2\2\2\u0107\u0108\5\32\16\2\u0108\u0109\5$\23\2\u0109\u010c"+
		"\3\2\2\2\u010a\u010c\5\32\16\2\u010b\u0107\3\2\2\2\u010b\u010a\3\2\2\2"+
		"\u010c%\3\2\2\2\u010d\u0115\7\65\2\2\u010e\u0115\7\64\2\2\u010f\u0115"+
		"\5N(\2\u0110\u0111\7\6\2\2\u0111\u0112\5L\'\2\u0112\u0113\7\7\2\2\u0113"+
		"\u0115\3\2\2\2\u0114\u010d\3\2\2\2\u0114\u010e\3\2\2\2\u0114\u010f\3\2"+
		"\2\2\u0114\u0110\3\2\2\2\u0115\'\3\2\2\2\u0116\u0117\b\25\1\2\u0117\u0118"+
		"\5&\24\2\u0118\u0135\3\2\2\2\u0119\u011a\f\b\2\2\u011a\u011b\7\t\2\2\u011b"+
		"\u011c\5L\'\2\u011c\u011d\7\n\2\2\u011d\u0134\3\2\2\2\u011e\u011f\f\7"+
		"\2\2\u011f\u0121\7\6\2\2\u0120\u0122\5J&\2\u0121\u0120\3\2\2\2\u0121\u0122"+
		"\3\2\2\2\u0122\u0123\3\2\2\2\u0123\u0134\7\7\2\2\u0124\u0125\f\6\2\2\u0125"+
		"\u0126\7\f\2\2\u0126\u0127\7\63\2\2\u0127\u0129\7\6\2\2\u0128\u012a\5"+
		"J&\2\u0129\u0128\3\2\2\2\u0129\u012a\3\2\2\2\u012a\u012b\3\2\2\2\u012b"+
		"\u0134\7\7\2\2\u012c\u012d\f\5\2\2\u012d\u012e\7\f\2\2\u012e\u0134\7\65"+
		"\2\2\u012f\u0130\f\4\2\2\u0130\u0134\7\r\2\2\u0131\u0132\f\3\2\2\u0132"+
		"\u0134\7\16\2\2\u0133\u0119\3\2\2\2\u0133\u011e\3\2\2\2\u0133\u0124\3"+
		"\2\2\2\u0133\u012c\3\2\2\2\u0133\u012f\3\2\2\2\u0133\u0131\3\2\2\2\u0134"+
		"\u0137\3\2\2\2\u0135\u0133\3\2\2\2\u0135\u0136\3\2\2\2\u0136)\3\2\2\2"+
		"\u0137\u0135\3\2\2\2\u0138\u0145\5(\25\2\u0139\u013a\7\r\2\2\u013a\u0145"+
		"\5*\26\2\u013b\u013c\7\16\2\2\u013c\u0145\5*\26\2\u013d\u013e\5,\27\2"+
		"\u013e\u013f\5\62\32\2\u013f\u0145\3\2\2\2\u0140\u0141\7\17\2\2\u0141"+
		"\u0145\5\62\32\2\u0142\u0143\7\20\2\2\u0143\u0145\5\62\32\2\u0144\u0138"+
		"\3\2\2\2\u0144\u0139\3\2\2\2\u0144\u013b\3\2\2\2\u0144\u013d\3\2\2\2\u0144"+
		"\u0140\3\2\2\2\u0144\u0142\3\2\2\2\u0145+\3\2\2\2\u0146\u0147\t\2\2\2"+
		"\u0147-\3\2\2\2\u0148\u0152\5*\26\2\u0149\u014a\7\61\2\2\u014a\u014e\5"+
		"\20\t\2\u014b\u014d\5\60\31\2\u014c\u014b\3\2\2\2\u014d\u0150\3\2\2\2"+
		"\u014e\u014c\3\2\2\2\u014e\u014f\3\2\2\2\u014f\u0152\3\2\2\2\u0150\u014e"+
		"\3\2\2\2\u0151\u0148\3\2\2\2\u0151\u0149\3\2\2\2\u0152/\3\2\2\2\u0153"+
		"\u0155\7\t\2\2\u0154\u0156\5L\'\2\u0155\u0154\3\2\2\2\u0155\u0156\3\2"+
		"\2\2\u0156\u0157\3\2\2\2\u0157\u0158\7\n\2\2\u0158\61\3\2\2\2\u0159\u015a"+
		"\b\32\1\2\u015a\u015b\5.\30\2\u015b\u0167\3\2\2\2\u015c\u015d\f\5\2\2"+
		"\u015d\u015e\7\23\2\2\u015e\u0166\5\62\32\6\u015f\u0160\f\4\2\2\u0160"+
		"\u0161\7\24\2\2\u0161\u0166\5\62\32\5\u0162\u0163\f\3\2\2\u0163\u0164"+
		"\7\25\2\2\u0164\u0166\5\62\32\4\u0165\u015c\3\2\2\2\u0165\u015f\3\2\2"+
		"\2\u0165\u0162\3\2\2\2\u0166\u0169\3\2\2\2\u0167\u0165\3\2\2\2\u0167\u0168"+
		"\3\2\2\2\u0168\63\3\2\2\2\u0169\u0167\3\2\2\2\u016a\u016b\b\33\1\2\u016b"+
		"\u016c\5\62\32\2\u016c\u0175\3\2\2\2\u016d\u016e\f\4\2\2\u016e\u016f\7"+
		"\17\2\2\u016f\u0174\5\62\32\2\u0170\u0171\f\3\2\2\u0171\u0172\7\20\2\2"+
		"\u0172\u0174\5\62\32\2\u0173\u016d\3\2\2\2\u0173\u0170\3\2\2\2\u0174\u0177"+
		"\3\2\2\2\u0175\u0173\3\2\2\2\u0175\u0176\3\2\2\2\u0176\65\3\2\2\2\u0177"+
		"\u0175\3\2\2\2\u0178\u0179\b\34\1\2\u0179\u017a\5\64\33\2\u017a\u0183"+
		"\3\2\2\2\u017b\u017c\f\4\2\2\u017c\u017d\7\26\2\2\u017d\u0182\5\64\33"+
		"\2\u017e\u017f\f\3\2\2\u017f\u0180\7\27\2\2\u0180\u0182\5\64\33\2\u0181"+
		"\u017b\3\2\2\2\u0181\u017e\3\2\2\2\u0182\u0185\3\2\2\2\u0183\u0181\3\2"+
		"\2\2\u0183\u0184\3\2\2\2\u0184\67\3\2\2\2\u0185\u0183\3\2\2\2\u0186\u0187"+
		"\b\35\1\2\u0187\u0188\5\66\34\2\u0188\u0197\3\2\2\2\u0189\u018a\f\6\2"+
		"\2\u018a\u018b\7\30\2\2\u018b\u0196\5\66\34\2\u018c\u018d\f\5\2\2\u018d"+
		"\u018e\7\31\2\2\u018e\u0196\5\66\34\2\u018f\u0190\f\4\2\2\u0190\u0191"+
		"\7\32\2\2\u0191\u0196\5\66\34\2\u0192\u0193\f\3\2\2\u0193\u0194\7\33\2"+
		"\2\u0194\u0196\5\66\34\2\u0195\u0189\3\2\2\2\u0195\u018c\3\2\2\2\u0195"+
		"\u018f\3\2\2\2\u0195\u0192\3\2\2\2\u0196\u0199\3\2\2\2\u0197\u0195\3\2"+
		"\2\2\u0197\u0198\3\2\2\2\u01989\3\2\2\2\u0199\u0197\3\2\2\2\u019a\u019b"+
		"\b\36\1\2\u019b\u019c\58\35\2\u019c\u01a5\3\2\2\2\u019d\u019e\f\4\2\2"+
		"\u019e\u019f\7\34\2\2\u019f\u01a4\58\35\2\u01a0\u01a1\f\3\2\2\u01a1\u01a2"+
		"\7\35\2\2\u01a2\u01a4\58\35\2\u01a3\u019d\3\2\2\2\u01a3\u01a0\3\2\2\2"+
		"\u01a4\u01a7\3\2\2\2\u01a5\u01a3\3\2\2\2\u01a5\u01a6\3\2\2\2\u01a6;\3"+
		"\2\2\2\u01a7\u01a5\3\2\2\2\u01a8\u01a9\b\37\1\2\u01a9\u01aa\5:\36\2\u01aa"+
		"\u01b0\3\2\2\2\u01ab\u01ac\f\3\2\2\u01ac\u01ad\7\36\2\2\u01ad\u01af\5"+
		":\36\2\u01ae\u01ab\3\2\2\2\u01af\u01b2\3\2\2\2\u01b0\u01ae\3\2\2\2\u01b0"+
		"\u01b1\3\2\2\2\u01b1=\3\2\2\2\u01b2\u01b0\3\2\2\2\u01b3\u01b4\b \1\2\u01b4"+
		"\u01b5\5<\37\2\u01b5\u01bb\3\2\2\2\u01b6\u01b7\f\3\2\2\u01b7\u01b8\7\37"+
		"\2\2\u01b8\u01ba\5<\37\2\u01b9\u01b6\3\2\2\2\u01ba\u01bd\3\2\2\2\u01bb"+
		"\u01b9\3\2\2\2\u01bb\u01bc\3\2\2\2\u01bc?\3\2\2\2\u01bd\u01bb\3\2\2\2"+
		"\u01be\u01bf\b!\1\2\u01bf\u01c0\5> \2\u01c0\u01c6\3\2\2\2\u01c1\u01c2"+
		"\f\3\2\2\u01c2\u01c3\7 \2\2\u01c3\u01c5\5> \2\u01c4\u01c1\3\2\2\2\u01c5"+
		"\u01c8\3\2\2\2\u01c6\u01c4\3\2\2\2\u01c6\u01c7\3\2\2\2\u01c7A\3\2\2\2"+
		"\u01c8\u01c6\3\2\2\2\u01c9\u01ca\b\"\1\2\u01ca\u01cb\5@!\2\u01cb\u01d1"+
		"\3\2\2\2\u01cc\u01cd\f\3\2\2\u01cd\u01ce\7!\2\2\u01ce\u01d0\5@!\2\u01cf"+
		"\u01cc\3\2\2\2\u01d0\u01d3\3\2\2\2\u01d1\u01cf\3\2\2\2\u01d1\u01d2\3\2"+
		"\2\2\u01d2C\3\2\2\2\u01d3\u01d1\3\2\2\2\u01d4\u01d5\b#\1\2\u01d5\u01d6"+
		"\5B\"\2\u01d6\u01dc\3\2\2\2\u01d7\u01d8\f\3\2\2\u01d8\u01d9\7\"\2\2\u01d9"+
		"\u01db\5B\"\2\u01da\u01d7\3\2\2\2\u01db\u01de\3\2\2\2\u01dc\u01da\3\2"+
		"\2\2\u01dc\u01dd\3\2\2\2\u01ddE\3\2\2\2\u01de\u01dc\3\2\2\2\u01df\u01e5"+
		"\5D#\2\u01e0\u01e1\5*\26\2\u01e1\u01e2\5H%\2\u01e2\u01e3\5F$\2\u01e3\u01e5"+
		"\3\2\2\2\u01e4\u01df\3\2\2\2\u01e4\u01e0\3\2\2\2\u01e5G\3\2\2\2\u01e6"+
		"\u01e7\7\13\2\2\u01e7I\3\2\2\2\u01e8\u01e9\b&\1\2\u01e9\u01ea\5F$\2\u01ea"+
		"\u01f0\3\2\2\2\u01eb\u01ec\f\3\2\2\u01ec\u01ed\7\b\2\2\u01ed\u01ef\5F"+
		"$\2\u01ee\u01eb\3\2\2\2\u01ef\u01f2\3\2\2\2\u01f0\u01ee\3\2\2\2\u01f0"+
		"\u01f1\3\2\2\2\u01f1K\3\2\2\2\u01f2\u01f0\3\2\2\2\u01f3\u01f4\5F$\2\u01f4"+
		"M\3\2\2\2\u01f5\u01f6\t\3\2\2\u01f6O\3\2\2\2\67\\m}\u0085\u0088\u008f"+
		"\u0096\u009f\u00a5\u00a9\u00b1\u00b6\u00ba\u00be\u00c3\u00c5\u00c9\u00cf"+
		"\u00db\u00df\u00e6\u00ee\u00f6\u00fb\u00fd\u0105\u010b\u0114\u0121\u0129"+
		"\u0133\u0135\u0144\u014e\u0151\u0155\u0165\u0167\u0173\u0175\u0181\u0183"+
		"\u0195\u0197\u01a3\u01a5\u01b0\u01bb\u01c6\u01d1\u01dc\u01e4\u01f0";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}