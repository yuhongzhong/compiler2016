package IR.IRRegister;


/**
 * Created by yhzmiao on 16/5/5.
 */
public class ArrayReg extends IRRegister {
    public int id;
    public int size;

    public ArrayReg (int creg) {
        this.id = creg;
    }

    public String toString () {
        return '$' + Integer.toString(id);
    }

    @Override
    public int getId() {
        return this.id;
    }
}
