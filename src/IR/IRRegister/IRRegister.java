package IR.IRRegister;

import IR.IRRegister.VirtualReg.VRegforAddr;
import IR.IRRegister.VirtualReg.VRegforVar;
import IR.IRRegister.VirtualReg.VirtualReg;

import java.util.HashMap;

/**
 * Created by yhzmiao on 16/5/2.
 */
public abstract class IRRegister {
    public static int cur_reg = 0;
    public static HashMap regList = new HashMap();
    public static HashMap uniReg = new HashMap();
    public static int cur_Tar = 0;
    public static int cur_tmp = 0;

    public static VRegforVar newVarReg (String var) {
        if (regList.containsKey(var)) {
            return new VRegforVar((int)regList.get(var));
        }
        //System.out.println(var);
        ++ cur_reg;
        regList.put(var, cur_reg);
        return new VRegforVar(cur_reg);
    }

    public static VRegforAddr newAddrReg (String var, VirtualReg address, int offset) {
        if (uniReg.get(var) != null) {
            return (VRegforAddr) uniReg.get(var);
        }
        ++ cur_reg;
        VRegforAddr ret = new VRegforAddr(cur_reg, offset, address);
        uniReg.put(var, ret);
        return ret;
    }

    public abstract String toString();

    public abstract int getId();
}
