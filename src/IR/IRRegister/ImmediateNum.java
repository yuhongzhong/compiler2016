package IR.IRRegister;

/**
 * Created by yhzmiao on 16/5/2.
 */
public class ImmediateNum extends IRRegister {
    public int value;

    public ImmediateNum () {}

    public ImmediateNum (int value) {
        this.value = value;
    }

    public String toString() {
        return Integer.toString(value);
    }

    public int getId() {
        return -1;
    }
}
