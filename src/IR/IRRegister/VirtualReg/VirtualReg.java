package IR.IRRegister.VirtualReg;


import IR.IRRegister.IRRegister;

/**
 * Created by yhzmiao on 16/5/2.
 */
public abstract class VirtualReg extends IRRegister {

    public abstract int getId ();
}
