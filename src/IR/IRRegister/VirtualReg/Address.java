package IR.IRRegister.VirtualReg;

/**
 * Created by yhzmiao on 16/5/7.
 */
public class Address extends VirtualReg {

    public String body;

    public Address () {}
    public Address (String body) {
        this.body = "Universe__" + body;
    }

    public String toString () {
        return body;
    }

    public int getId () {
        return -1;
    }
}
