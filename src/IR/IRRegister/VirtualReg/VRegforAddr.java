package IR.IRRegister.VirtualReg;

/**
 * Created by yhzmiao on 16/5/6.
 */
public class VRegforAddr extends VirtualReg {
    public int id;
    public int offset;
    public VirtualReg addr;

    public VRegforAddr () {
    }

    public VRegforAddr (int id, int offset, VirtualReg addr) {
        this.id = id;
        this.offset = offset;
        this.addr = addr;
    }

    @Override
    public int getId() {
        return id;
    }

    public String toString () {
        return "$" + Integer.toString(id);
    }
}
