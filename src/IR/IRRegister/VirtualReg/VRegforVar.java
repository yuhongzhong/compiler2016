package IR.IRRegister.VirtualReg;

import IR.IRRegister.IRRegister;

/**
 * Created by yhzmiao on 16/5/6.
 */
public class VRegforVar extends VirtualReg {
    public int id;

    public VRegforVar () {
        this.id = ++ IRRegister.cur_reg;
    }

    public VRegforVar (int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String toString () {
        return "$" + Integer.toString(id);
    }
}
