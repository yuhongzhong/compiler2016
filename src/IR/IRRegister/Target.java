package IR.IRRegister;

/**
 * Created by yhzmiao on 16/5/2.
 */
public class Target extends IRRegister {
    public String name;
    public int id;

    public Target(String name) {
        this.name = name;
        id = ++cur_Tar;
    }

    public Target(String name, int id){
        this.name = name;
        this.id = id;
    }

    public String toString() {
        return "%" + name + (id == 0 ? "" : Integer.toString(id));
    }

    public int getId() {
        return 0;
    }
}
