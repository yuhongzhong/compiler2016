package IR;

import java.util.HashMap;

/**
 * Created by yhzmiao on 16/5/9.
 */
public class FuncPara {
    public HashMap para;
    public int total;

    public FuncPara () {
        total = 1;
        para = new HashMap();
    }

    public void add (int num) {
        if (para.containsKey(num)) {
            return;
        }
        else {
            para.put(num, total ++);
            return;
        }
    }
}
