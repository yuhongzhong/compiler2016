package IR.IRInstruction.MemAccInstr;

import IR.IRRegister.IRRegister;
import IR.IRRegister.VirtualReg.VirtualReg;

/**
 * Created by yhzmiao on 16/5/2.
 */
public class Load extends MemAcc {
    public IRRegister addr;
    public int offset;
    public VirtualReg dest;

    public Load (IRRegister addr, int offset, VirtualReg dest) {
        this.addr = addr;
        this.offset = offset;
        this.dest = dest;
    }

    public String toString () {
        return dest.toString() + " = load 4 "  + addr.toString() + " " + Integer.toString(offset);
    }
}
