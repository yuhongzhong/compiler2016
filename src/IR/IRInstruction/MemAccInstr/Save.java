package IR.IRInstruction.MemAccInstr;

import IR.IRRegister.IRRegister;

/**
 * Created by yhzmiao on 16/5/2.
 */
public class Save extends MemAcc {
    public IRRegister addr, val;
    public int offset;

    public Save (IRRegister addr, IRRegister val, int offset) {
        this.addr = addr;
        this.val = val;
        this.offset = offset;
    }

    public String toString () {
        return "store 4 " + addr.toString() + " " + val.toString() + " " + Integer.toString(offset);
    }
}
