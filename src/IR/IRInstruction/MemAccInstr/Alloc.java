package IR.IRInstruction.MemAccInstr;

import IR.IRRegister.IRRegister;
import IR.IRRegister.VirtualReg.VirtualReg;

/**
 * Created by yhzmiao on 16/5/2.
 */
public class Alloc extends MemAcc {
    public VirtualReg reg;
    public IRRegister size;

    public Alloc (VirtualReg reg, IRRegister size) {
        this.reg = reg;
        this.size = size;
    }

    public String toString () {
        return reg.toString() + " = alloc " + size.toString();
    }
}
