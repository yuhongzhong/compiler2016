package IR.IRInstruction;

import IR.IRRegister.Target;

/**
 * Created by yhzmiao on 16/5/4.
 */
public class Label extends IRInstr {
    public Target body;

    public Label (Target body) {
        this.body = body;
    }

    public String toString () {
        return body.toString() + ":";
    }
}
