package IR.IRInstruction.JumpInstr;

import IR.IRRegister.Target;

/**
 * Created by yhzmiao on 16/5/2.
 */
public class Jump extends JumpInstr {
    public Target target;

    public Jump (Target target) {
        this.target = target;
    }

    public String toString() {
        return "jump " + target;
    }
}
