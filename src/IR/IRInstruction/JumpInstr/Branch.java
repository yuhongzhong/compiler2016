package IR.IRInstruction.JumpInstr;

import IR.IRRegister.Target;
import IR.IRRegister.VirtualReg.VirtualReg;

/**
 * Created by yhzmiao on 16/5/2.
 */
public class Branch extends JumpInstr {
    public VirtualReg cond;
    public Target ifTrue, ifFalse;

    public Branch (VirtualReg cond) {
        this.cond = cond;
        this.ifTrue = new Target("if_true");
        this.ifFalse = new Target("if_false");
    }

    public Branch (VirtualReg cond, Target ifTrue, Target ifFalse) {
        this.cond = cond;
        this.ifTrue = ifTrue;
        this.ifFalse = ifFalse;
    }

    public String toString () {
        return "br " + cond.toString() + " " + ifTrue.toString() + " " + ifFalse.toString();
    }
}
