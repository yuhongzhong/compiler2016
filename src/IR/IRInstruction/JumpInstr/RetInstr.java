package IR.IRInstruction.JumpInstr;

import IR.IRRegister.IRRegister;

/**
 * Created by yhzmiao on 16/5/2.
 */
public class RetInstr extends JumpInstr {
    public IRRegister ret;

    public RetInstr (IRRegister ret) {
        this.ret = ret;
    }

    public String toString () {
        return "ret " + (ret == null ? "" : ret.toString());
    }
}
