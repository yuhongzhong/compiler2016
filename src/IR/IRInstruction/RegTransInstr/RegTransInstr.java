package IR.IRInstruction.RegTransInstr;

import IR.IRInstruction.IRInstr;
import IR.IRRegister.IRRegister;
import IR.IRRegister.VirtualReg.VirtualReg;

/**
 * Created by yhzmiao on 16/5/2.
 */
public class RegTransInstr extends IRInstr {
    public VirtualReg dest;
    public IRRegister src;

    public RegTransInstr (VirtualReg dest, IRRegister src) {
        this.dest = dest;
        this.src = src;
    }

    public String toString () {
        return dest.toString() + " = move " + src.toString();
    }
}
