package IR.IRInstruction;

import IR.IRRegister.VirtualReg.VirtualReg;

import java.util.ArrayList;

/**
 * Created by yhzmiao on 16/5/4.
 */
public class FuncDeclInstr extends IRInstr {
    public String name;
    public ArrayList <VirtualReg> paraList = new ArrayList<>();

    public FuncDeclInstr () {
        name = null;
        paraList = new ArrayList<>();
    }

    public String toString () {
        String ret = "func " + name;
        if (paraList != null) {
            for (int i = 0; i < paraList.size(); ++i) {
                ret += " " + paraList.get(i).toString();
            }
        }
        return ret + " {";
    }
}
