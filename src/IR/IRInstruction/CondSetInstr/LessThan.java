package IR.IRInstruction.CondSetInstr;

import IR.IRRegister.IRRegister;
import IR.IRRegister.VirtualReg.VirtualReg;

/**
 * Created by yhzmiao on 16/5/2.
 */
public class LessThan extends CondInstr {
    public VirtualReg dest;
    public IRRegister src1, src2;


    public LessThan (VirtualReg dest, IRRegister src1, IRRegister src2) {
        this.dest = dest;
        this.src1 = src1;
        this.src2 = src2;
    }

    public String toString () {
        return dest.toString() + " = slt " + src1.toString() + " " + src2.toString();
    }
}
