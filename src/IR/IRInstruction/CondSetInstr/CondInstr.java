package IR.IRInstruction.CondSetInstr;

import IR.IRInstruction.IRInstr;
import IR.IRRegister.VirtualReg.VirtualReg;

/**
 * Created by yhzmiao on 16/5/2.
 */
public abstract class CondInstr extends IRInstr {
    public VirtualReg dest;
}
