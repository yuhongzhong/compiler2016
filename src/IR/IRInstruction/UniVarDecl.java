package IR.IRInstruction;

/**
 * Created by yhzmiao on 16/5/5.
 */
public class UniVarDecl extends IRInstr {

    public String name;
    public int value;

    public UniVarDecl (String name) {
        this.name = name;
        this.value = 0;
    }

    public String toString() {
        return null;
    }
}
