package IR.IRInstruction;

import java.util.ArrayList;

/**
 * Created by yhzmiao on 16/4/28.
 */
public abstract class IRInstr {
    public static ArrayList <IRInstr> instrs = new ArrayList<>();
    public static ArrayList <IRInstr> uniins = new ArrayList<>();

    public static void AddIns (IRInstr ins, String frame) {
        if (frame.equals("."))
            uniins.add(ins);
        else
            instrs.add(ins);
    }

    public abstract String toString();
}
