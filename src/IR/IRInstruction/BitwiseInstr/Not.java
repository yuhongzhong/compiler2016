package IR.IRInstruction.BitwiseInstr;

import IR.IRRegister.IRRegister;
import IR.IRRegister.VirtualReg.VirtualReg;

/**
 * Created by yhzmiao on 16/5/4.
 */
public class Not extends BitInstr {
    public VirtualReg dest;
    public IRRegister src;

    public Not (VirtualReg dest, IRRegister src) {
        this.dest = dest;
        this.src = src;
    }

    @Override
    public String toString() {
        return dest.toString() + " = not " + src.toString();
    }
}
