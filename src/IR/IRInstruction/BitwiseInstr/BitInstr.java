package IR.IRInstruction.BitwiseInstr;

import IR.IRInstruction.IRInstr;
import IR.IRRegister.VirtualReg.VirtualReg;

/**
 * Created by yhzmiao on 16/5/4.
 */
public abstract class BitInstr extends IRInstr {
    public VirtualReg dest;
}
