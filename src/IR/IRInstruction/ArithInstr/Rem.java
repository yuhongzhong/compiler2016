package IR.IRInstruction.ArithInstr;

import IR.IRRegister.IRRegister;
import IR.IRRegister.VirtualReg.VirtualReg;

/**
 * Created by yhzmiao on 16/5/4.
 */
public class Rem extends ArithInstr {
    public VirtualReg dest;
    public IRRegister src1, src2;

    public Rem (VirtualReg dest, IRRegister src1, IRRegister src2) {
        this.dest = dest;
        this.src1 = src1;
        this.src2 = src2;
    }

    @Override
    public String toString() {
        return dest.toString() + " = div " + src1.toString() + " " + src2.toString();
    }
}
