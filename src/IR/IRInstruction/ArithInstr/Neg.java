package IR.IRInstruction.ArithInstr;

import IR.IRRegister.IRRegister;
import IR.IRRegister.VirtualReg.VirtualReg;

/**
 * Created by yhzmiao on 16/5/4.
 */
public class Neg extends ArithInstr {
    public VirtualReg dest;
    public IRRegister src;

    public Neg (VirtualReg dest, IRRegister src) {
        this.dest = dest;
        this.src = src;
    }

    @Override
    public String toString() {
        return dest.toString() + " = shl " + src.toString();
    }
}
