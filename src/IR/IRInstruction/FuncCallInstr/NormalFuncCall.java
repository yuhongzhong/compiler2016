package IR.IRInstruction.FuncCallInstr;

import IR.IRRegister.IRRegister;
import IR.IRRegister.VirtualReg.VRegforVar;
import IR.IRRegister.VirtualReg.VirtualReg;

import java.util.ArrayList;

/**
 * Created by yhzmiao on 16/5/2.
 */
public class NormalFuncCall extends FuncCall {
    public String funcName;
    public ArrayList <IRRegister> list;
    public VirtualReg dest;

    public NormalFuncCall () {
        this.funcName = null;
        this.list = new ArrayList<>();
        this.dest = new VRegforVar();
    }

    public NormalFuncCall (String funcName) {
        this.funcName = funcName;
        this.list = new ArrayList<>();
        this.dest = new VRegforVar();
    }

    public String toString () {
        String regList = "";
        for (int i = 0; i < list.size(); ++ i)
            regList += list.get(i).toString() + " ";
        return dest.toString() + " = call " + funcName + " " + regList;
    }
}
