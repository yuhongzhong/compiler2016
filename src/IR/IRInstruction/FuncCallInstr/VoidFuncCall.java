package IR.IRInstruction.FuncCallInstr;

import IR.IRRegister.IRRegister;

import java.util.ArrayList;

/**
 * Created by yhzmiao on 16/5/2.
 */
public class VoidFuncCall extends FuncCall {
    public String funcName;
    public ArrayList<IRRegister> list = new ArrayList<>();

    public VoidFuncCall () {
        this.funcName = null;
        this.list = new ArrayList<>();
    }

    public VoidFuncCall (String funcName) {
        this.funcName = funcName;
        this.list = new ArrayList<>();
    }

    public String toString () {
        String regList = "";
        for (int i = 0; i < list.size(); ++ i)
            regList += list.get(i).toString() + " ";
        return "call " + funcName + " " + regList;
    }
}
