package IR.IRInstruction.FuncCallInstr;

import IR.IRRegister.IRRegister;
import IR.IRRegister.VirtualReg.VRegforVar;
import IR.IRRegister.VirtualReg.VirtualReg;

import java.util.ArrayList;

/**
 * Created by yhzmiao on 16/5/10.
 */
public class BuildinFuncCall extends FuncCall {
    public String funcName;
    public ArrayList<IRRegister> list;
    public VirtualReg dest;
    public boolean isVoid;

    public BuildinFuncCall () {
        this.funcName = null;
        this.list = new ArrayList<>();
        this.dest = new VRegforVar();
    }

    public BuildinFuncCall (String funcName, boolean isVoid) {
        this.funcName = funcName;
        this.list = new ArrayList<>();
        this.isVoid = isVoid;
        if (!isVoid)
            this.dest = new VRegforVar();
    }



    public String toString() {
        String regList = "";
        for (int i = 0; i < list.size(); ++ i) {
            //System.out.println(list.get(i));
            regList += list.get(i).toString() + " ";
        }
        return (isVoid ? "" : dest.toString() + " = ") + "call "  + funcName + " " + regList;
    }
}
