all: clean
	@mkdir -p ./bin/src/Buildin
	@cd ./src/Parser && java -jar "../../lib/antlr-4.5-complete.jar" -package Parser -Dlanguage=Java -listener -no-visitor -lib . MParser.g4
	@cd ./src && javac -cp "../lib/antlr-4.5-complete.jar" \
		./*/*/*/*/*/*.java \
		./*/*/*/*/*.java \
		./*/*/*/*.java \
		./*/*/*.java \
		./*/*.java \
		./*.java \
		-d ../bin
	@cp ./lib/antlr-4.5-complete.jar ./bin
	@cp ./src/Buildin/* ./bin/src/Buildin/
	@cd ./bin && jar xf ./antlr-4.5-complete.jar \
			 && rm -rf ./META-INF \
			 && jar cef Compiler Compiler.jar ./ \
			 && rm -rf ./antlr-4.5-complete.jar ./Compiler  ./org  ./st4hidden

clean:
	rm -rf ./bin
